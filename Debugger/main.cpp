/*
 * main.cpp
 *
 *  Created on: 04.07.2013
 *      Author: David Kofler
 */

#include "config.h"
#include "utility.h"

#include "MainLoops/LocalMainLoop.h"
#include "Server.h"
#include "DyninstEventLoop.h"
using namespace DynDebugger;

namespace Dyninst {
BPatch bpatch;
}

#include <exception>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include <cstdlib>
#include <cstring>
#include <signal.h>

static vector<const char *> usage = { "Usage: dyndebugger [ attach <PID> [<FILE>] | load <FILE> {<ARG>} ]",
		"       dyndebugger load <FILE> {<ARG>}",
		"       dyndebugger server[r] [ <PORT> ]",
		"       First mode: Attach to a process. On Windows, it is necessary to specify the binary.",
		"       Second mode: Spawn a new process with the provided arguments and suspend it.",
		"       Third mode: attach to remote debugger.",
		"       Fourth mode: Start server and wait for another dyndebugger to connect.",
		"       Without arguments: Start the debugger without connecting to a program" };

static void printUsage() {
	for (const char *line : usage) {
		cerr << line << endl;
	}
}

static int attachCommand(int argc, const char **argv);
static int loadCommand(int argc, const char **argv);
static int serverCommand(int argc, const char **argv);

int main(int argc, const char **argv) {
	DyninstEventLoop dyninstEventLoop;
	dyninstEventLoop.runInBackground();

	if (argc == 1) {
		return MainLoops::LocalMainLoop(cout).runIdle();
	}

	string command = argv[1];

	if (string("help") == argv[1] || string("--help") == argv[1] || string("-h") == argv[1]) {
		printUsage();
		return 0;

	} else if ("attach" == command) {
		return attachCommand(argc - 2, &argv[2]);

	} else if ("load" == command) {
		return loadCommand(argc - 2, &argv[2]);

	} else if ("serve" == command || "server" == command) {
		return serverCommand(argc - 2, &argv[2]);
	}

	cerr << "Unknown command" << endl;
	printUsage();
	return 2;
}

static int attachCommand(int argc, const char **argv) {
	int pid = 0;
	try {
		if (argc > 0) {
			pid = stoul(argv[0]);
		}
	} catch (invalid_argument&) {
	}

	if (pid == 0) {
		cerr << "A process ID was expected" << endl;
		printUsage();
		return 2;
	}

	const char *imageFile = nullptr;
	if (argc > 1) {
		imageFile = argv[3];
	}

	// If there is a image file
	return MainLoops::LocalMainLoop(cout).attachProcessAndRun(pid, imageFile);
}

static int loadCommand(int argc, const char **argv) {
	if (argc == 0) {
		cerr << "The filename of an executable file was expected" << endl;
		printUsage();
		return 2;
	}

	return MainLoops::LocalMainLoop(cout).loadProgramAndRun(argc, argv);
}

static Server *server = nullptr;

void sigIntHandler(int sino) {
	server->shutdown();
}

static int serverCommand(int argc, const char **argv) {
	struct sigaction handler;
	memset(&handler, 0, sizeof(handler));
	handler.sa_handler = sigIntHandler;
	sigaction(SIGINT, &handler, nullptr);

	if (argc == 0) {
		server = new Server();
		server->serve();
		return 0;
	}

	if (argc == 1) {
		unsigned port = stoul(argv[0]);
		if (port < 65536) {
			server = new Server(port);
			server->serve();
			return 0;
		}
	}

	cerr << "Invalid port" << endl;
	printUsage();
	return 2;
}
