/*
 * DyninstEventLoop.cpp
 *
 *  Created on: 25.03.2014
 *      Author: David Kofler
 */

#include "DyninstEventLoop.h"

#include "config.h"
#include "utility.h"

#include <dyninst/BPatch.h>

#include <algorithm>
#include <stdexcept>
#include <thread>
using namespace std;

#include <sys/select.h>
#include <unistd.h>

namespace DynDebugger {

DyninstEventLoop::DyninstEventLoop() {
	Utility::pipeCreate(terminationEventPipe);
}

DyninstEventLoop::~DyninstEventLoop() {
	shutdown();
	close(terminationEventPipe[1]);
	close(terminationEventPipe[0]);
}

void DyninstEventLoop::runInBackground() {
	eventHandlerThread = std::thread(mem_fn(&DyninstEventLoop::handleEvents), this);
}

/**
 * This function handles events from the Dyninst library, most importantly communication with the debuggee.
 */
void DyninstEventLoop::handleEvents() {
	const int dyninstEventFd = BPatch::getBPatch()->getNotificationFD();
	const int maxFd = max(dyninstEventFd, terminationEventPipe[0]);
	char c;
	fd_set descriptors;

	while (true) {
		FD_ZERO(&descriptors);
		FD_SET(dyninstEventFd, &descriptors);
		FD_SET(terminationEventPipe[0], &descriptors);
		// Wait until an event happened.
		int selectResult = select(maxFd + 1, &descriptors, nullptr, nullptr, nullptr);
		if (selectResult < 0) {
			throw runtime_error(string("handleEvents (call to `select`): ") + strerror(errno));
		}

		assert(selectResult > 0);
		if (FD_ISSET(dyninstEventFd, &descriptors)) {
			int readResult = read(dyninstEventFd, &c, 1);
			if (readResult != 1) {
				// Else Dyninst closed the pipe.
				if (readResult < 0) {
					throw runtime_error(string("handleEvents (call to `read`)") + strerror(errno));
				}

				return;
			}

			// Handle things which may have happened in the meantime.
			// This function will execute various callbacks (and spend hopefully only little time doing so).
			BPatch::getBPatch()->pollForStatusChange();
		}

		if (FD_ISSET(terminationEventPipe[0], &descriptors)) {
			// The program is going to shutdown. Another thread notified this thread.
			return;
		}

	}
}

void DyninstEventLoop::shutdown() {
	char c = 'q';
	int writeResult = write(terminationEventPipe[1], &c, 1);
	if (writeResult != 1) {
		throw runtime_error(strerror(errno));
	}

	eventHandlerThread.join();
}

} /* namespace DynDebugger */
