/*
 * client.cpp
 *
 *  Created on: 08.04.2014
 *      Author: David Kofler
 */

#include "config.h"
#include "utility.h"
#include "MainLoops/RemoteMainLoop.h"
using namespace DynDebugger;

#include <exception>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include <cstdlib>
#include <cstring>
#include <signal.h>

static vector<const char *> usage = { "Usage: dyndebugger-client <HOST> [<PORT>]" };

static void printUsage() {
	for (const char *line : usage) {
		cerr << line << endl;
	}
}

static int remoteCommand(int argc, const char **argv);

int main(int argc, const char **argv) {
	if (argc >= 2 && (string("help") == argv[1] || string("--help") == argv[1] || string("-h") == argv[1])) {
		printUsage();
		return 0;
	}

	return remoteCommand(argc-1, &argv[1]);
}

static int remoteCommand(int argc, const char **argv) {
	if (argc == 0) {
		cerr << "A hostname was expected" << endl;
		printUsage();
		return 2;
	}

	string hostname = argv[0];

	if (argc == 1) {
		return MainLoops::RemoteMainLoop(cout, hostname).runIdle();
	}

	try {
		unsigned port = stoul(argv[1]);
		if (port < 65536) {
			return MainLoops::RemoteMainLoop(cout, hostname, port).runIdle();
		}

	} catch (invalid_argument&) {
	}

	cerr << "Invalid port number" << endl;
	printUsage();
	return 2;
}
