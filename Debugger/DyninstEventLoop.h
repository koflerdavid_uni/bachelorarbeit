/*
 * DyninstEventLoop.h
 *
 *  Created on: 25.03.2014
 *      Author: David Kofler
 */

#ifndef DYNINSTEVENTLOOP_H_
#define DYNINSTEVENTLOOP_H_

#include <thread>
using namespace std;

namespace DynDebugger {

class DyninstEventLoop {
public:
	DyninstEventLoop();
	virtual ~DyninstEventLoop();

	void runInBackground();
	void shutdown();

protected:
	void handleEvents();

private:
	std::thread eventHandlerThread;
	int terminationEventPipe[2];
};

} /* namespace DynDebugger */

#endif /* DYNINSTEVENTLOOP_H_ */
