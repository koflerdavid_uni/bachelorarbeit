/*
 * RemoteMainLoop.h
 *
 *  Created on: 01.03.2014
 *      Author: David Kofler
 */

#ifndef REMOTEMAINLOOP_H_
#define REMOTEMAINLOOP_H_

#include "config.h"

#include "AbstractMainLoop.h"

#include <queue>
#include <string>
#include <thread>
#include <mutex>
using namespace std;

namespace DynDebugger {
namespace MainLoops {

class RemoteMainLoop: public AbstractMainLoop {
public:
	RemoteMainLoop(ostream& outputStream, const string& host, unsigned port = DYNDEBUGGER_SERVER_DEFAULT_PORT);
	~RemoteMainLoop();

	virtual int loadProgramAndRun(int argc, const char **argv);

	/**
	 * Starts a debugger and points it to a process.
	 */
	virtual int attachProcessAndRun(int pid, const char *imageFile = nullptr);

protected:
	virtual void executeCommand(const string& command, const string& arguments);
	void sendCommand(const string& command, const string& arguments);
	void handleResponses();

private:
	int connection;
	std::thread receiverThread;
	int notificationPipe[2];
	queue<pair<string, string>> commandsToProcess;
	std::mutex commandsToProcessMutex;
};

} /* namespace MainLoops */
} /* namespace DynDebugger */

#endif /* REMOTEMAINLOOP_H_ */
