/*
 * LocalMainLoop.cpp
 *
 *  Created on: 01.03.2013
 *      Author: David Kofler
 */

#include "config.h"

#include "LocalMainLoop.h"

#include "../Debugger.h"
#include "../parsers/CommandParser.h"

#include <cassert>
#include <cerrno>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <ostream>
#include <string>
#include <thread>
using namespace std;
using namespace std::placeholders;

#include <readline/readline.h>
#include <readline/history.h>

#include <sys/select.h>
#include <unistd.h>

namespace DynDebugger {
namespace MainLoops {

static bool readlineWrapper(const string& prompt, string& output) {
	char *line = readline(prompt.c_str());
	if (line && line[0] != '\0') {
		output = line;
		rl_free(line);
		return true;
	}

	return false;
}

LocalMainLoop::LocalMainLoop(std::ostream& _outputStream) :
		AbstractMainLoop(_outputStream), debugger(_outputStream,
				bind(mem_fn(&LocalMainLoop::printEventResponse), this, _1), readlineWrapper) {
}

LocalMainLoop::~LocalMainLoop() {
}

void LocalMainLoop::executeCommand(const string& command, const string& arguments) {
	debugger.executeCommand(command, arguments);
}

int LocalMainLoop::loadProgramAndRun(int argc, const char** argv) {
	// Prepare to start the specified program
	vector<string> arguments(argv, argv + argc);
	debugger.specifyProgramAndArguments(arguments);

	return runIdle();
}

int LocalMainLoop::attachProcessAndRun(int pid, const char* imageFile) {
	// Attach to process
	debugger.attachToProcess(pid, imageFile);

	if (!debugger.process) {
		outputStream << "Failed to attach to process" << endl;
	} else {
		outputStream << "Attaching to " << pid << endl;
	}

	return runIdle();
}

} /* namespace MainLoops */
} /* namespace DynDebugger */
