/*
 * RemoteMainLoop.cpp
 *
 *  Created on: 01.03.2014
 *      Author: David Kofler
 */

#include "config.h"

#include "RemoteMainLoop.h"

#include "../utility.h"

#include <cassert>
#include <cerrno>
#include <cstdint>
#include <cstring>
#include <functional>
#include <memory>
#include <mutex>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>
using namespace std;

#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>
#include <signal.h>

#include <readline/readline.h>
#include <readline/history.h>

namespace DynDebugger {
namespace MainLoops {

static bool sendRequest(int handle, vector<string>& parts);
static bool receiveResponse(int connection, MessageType& messageType, string& content);

RemoteMainLoop::RemoteMainLoop(std::ostream& _outputStream, const string& host, unsigned port) :
		AbstractMainLoop(_outputStream) {
	struct addrinfo hints;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = 0;

	struct addrinfo *results = nullptr;
	int error = getaddrinfo(host.c_str(), to_string(port).c_str(), &hints, &results);

	if (error == EAI_NODATA || error == EAI_NONAME) {
		throw invalid_argument(gai_strerror(error));
	} else if (error == EAI_SYSTEM) {
		throw runtime_error(strerror(errno));
	} else if (error != 0) {
		throw runtime_error(gai_strerror(error));
	}

	struct addrinfo *current = results;
	do {
		connection = socket(current->ai_family, current->ai_socktype, current->ai_protocol);
		if (connection == -1) {
			if (current->ai_next == nullptr) {
				throw runtime_error("Could not connect to " + host + ":" + to_string(port));
			}

			current = current->ai_next;
		}

	} while (connection == -1);

	if (connect(connection, current->ai_addr, current->ai_addrlen) != 0) {
		throw runtime_error(strerror(errno));
	}

	freeaddrinfo(results);

	receiverThread = std::thread(bind(mem_fn(&RemoteMainLoop::handleResponses), this));
	Utility::pipeCreate(notificationPipe);
}

RemoteMainLoop::~RemoteMainLoop() {
	shutdown(connection, SHUT_RDWR);
	// shutting down the socket is an event which the receiver thread can detect by himself.
	receiverThread.join();
	close(connection);
	close(notificationPipe[0]);
	close(notificationPipe[1]);
}

static bool receiveResponse(int connection, MessageType& messageType, string& content) {
	vector<uint8_t> messageBuffer;

	if (!Utility::safeReceive(connection, &messageType, sizeof(messageType))) {
		return false;
	}

	if (messageType == MessageType::END_OF_SESSION) {
		return true;
	}

	uint64_t messageLength;
	if (!Utility::safeReceive(connection, (void *) &messageLength, sizeof(messageLength))) {
		return false;
	}

	messageLength = Utility::endianDecode<uint64_t>(messageLength);

	if (messageLength > MAX_MESSAGE_LENGTH) {
		throw range_error("Received a too large message (length > " + to_string(MAX_MESSAGE_LENGTH) + ")");
	}

	messageBuffer.reserve(messageLength * sizeof(char));
	string::size_type toReceive = messageLength;
	if (!Utility::safeReceive(connection, messageBuffer.data(), messageLength * sizeof(char))) {
		return false;
	}

	content.assign((char *) messageBuffer.data(), messageLength * sizeof(char));
	return true;
}

void RemoteMainLoop::handleResponses() {
	MessageType messageType;
	string message;

	// Prevent SIG_PIPE when the connection is lost.
	// The case can already be handled with errno = EPIPE
	struct sigaction action;
	struct sigaction oldAction;
	memset(&action, 0, sizeof(action));
	memset(&oldAction, 0, sizeof(oldAction));

	action.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &action, &oldAction);

	fd_set readDescriptors;
	FD_ZERO(&readDescriptors);
	int maxFd = max(notificationPipe[0], connection);

	while (messageType != MessageType::END_OF_SESSION) {
		FD_SET(notificationPipe[0], &readDescriptors);
		FD_SET(connection, &readDescriptors);

		int result = select(maxFd + 1, &readDescriptors, nullptr, nullptr, nullptr);
		if (result == -1) {
			throw runtime_error(strerror(errno));
		}

		if (FD_ISSET(connection, &readDescriptors)) {
			if (!receiveResponse(connection, messageType, message)) {
				break;
			}

			vector<string> wrap;
			if (messageType == MessageType::INPUT_REQUEST) {
				char *input = readline(message.c_str());

				wrap.emplace_back(input);
				if (!input || !sendRequest(connection, wrap)) {
					break;
				}

				if (input[0] != '\0') {
					add_history(input);
				}

				rl_free(input);

			} else {
				wrap.emplace_back(message);
				printEventResponse(wrap);
			}
		}

		if (FD_ISSET(notificationPipe[0], &readDescriptors)) {
			char c;
			read(notificationPipe[0], &c, 1);
			std::lock_guard < std::mutex > lock(commandsToProcessMutex);
			if (!commandsToProcess.empty()) {
				pair<string, string> command = commandsToProcess.front();
				commandsToProcess.pop();

				sendCommand(command.first, command.second);
			}
		}
	}

	shutdown(connection, SHUT_RDWR);
	// Reset signal handler for SIGPIPE
	sigaction(SIGPIPE, &oldAction, &action);
}

int RemoteMainLoop::loadProgramAndRun(int argc, const char** argv) {
	ostringstream stream;

	if (argc > 0) {
		stream << Utility::escapeString(argv[0]);

		for (int i = 0; i < argc && argv[i] != nullptr; ++i) {
			stream << ' ';
			stream << Utility::escapeString(argv[i]);
		}
	}

	executeCommand("load", stream.str());

	return runIdle();
}

int RemoteMainLoop::attachProcessAndRun(int pid, const char* imageFile) {
	if (imageFile) {
		executeCommand("attach", to_string(pid));
	} else {
		executeCommand("attach", to_string(pid) + Utility::escapeString(imageFile));
	}

	return runIdle();
}

void RemoteMainLoop::executeCommand(const string& command, const string& arguments) {
	{
		std::lock_guard < std::mutex > lock(commandsToProcessMutex);
		commandsToProcess.emplace(command, arguments);
	}

	char c = 'c';
	// Notify the loop about the new command.
	// Since the loop executes select() condition variables cannot be used.
	write(notificationPipe[1], &c, 1);
}

static bool sendRequest(int handle, vector<string>& parts) {
	const uint64_t partCount = Utility::endianEncode<uint64_t>((uint64_t) parts.size());
	if (!Utility::safeSend(handle, &partCount, sizeof(partCount))) {
		return false;
	}

	size_t headerLength = parts.size() * sizeof(uint64_t);
	vector<uint8_t> buffer;
	buffer.reserve(headerLength);

	for (auto& part : parts) {
		const uint64_t partLength = Utility::endianEncode<uint64_t>((uint64_t) part.length());
		if (!Utility::safeSend(handle, &partLength, sizeof(partLength))) {
			return false;
		}
	}

	for (string& part : parts) {
		if (!Utility::safeSend(handle, part.c_str(), part.length() * sizeof(char))) {
			return false;
		}
	}

	return true;
}

void RemoteMainLoop::sendCommand(const string& command, const string& arguments) {
	vector<string> args;
	args.push_back(command);
	args.push_back(arguments);
	sendRequest(connection, args);
}

} /* namespace MainLoops */
} /* namespace DynDebugger */

