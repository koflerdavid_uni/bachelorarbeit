/*
 * LocalMainLoop.h
 *
 *  Created on: 01.03.2014
 *      Author: David Kofler
 */

#ifndef MAIN_LOOPS_LOCAL_MAIN_LOOP_H_
#define MAIN_LOOPS_LOCAL_MAIN_LOOP_H_

#include "config.h"

#include "AbstractMainLoop.h"

#include "../Debugger.h"

#include <ostream>
#include <string>
#include <thread>
#include <vector>
using namespace std;

namespace DynDebugger {
namespace MainLoops {

class LocalMainLoop: public AbstractMainLoop {
public:
	LocalMainLoop(ostream& outputStream);
	virtual ~LocalMainLoop();

	virtual int loadProgramAndRun(int argc, const char **argv);

	/**
	 * Starts a debugger and points it to a process.
	 */
	virtual int attachProcessAndRun(int pid, const char *imageFile = nullptr);

protected:
	virtual void executeCommand(const string& command, const string& arguments);

private:
	Debugger debugger;

	int quitHandlerThreadPipe[2];
	std::thread eventHandlerThread;
};

} /* namespace MainLoops */
} /* namespace DynDebugger */

#endif /* MAIN_LOOPS_LOCAL_MAIN_LOOP_H_ */
