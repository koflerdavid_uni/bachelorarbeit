/*
 * AbstractMainLoop.cpp
 *
 *  Created on: 01.03.2014
 *      Author: David Kofler
 */

#include "config.h"

#include "AbstractMainLoop.h"

#include "../parsers/CommandParser.h"

#include <string>
#include <vector>
using namespace std;

#include <readline/readline.h>
#include <readline/history.h>

namespace DynDebugger {
namespace MainLoops {

AbstractMainLoop::AbstractMainLoop(std::ostream& _outputStream) :
		outputStream(_outputStream) {
}

int AbstractMainLoop::runIdle() {
	char *input = nullptr;
	while ((input = readline("> ")) != nullptr) {
		add_history(input);

		string arguments;
		string command;

		if (Parsers::parseCommand(input, command, arguments)) {
			executeCommand(command, arguments);
		}

		free(input);
	}

	outputStream << endl;
	return 0;
}

void AbstractMainLoop::printEventResponse(vector<string>& outputBlocks) {
	// Save current input line
	char *savedLine;
	int savedPoint;

	savedPoint = rl_point;
	savedLine = rl_copy_text(0, rl_end);

	string prompt(rl_prompt);
	rl_set_prompt("");
	rl_replace_line("", 0);
	rl_redisplay();

	for (string& output : outputBlocks) {
		this->outputStream << output << endl;
	}

	// Restore input
	rl_set_prompt(prompt.c_str());
	rl_replace_line(savedLine, 0);
	rl_point = savedPoint;
	rl_redisplay();

	free(savedLine);
}

} /* namespace MainLoops */
} /* namespace DynDebugger */
