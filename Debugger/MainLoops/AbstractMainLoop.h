/*
 * MainLoop.h
 *
 *  Created on: 04.07.2013
 *      Author: David Kofler
 */

#ifndef __MAIN_LOOPS_ABSTRACT_MAIN_LOOP_H_
#define __MAIN_LOOPS_ABSTRACT_MAIN_LOOP_H_

#include "config.h"

#include <ostream>
#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {
namespace MainLoops {

class AbstractMainLoop {
public:
	AbstractMainLoop(std::ostream& _outputStream);
	virtual ~AbstractMainLoop() {
	}

	/**
	 * Starts an idle debugger session;
	 */
	int runIdle();

	/**
	 * Starts a debugger and performs setup to execute a binary.
	 */
	virtual int loadProgramAndRun(int argc, const char **argv) = 0;

	/**
	 * Starts a debugger and points it to a process.
	 */
	virtual int attachProcessAndRun(int pid, const char *imageFile = nullptr) = 0;

protected:
	virtual void executeCommand(const string& command, const string& arguments) = 0;
	void printEventResponse(vector<string>& outputBlocks);

	ostream& outputStream;
};

} /* namespace MainLoops */
} /* namespace DynDebugger */

#endif // __MAIN_LOOPS_ABSTRACT_MAIN_LOOP_H_
