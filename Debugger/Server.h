/*
 * Server.h
 *
 *  Created on: 01.03.2014
 *      Author: david
 */

#ifndef SERVER_H_
#define SERVER_H_

#include "config.h"

#include <thread>
#include <vector>
using namespace std;

namespace DynDebugger {

class Server {
public:
	Server(unsigned port = DYNDEBUGGER_SERVER_DEFAULT_PORT);
	~Server();

	void serveInBackground();
	void serve();
	void shutdown();

protected:
	void serveClient(int clientSocket);

private:
	void createServerSocket();

	unsigned port;
	std::thread serverThread;

	bool serverSocketInitialized = false;
	int serverSocketHandle;
	int shutdownPipe[2];
};

} /* namespace DynDebugger */

#endif /* SERVER_H_ */
