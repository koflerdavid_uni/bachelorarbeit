/*
 * columnprinter.cpp
 *
 *  Created on: 20.12.2013
 *      Author: David Kofler
 */

#include "TablePrinter.h"

#include <initializer_list>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {

TablePrinter::TablePrinter(initializer_list<string> &_columns) throw (NoColumnsException) :
		TablePrinter(vector<string>(_columns.begin(), _columns.end())) {
}

TablePrinter::TablePrinter(const vector<string> &_columns) throw (NoColumnsException) :
		columns(_columns) {
	if (columns.size() == 0) {
		throw NoColumnsException();
	}

	for (vector<string>::const_iterator it = columns.begin(); it != columns.end(); ++it) {
		widths.push_back(it->length());
	}
}

TablePrinter::~TablePrinter() {
	for (vector<const string *>::const_iterator it = toDelete.begin(); it != toDelete.end(); ++it) {
		delete *it;
	}
}

void TablePrinter::put(const string& data) {
	put(new string(data));
}

void TablePrinter::put(const char *data) {
	put(new string(data));
}

void TablePrinter::put(unsigned int number) {
	ostringstream sstream;
	sstream << number;
	string *str = new string(sstream.str());
	put(str);
}

void TablePrinter::putAddress(void *address) {
	const int ADDRESS_HEX_WIDTH = sizeof(void *) * 2;
	ostringstream sstream;
	sstream.seekp(0);
	sstream << showbase << internal << setfill('0') << setw(ADDRESS_HEX_WIDTH + 2) << hex << address;
	sstream.flush();
	string *str = new string(sstream.str());
	put(str);
}

void TablePrinter::put(int number) {
	stringstream sstream;
	sstream << number;
	string *str = new string(sstream.str());
	put(str);
}

void TablePrinter::put(bool value) {
	if (value) {
		put("*");
	} else {
		put("");
	}
}

/**
 * Internal method to save the value in the object. It does only bookkeeping tasks.
 * Formatting the data is done by the public overloaded methods.
 * Strings passed to this method will be deleted by the destructor.
 */
void TablePrinter::put(const string *data) {
	const int columnCount = columns.size();
	const int currentColumn = fields.size() % columnCount;

	// Calculate maximum column width
	if (data->size() > widths[currentColumn]) {
		widths[currentColumn] = data->size();
	}

	toDelete.push_back(data);
	fields.push_back(data);
}

bool TablePrinter::atEndOfRow() const {
	const int columnCount = columns.size();
	const int currentColumn = fields.size() % columnCount;
	return currentColumn == columnCount - 1;
}

static void center(const string& value, unsigned int width, string& output) {
	// Cut off at the right
	if (value.length() > width) {
		output.assign(value, 0, width);
		return;
	}

	const unsigned int difference = width - value.length();
	output.clear();
	output.reserve(width);
	output.append(difference / 2, ' ');
	output.append(value);
	output.append(difference / 2, ' ');

	if (difference % 2 != 0) {
		output.append(" ");
	}
}

void TablePrinter::output(ostream& stream, bool noHeaderIfEmpty) const {
	const unsigned int columnCount = columns.size();
	string temp;

	if (fields.size() >= columnCount || !noHeaderIfEmpty) {
		unsigned int totalWidth = 0;

		string spacingLine;

		for (unsigned int i = 0; i < columnCount; ++i) {
			spacingLine += '+';
			spacingLine.append(widths[i] + 2, '-');
		}

		spacingLine += '+';

		stream << spacingLine << endl;

		stream << "|";
		for (unsigned int i = 0; i < columnCount; ++i) {
			totalWidth += widths[i] + 3;
			center(columns[i], widths[i] + 2, temp);
			stream << temp << "|";
		}

		stream << endl << spacingLine << endl;
	}

	for (unsigned int i = 0; i < fields.size(); i += columnCount) {

		for (unsigned int j = 0; j < columnCount && i + j < fields.size(); ++j) {
			const string *value = fields[i + j];
			center(*value, widths[j] + 2, temp);
			stream << '|' << temp;
		}

		stream << '|' << endl;
	}
}

}
