/*
 * VariableLookupHelper.h
 *
 *  Created on: 28.03.2014
 *      Author: David Kofler
 */

#ifndef VARIABLELOOKUPHELPER_H_
#define VARIABLELOOKUPHELPER_H_

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_frame.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <map>
#include <string>
using namespace std;

namespace DynDebugger {

class VariableLookupHelper {
public:
	VariableLookupHelper(BPatch_addressSpace *_addressSpace, BPatch_frame _currentFrame);
	~VariableLookupHelper();

	BPatch_snippet *operator ()(const string& name);

private:
	BPatch_addressSpace *addressSpace;
	BPatch_frame currentFrame;
	BPatch_function *currentFunction;
	map<string, BPatch_snippet *> cache;
};

} /* namespace DynDebugger */

#endif /* VARIABLELOOKUPHELPER_H_ */
