/*
 * CommandTable.cpp
 *
 *  Created on: 02.04.2014
 *      Author: David Kofler
 */

#include "Commands.h"

#include "commands/breakpoint.h"
#include "commands/information.h"
#include "commands/manipulation.h"
#include "commands/process.h"
#include "commands/watchpoint.h"

namespace DynDebugger {

unordered_map<string, DebuggerCommand> CommandTable::commandTable;

static void initCommandTable(unordered_map<string, DebuggerCommand>& commandTable);

DebuggerCommand CommandTable::lookupCommand(const string& name) {
	initClass();

	auto it = commandTable.find(name);
	if (it == commandTable.end()) {
		throw NoSuchCommandException(name);
	}

	return it->second;
}

void CommandTable::initClass() {
	static Initializer initializer;
}

CommandTable::Initializer::Initializer() {
	initCommandTable(CommandTable::commandTable);
}

NoSuchCommandException::NoSuchCommandException(const string& name) :
		invalid_argument("Unknown command: " + name) {

}

DYNDEBUGGER_DEBUGGER_COMMAND(printHelp) {
	outputStream << "Commands:" << endl;
	for (auto& entry : CommandTable::commandTable) {
		outputStream << '\t' << entry.first << endl;
	}
}

static void initCommandTable(unordered_map<string, DebuggerCommand>& commandTable) {
	commandTable["?"] = printHelp;
	commandTable["h"] = printHelp;
	commandTable["help"] = printHelp;

	commandTable["a"] = Commands::attachToProcess;
	commandTable["attach"] = Commands::attachToProcess;
	commandTable["bt"] = Commands::printStacktrace;
	commandTable["backtrace"] = Commands::printStacktrace;
	commandTable["break"] = Commands::setBreakpoint;
	commandTable["c"] = Commands::continueProcess;
	commandTable["commands"] = Commands::addBreakpointActions;
	commandTable["continue"] = Commands::continueProcess;
	commandTable["detach"] = Commands::detachFromProcessCommand;
	commandTable["db"] = Commands::deleteBreakpoints;
	commandTable["delete_breakpoint"] = Commands::deleteBreakpoints;
	commandTable["delete_breakpoints"] = Commands::deleteBreakpoints;
	commandTable["file"] = Commands::loadBinaryFile;
	commandTable["find_fun"] = Commands::findFunction;
	commandTable["find_function"] = Commands::findFunction;
	commandTable["find_type"] = Commands::findType;
	commandTable["find_var"] = Commands::findVariable;
	commandTable["find_variable"] = Commands::findVariable;
	commandTable["f"] = Commands::selectFrame;
	commandTable["frame"] = Commands::selectFrame;
	commandTable["info_breakpoints"] = Commands::printBreakpoints;
	commandTable["im"] = Commands::printModules;
	commandTable["info_modules"] = Commands::printModules;
	commandTable["if"] = Commands::printFunctions;
	commandTable["ignore"] = Commands::setIgnoreCount;
	commandTable["info_functions"] = Commands::printFunctions;
	commandTable["ip"] = Commands::infoProgram;
	commandTable["info_program"] = Commands::infoProgram;
	commandTable["insert_set"] = Commands::insertSetSnippet;
	commandTable["it"] = Commands::printThreads;
	commandTable["info_threads"] = Commands::printThreads;
	commandTable["iv"] = Commands::printVariables;
	commandTable["info_variables"] = Commands::printVariables;
	commandTable["k"] = Commands::killProcess;
	commandTable["kill"] = Commands::killProcess;
	commandTable["print"] = Commands::printExpression;
	commandTable["r"] = Commands::runProcess;
	commandTable["run"] = Commands::runProcess;
	commandTable["run_hold"] = Commands::runAndHoldProcess;
	commandTable["set"] = Commands::setVariable;
	commandTable["show_args"] = Commands::showMutateeArguments;
	commandTable["show_pid"] = Commands::showPid;
	commandTable["print_scope"] = Commands::printLocalVariablesAndParameters;
	commandTable["start"] = Commands::startProcess;
	commandTable["start_hold"] = Commands::startAndHoldProcess;
	commandTable["step"] = Commands::stepExecution;
	commandTable["stop"] = Commands::stopProcessOrThread;
	commandTable["t"] = Commands::traceAccess;
	commandTable["thread"] = Commands::switchToThread;
	commandTable["trace"] = Commands::traceAccess;
	commandTable["wait"] = Commands::waitForStop;
	commandTable["wait_for"] = Commands::waitForStop;

	commandTable["call"] = Commands::callFunction;
	commandTable["insert_call"] = Commands::insertFunctionCall;
}

} /* namespace DynDebugger */
