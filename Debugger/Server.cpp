/*
 * Server.cpp
 *
 *  Created on: 01.03.2014
 *      Author: david
 */

#include "config.h"

#include "Server.h"

#include "Debugger.h"

#include "utility.h"

#ifdef WIN32
#error "Windows server support is not yet implemented"
#else
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#endif

#include <cassert>
#include <thread>
#include <vector>
using namespace std;
using namespace std::placeholders;

namespace DynDebugger {

Server::Server(unsigned _port) :
		port(_port) {
	createServerSocket();

	Utility::pipeCreate(shutdownPipe);
}

Server::~Server() {
	shutdown();
	serverThread.join();
}

void Server::createServerSocket() {
	struct addrinfo hints = { 0 };
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = AI_PASSIVE;

	struct addrinfo *results = nullptr;
	int error = getaddrinfo(nullptr, to_string(port).c_str(), &hints, &results);

	if (error == EAI_NODATA || error == EAI_NONAME) {
		throw invalid_argument(gai_strerror(error));
	} else if (error == EAI_SYSTEM) {
		throw runtime_error(strerror(errno));
	} else if (error != 0) {
		throw runtime_error(gai_strerror(error));
	}

	struct addrinfo *current = results;
	do {
		serverSocketHandle = socket(current->ai_family, current->ai_socktype, current->ai_protocol);
		if (serverSocketHandle == -1) {
			if (current->ai_next == nullptr) {
				throw runtime_error("Could not create socket");
			}

			current = current->ai_next;
		}

	} while (serverSocketHandle == -1);

	if (bind(serverSocketHandle, current->ai_addr, current->ai_addrlen) == -1) {
		throw runtime_error(strerror(errno));
	}

	freeaddrinfo(results);

	if (listen(serverSocketHandle, SOCKET_SERVER_CONNECTION_BACKLOG) == -1) {
		throw runtime_error(strerror(errno));
	}

	serverSocketInitialized = true;
}

void Server::serveInBackground() {
	serverThread = std::thread(bind(mem_fn(&Server::serve), this));
}

void Server::serve() {
	if (!serverSocketInitialized) {
		createServerSocket();
	}

	// Prevent SIG_PIPE when the connection is lost.
	// The case can already be handled with errno = EPIPE
	struct sigaction action;
	struct sigaction oldAction;
	memset(&action, 0, sizeof(action));
	memset(&oldAction, 0, sizeof(oldAction));

	action.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &action, &oldAction);

	fd_set descriptors;
	vector<std::thread> handlerThreads;

	const int maxFd = serverSocketHandle > shutdownPipe[0] ? serverSocketHandle : shutdownPipe[0];

	while (true) {
		FD_ZERO(&descriptors);
		FD_SET(serverSocketHandle, &descriptors);
		FD_SET(shutdownPipe[0], &descriptors);

		// Wait until an event happened.
		int selectResult = select(maxFd + 1, &descriptors, nullptr, nullptr, nullptr);
		if (selectResult < 0) {
			throw runtime_error(strerror(errno));
		}

		assert(selectResult > 0);

		if (FD_ISSET(shutdownPipe[0], &descriptors)) {
			// The server loop shall be terminated.
			::shutdown(serverSocketHandle, SHUT_RD);
			close(serverSocketHandle);
			serverSocketInitialized = false;
			break;
		}

		assert(FD_ISSET(serverSocketHandle, &descriptors));

		struct sockaddr_in clientAddress;
		socklen_t length;
		int clientSocket = accept(serverSocketHandle, (struct sockaddr *) &clientAddress, &length);
		if (clientSocket == -1) {
			throw runtime_error(strerror(errno));
		}

		handlerThreads.emplace_back(bind(mem_fn(&Server::serveClient), this, _1), clientSocket);
	}

	for (std::thread& thread : handlerThreads) {
		thread.join();
	}

	sigaction(SIGPIPE, &oldAction, &action);
}

bool readRequest(int handle, vector<string>& message) {
	uint64_t messagePartCount;

	if (!Utility::safeReceive(handle, &messagePartCount, sizeof(messagePartCount))) {
		return false;
	}

	messagePartCount = Utility::endianDecode<uint64_t>(messagePartCount);

	vector<uint64_t> messagePartsLengths;
	for (uint64_t i = 0; i < messagePartCount; ++i) {
		uint64_t length;

		if (!Utility::safeReceive(handle, &length, sizeof(length))) {
			return false;
		}

		messagePartsLengths.push_back(Utility::endianDecode<uint64_t>(length));
	}

	message.clear();
	message.reserve(messagePartCount);

	vector<char> buffer;
	for (uint64_t length : messagePartsLengths) {
		buffer.reserve(length);
		if (!Utility::safeReceive(handle, buffer.data(), length * sizeof(char))) {
			return false;
		}

		message.emplace_back(buffer.data(), length);
	}

	return true;
}

bool sendResponse(int handle, MessageType messageType, const void *data, size_t length) {
	if (!Utility::safeSend(handle, &messageType, sizeof(messageType))) {
		return false;
	}

	if (messageType == MessageType::END_OF_SESSION) {
		return true;
	}

	const uint64_t lineLength = Utility::endianEncode<uint64_t>(length);
	if (!Utility::safeSend(handle, &lineLength, sizeof(lineLength))) {
		return false;
	}

	if (!Utility::safeSend(handle, data, length)) {
		return false;
	}

	return true;
}

bool sendCommandOutput(int handle, const string& output) {
	string::size_type pos = 0;
	string::size_type endPos = output.find('\n');
	string::size_type nextDelimiter = 0;

	while (endPos != string::npos) {
		if (!sendResponse(handle, MessageType::OUTPUT_MESSAGE, &output.c_str()[pos], endPos - pos)) {
			return false;
		}

		pos = endPos + 1;
		endPos = output.find('\n', pos);
	}

	// Send end-of-output message
	return sendResponse(handle, MessageType::END_OF_OUTPUT, &output.c_str()[pos], output.size() - pos);
}

void Server::serveClient(int clientSocket) {
	ostringstream debuggerOutputStream;
	ostringstream debuggerEventStream;

	auto eventHandler = [clientSocket] (vector<string>& outputs) {
		for (string& output : outputs) {
			if (!sendCommandOutput(clientSocket, output)) {
				break;
			}
		}
	};

	auto readlineFunction = [clientSocket](const string& prompt, string& output) {
		if (!sendResponse(clientSocket, MessageType::INPUT_REQUEST, prompt.c_str(), prompt.size())) {
			return false;
		}

		vector<string> parts;
		if (!readRequest(clientSocket, parts) || parts.size() != 1) {
			return false;
		}

		output = parts.front();
		return true;
	};

	Debugger debugger(debuggerOutputStream, eventHandler, readlineFunction);

	vector<string> message;

	while (true) {
		// All known client messages are of length two.
		if (!readRequest(clientSocket, message) || message.size() != 2) {
			return;
		}

		string& command(message[0]);
		string& arguments(message[1]);

		// This is interpreted as sign that the client wishes to terminate the session.
		if (command.length() == 0 || command == "quit") {
			break;
		}

		debugger.executeCommand(command, arguments);

		// debuggerOutputStream now contains the output.
		if (!sendCommandOutput(clientSocket, debuggerOutputStream.str())) {
			return;
		}

		debuggerOutputStream.str("");
	}

	sendResponse(clientSocket, MessageType::END_OF_SESSION, nullptr, 0);
	::shutdown(clientSocket, SHUT_RDWR);
	close(clientSocket);
}

void Server::shutdown() {
	if (serverThread.joinable()) {
		char c = 'q';
		int writeResult = write(shutdownPipe[1], &c, 1);
		if (writeResult != 1) {
			throw runtime_error(strerror(errno));
		}
	}
}

} /* namespace DynDebugger */

