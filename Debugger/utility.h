/*
 * utility.h
 *
 *  Created on: 02.03.2014
 *      Author: David Kofler
 */

#ifndef UTILITY_H_
#define UTILITY_H_

#include "config.h"

#include <cstddef>
#include <cstdint>
#include <string>
using namespace std;

#include <arpa/inet.h>

namespace DynDebugger {
namespace Utility {

string escapeString(string source);
string escapeString(const char *source);

string to_string(void *address);

void pipeCreate(int pipefds[2]);

/**
 * Fill the buffer completely with data, without discarding anything.
 * If this is not possible because of errors, an exception will be thrown.
 * If the stream was closed, false will be returned.
 */
bool safeReceive(int handle, void *buffer, size_t length);

/**
 * Send all data contained within the specified buffer.
 * If this is not possible because of errors, throw an exception.
 * If the stream was closed, return false.
 */
bool safeSend(int handle, const void *buffer, size_t length);

template<typename T>
T endianEncode(T unencoded) {
	T mask = 0xFF;
	T encoded = 0;
	uint8_t *byte = (uint8_t*) &encoded;

	for (size_t i = 0; i < sizeof(encoded); ++i) {
		byte[i] = (unencoded & mask) >> (i*8);
		mask <<= 8;
	}

	return encoded;
}

template<typename T>
T endianDecode(T encoded) {
	T decoded = 0;
	uint8_t *byte = (uint8_t*) &encoded;

	for (size_t i = 0; i < sizeof(encoded); ++i) {
		decoded |= ((T) byte[i]) << (i * 8);
	}

	return decoded;
}

extern template uint8_t endianEncode<uint8_t>(uint8_t arg);

extern template uint8_t endianDecode<uint8_t>(uint8_t arg);

extern template uint16_t endianEncode<uint16_t>(uint16_t arg);

extern template uint16_t endianDecode<uint16_t>(uint16_t arg);

extern template uint32_t endianEncode<uint32_t>(uint32_t arg);

extern template uint32_t endianDecode<uint32_t>(uint32_t arg);

}
/* namespace Utility */
} /* namespace DynDebugger */

#endif /* UTILITY_H_ */
