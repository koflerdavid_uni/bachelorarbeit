/*
 * debugger.h
 *
 *  Created on: 15.12.2013
 *      Author: David Kofler
 */

#ifndef __DEBUGGER_H_
#define __DEBUGGER_H_

#include "Breakpoint.h"
#include "Commands.h"
#include "Location.h"

// DyninstAPI
#include <dyninst/BPatch.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_process.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <condition_variable>
#include <functional>
#include <map>
#include <mutex>
#include <ostream>
#include <queue>
#include <thread>
#include <vector>
using namespace std;

namespace DynDebugger {

class Debugger;

/**
 * This structure contains data which is only used by active breakpoints.
 */
struct BreakpointData {
	using Handle = unsigned;

	BreakpointData(Debugger& debugger, unsigned _breakpointId);
	~BreakpointData();

	Debugger& debugger;

	vector<BPatchSnippetHandle *> snippetHandles;
	BPatch_variableExpr *handleInDebuggee;
	Handle breakpointId;
};

/**
 * Represents a debugger instance. Each debugger instance may be connected to multiple processes over its lifetime.
 * Its main purpose is to execute commands, start new processes, attach to new processes and to handle breakpoints.
 *
 * It relies on the host application to call one of Dynist's event handler functions in regular intervals. This is
 * necessary because management of class-level resources (and an event handler thread would be that) is difficult,
 * not portable and error prone. Besides that, this class might not be the only user of the Dyninst API, so it
 * would not be a responsible choice to automagically handle Dyninst events.
 */
class Debugger {
public:
	Debugger(ostream& _outputStream, std::function<bool(const string&, string&)> _readlineFunction);
	Debugger(ostream& _outputStream, std::function<void(vector<string>&)> onAfterEvent,
			std::function<bool(const string&, string&)> _readlineFunction);
	Debugger(ostream& _outputStream, std::function<void()> onAfterCommand,
			std::function<void(vector<string>&)> onAfterEvent,
			std::function<bool(const string&, string&)> _readlineFunction);
	~Debugger();

	/**
	 * Due to the nature of this object (it includes a thread and manages a BPatch_process object)
	 * copying is not possible.
	 */
	Debugger(const Debugger&) = delete;

	void applyBreakpoints();
	void applySingleBreakpoint(const Breakpoint& breakpoint, unsigned breakpointId);
	void removeBreakpoint(unsigned breakpointId, bool removeFromContainer = true);

	bool executeCommand(const string& command, const string& arguments);

	bool attachToProcess(int pid, const char *pathToBinary = NULL);
	bool attachToProcess(int pid, string pathToImageFile);

	bool startProcess(bool hold);
	bool startProcess(vector<string> args, bool hold);

	void storeCurrentStackFrame(unsigned frameIndex = 0);
	void printBreakpointLocation(ostream& stream);

	bool detachFromProcess();
	bool terminateProcess();

	/**
	 * This method is used to specify the arguments which will be used to run the process.
	 */
	void specifyArguments(vector<string>& args);

	/**
	 * By using this method one specifies the program name and the arguments with which it will be run.
	 */
	void specifyProgramAndArguments(vector<string>& argv);

	BPatch_process *process;
	THR_ID current_thread;

	/**
	 * The index of the current stackframe on the current thread.
	 */
	unsigned currentFrameIndex;

	/**
	 * The current stackframe on the current thread.
	 */
	BPatch_frame currentFrame;

	/**
	 * The function associated with the current stackframe, if it could be determined.
	 */
	BPatch_function *currentFunction;

	/**
	 * The arguments used to start the debuggee.
	 */
	vector<string> arguments;

	/**
	 * The ID of the next breakpoint.
	 */
	unsigned nextBreakpointId = 1;

	/**
	 * If the debuggee is not started the breakpoints set by the user are stored here.
	 */
	map<unsigned, Breakpoint> breakpointsToApply;

	/**
	 * If the process is running the active breakpoints are stored here.
	 */
	map<unsigned, pair<Breakpoint, BreakpointData>> currentBreakpoints;

protected:
	/**
	 * Call this in each constructor to ensure that class initialization code gets run.
	 */
	static void initClass();

	/**
	 * Lookup method used by the message handler callback to associate debuggees with their
	 * Debugger object.
	 */
	static Debugger& getInstanceByProcess(BPatch_process *);

	/**
	 * The function which is executed by the breakpoint handler thread.
	 */
	void handleBreakpoints();

	/**
	 * Called whenthe debugger connects to a process, be it by starting it or by attaching.
	 */
	void handleProcessConnection();

	bool executeCommand(ostream& outputStream, const string& command, const string& arguments);

	/**
	 * To be executed when a breakpoint stopped the process. Since breakpoints are realized
	 * with instrumentation code the current stackframe is likely not be the one the user wants to see
	 * (but an instrumentation frame instead). This method takes care of storing the right stackframe.
	 */
	void storeBreakpointStackFrame();

	/**
	 * Called to perform cleanup task and to unregister itself from the global process -> debugger map.
	 */
	void handleProcessDisconnection();

	/**
	 * The Dyninst library is at least partially thread-safe, but the debugger commands aren't.
	 * Therefore invocations of debugger commands and of the event loop must not be concurrent.
	 */
	std::recursive_mutex dyninstLock;

	/**
	 * The handle of the breakpoint handler thread.
	 */
	std::thread breakpointHandlerThread;

	/*
	 * This is required for graceful termination of the object's breakpoint handler thread.
	 */
	volatile bool quitBreakpointHandlerThread = false;

	/**
	 * This queue contains the breakpoint the breakpoint handler thread shall handle.
	 */
	queue<unsigned> breakpointsToHandle;

	/**
	 * The condition variable necessary to notify the breakpoint handler thread.
	 */
	std::condition_variable_any breakpointCondition;

	/**
	 * The output stream where the output handlers write all output to.
	 */
	ostream& outputStream;

	/**
	 * Called after each command.
	 */
	std::function<void()> onAfterCommandHandler;

	/**
	 * Executed after an event (most importantly, breakpoints) have been handled.
	 */
	std::function<void(vector<string>&)> onAfterEventHandler;

	/**
	 * Called by debugger commands to get a line of input from the user.
	 */
	std::function<bool(const string&, string&)> readlineFunction;

private:
	/**
	 * The constructor contains code which is required to be run only once for the entire class,
	 * before any constructor for the outer class completes.
	 */
	class Initializer {
	public:
		Initializer();
	};

	friend class Initializer;

	/**
	 * A mutex ensuring that the accesses to the `processes` map are serialized.
	 */
	static std::mutex processesMutex;

	/**
	 * Contains a mapping of all known debuggees to their Debugger objects.
	 */
	static map<BPatch_process *, Debugger *> processes;

	/**
	 * Since there can only be one exit callback active at a time the previous one has to be remembered,
	 * executed on events and eventually restored.
	 */
	static BPatchExitCallback previousExitCallback;

	/**
	 * Handles the termination of a debuggee.
	 */
	static void handleProcessExit(BPatch_thread *proc, BPatch_exitType terminationStatus);

	/**
	 * Handles a message from the debuggee.
	 */
	static void handleMessageFromDebuggee(BPatch_process *process, void *buf, unsigned bufSize);
};

class DebuggerException {
public:
	DebuggerException(const string& _message) :
			message(_message) {
	}

	DebuggerException(const char *_message) :
			message(_message) {
	}

	const string& getMessage() const {
		return message;
	}

private:
	const string message;
};

class InvalidBreakpointException: public DebuggerException {
public:
	InvalidBreakpointException(const Location& _location) :
			DebuggerException(_location.toString() + " doesn't exist"), location(_location) {
	}

	const Location& getLocation() const {
		return location;
	}

private:
	const Location location;
};

} // namespace DynDebugger

#endif /* DEBUGGER_H_ */
