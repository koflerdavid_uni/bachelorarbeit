/*
 * process.cpp
 *
 *  Created on: 01.12.2013
 *      Author: David Kofler
 */

#include "process.h"

#include "breakpoint.h"

#include "../Debugger.h"
#include "../TablePrinter.h"
#include "../parsers/Tokenizer.h"

// DyninstAPI
#include <dyninst/BPatch_statement.h>
#include <dyninst/BPatch_snippet.h>

using namespace Dyninst;

#include <ostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

#include <cassert>
using namespace std;

namespace DynDebugger {
namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(attachToProcess) {
	if (debugger.process) {
		if (!debugger.process->isTerminated()) {
			throw DebuggerException("Already attached to a process or binary");
		}

		debugger.detachFromProcess();
	}

	try {
		vector<string> tokens = Parsers::splitString(arguments);

		if (tokens.size() == 0) {
			throw DebuggerException("No arguments!");
		}

		int pid = stoi(tokens.front());

		bool result;
		if (tokens.size() > 1) {
			result = debugger.attachToProcess(pid, tokens[1]);
		} else {
			result = debugger.attachToProcess(pid);
		}

		if (result) {
			outputStream << "Attaching to " << pid << endl;
		} else {
			throw DebuggerException("Could not attach to process");
		}

	} catch (invalid_argument& e) {
		throw DebuggerException("Invalid process ID");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(continueProcess) {
	if (debugger.process == nullptr || debugger.process->isTerminated()) {
		throw DebuggerException("No process");
	}

	debugger.applyBreakpoints();

	if (debugger.process->continueExecution()) {
		outputStream << "Process continues" << endl;

	} else {
		throw DebuggerException("Resuming process failed");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(waitForStop) {
	if (debugger.process == nullptr || debugger.process->isTerminated()) {
		throw DebuggerException("No process");
	}

	if (debugger.process->isStopped()) {
		throw DebuggerException("Process stopped with signal " + to_string(debugger.process->stopSignal()));
	}

	while (BPatch::getBPatch()->waitForStatusChange()) {
		if (debugger.process->isTerminated()) {
			outputStream << "Process terminated ";

			if (debugger.process->terminationStatus() == ExitedNormally) {
				outputStream << "with exit code " << debugger.process->getExitCode() << endl;
			} else {
				outputStream << "with signal " << debugger.process->getExitSignal() << endl;
			}
		} else if (debugger.process->isStopped()) {
			outputStream << "Process stopped with signal " << debugger.process->stopSignal() << endl;

			BPatch_thread *thread = debugger.process->getThread(debugger.current_thread);
			debugger.storeCurrentStackFrame();

			vector<BPatch_frame> frames;
			// We don't care if we don't get stack frames... printing the current line information is optional
			if (thread->getCallStack(frames)) {
				Address pc = (Address) frames[0].getPC();

				vector<BPatch_statement> statements;
				debugger.process->getSourceLines(pc, statements);

				for (vector<BPatch_statement>::iterator currentLine = statements.begin();
						currentLine != statements.end(); ++currentLine) {
					BPatch_statement &currentStatement = *currentLine;
					outputStream << "At " << currentStatement.fileName() << ":" << currentStatement.lineNumber() << ":"
							<< currentStatement.lineOffset() << endl;

				}
			}
		}
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(detachFromProcessCommand) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (debugger.detachFromProcess()) {
		outputStream << "Detached from process" << endl;
	} else {
		throw DebuggerException("Detaching was not successful");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(loadBinaryFile) {
	if (debugger.process) {
		if (!debugger.process->isTerminated()) {
			throw DebuggerException("Already attached to a process or binary");
		}

		debugger.detachFromProcess();
	}

	vector<string> tokens = Parsers::splitString(arguments);

	// Reset the argv array if called with no arguments.
	if (tokens.size() == 0) {
		debugger.arguments.clear();

	} else {
		debugger.specifyProgramAndArguments(tokens);
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(killProcess) {
	if (!debugger.process || debugger.process->isTerminated()) {
		throw DebuggerException("No process");
	}

	if (debugger.terminateProcess()) {
		outputStream << "Process killed" << endl;
	} else {
		throw DebuggerException("Failure to kill process");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(startAndHoldProcess) {
	if (debugger.process) {
		throw DebuggerException("The debugger is already attached to a process");
	}

	if (debugger.arguments.empty()) {
		throw DebuggerException("There is no binary. Use `file` to load a binary.");
	}

	if (!debugger.startProcess(true)) {
		throw DebuggerException("Failure starting process");
	}

	outputStream << debugger.arguments.front() << " started and suspended" << endl;
}

DYNDEBUGGER_DEBUGGER_COMMAND(runAndHoldProcess) {
	if (debugger.process) {
		throw DebuggerException("The debugger is already attached to a process");
	}

	if (debugger.arguments.empty()) {
		throw DebuggerException("There is no binary. Use `file` to load a binary.");
	}

	vector<string> splittedArguments = Parsers::splitString(arguments);
	if (!debugger.startProcess(splittedArguments, true)) {
		throw DebuggerException("Failure starting process");
	}

	outputStream << debugger.arguments.front() << " started and suspended" << endl;
}

DYNDEBUGGER_DEBUGGER_COMMAND(runProcess) {
	if (debugger.process) {
		throw DebuggerException("The debugger is already attached to a process");
	}

	if (debugger.arguments.empty()) {
		throw DebuggerException("There is no binary. Use `file` to load a binary.");
	}

	vector<string> splittedArguments = Parsers::splitString(arguments);
	if (!debugger.startProcess(splittedArguments, false)) {
		throw DebuggerException("Failure starting process");
	}

	outputStream << debugger.arguments.front() << " started" << endl;
}

DYNDEBUGGER_DEBUGGER_COMMAND(startProcess) {
	if (debugger.process) {
		throw DebuggerException("The debugger is already attached to a process");
	}

	if (debugger.arguments.empty()) {
		throw DebuggerException("There is no binary. Use `file` to load a binary.");
	}

	if (!debugger.startProcess(false)) {
		throw DebuggerException("Failure starting process");
	}

	outputStream << debugger.arguments.front() << " started" << endl;
}

DYNDEBUGGER_DEBUGGER_COMMAND(stepExecution) {
	throw DebuggerException("Doesn't work");
}

DYNDEBUGGER_DEBUGGER_COMMAND(stopProcessOrThread) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (debugger.process->isStopped() || debugger.process->stopExecution()) {
		outputStream << "Process stopped" << endl;
		debugger.storeCurrentStackFrame();
	} else {
		throw DebuggerException("Failure stopping process");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(switchToThread) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	try {
		THR_ID threadId = stoul(arguments);
		BPatch_thread *thread = debugger.process->getThread(threadId);
		if (!thread) {
			throw DebuggerException("The specified thread doesn't exist");
		}

		debugger.current_thread = threadId;
		debugger.storeCurrentStackFrame();

	} catch (invalid_argument& e) {
		throw DebuggerException("Invalid thread ID");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(selectFrame) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (!debugger.process->isStopped()) {
		throw DebuggerException("The process needs to be stopped");
	}

	try {
		debugger.storeCurrentStackFrame(stoul(arguments));
		BPatch_thread *thread = debugger.process->getThread(debugger.current_thread);
		vector<BPatch_frame> callStack;
		thread->getCallStack(callStack);

		if (debugger.currentFrameIndex > callStack.size()) {
			throw DebuggerException("Invalid frame index");
		}

		debugger.currentFrame = callStack[debugger.currentFrameIndex];
		debugger.currentFunction = debugger.currentFrame.findFunction();

		debugger.printBreakpointLocation(outputStream);

	} catch (invalid_argument&) {
		debugger.printBreakpointLocation(outputStream);
	}
}

} /* namespace Commands */
} /* namespace DynDebugger */
