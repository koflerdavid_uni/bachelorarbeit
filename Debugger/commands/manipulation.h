/*
 * manipulation.h
 *
 *  Created on: 18.02.2014
 *      Author: David Kofler
 */

#ifndef __COMMANDS_MANIPULATION_H_
#define __COMMANDS_MANIPULATION_H_

#include "../Debugger.h"

#include <string>
using namespace std;

namespace DynDebugger {
namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(setVariable);

DYNDEBUGGER_DEBUGGER_COMMAND(insertSetSnippet);

DYNDEBUGGER_DEBUGGER_COMMAND(callFunction);

DYNDEBUGGER_DEBUGGER_COMMAND(insertFunctionCall);

} /* namespace Commands */
} /* namespace DynDebugger */

#endif /* __COMMANDS_MANIPULATION_H_ */
