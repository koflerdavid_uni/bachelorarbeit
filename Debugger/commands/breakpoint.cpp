/*
 * breakpoint.cpp
 *
 *  Created on: 30.11.2013
 *      Author: David Kofler
 */

#include "breakpoint.h"

#include "../Breakpoint.h"
#include "../Location.h"
#include "../Debugger.h"
#include "../TablePrinter.h"
#include "../VariableLookupHelper.h"

#include "../parsers/CommandParser.h"
#include "../parsers/ExpressionParser.h"
#include "../parsers/LocationParser.h"
#include "../parsers/Tokenizer.h"

#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_image.h>
#include <dyninst/BPatch_point.h>
#include <dyninst/BPatch_process.h>
#include <dyninst/BPatch_snippet.h>

#include <readline/readline.h>
#include <readline/history.h>

#include <exception>
#include <string>
#include <utility>
using namespace std;

namespace DynDebugger {
namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(setBreakpoint) {
	if (debugger.process && !debugger.process->isStopped()) {
		throw DebuggerException("Only stopped processes can be breakpoint'ed");
	}

	// The `arguments` string is either empty or starts with a non-whitespace character.
	assert(arguments.size() == 0 || !isspace(arguments[0]));

	// And breakpoint specifications don't contain any whitespaces.
	string::size_type expressionBegin = 0;
	while (expressionBegin < arguments.size() && !isspace(arguments[expressionBegin])) {
		++expressionBegin;
	}

	string breakpointString;
	BPatch_snippet *condition = nullptr;
	if (expressionBegin == arguments.size()) {
		// There is no expression.
		breakpointString = arguments;

	} else {
		if (debugger.process == nullptr) {
			throw DebuggerException("Conditional breakpoints can currently only be "
					"created after the debuggee has been started.");
		}

		breakpointString.assign(arguments, 0, expressionBegin);

		try {
			VariableLookupHelper lookupHelper(debugger.process, debugger.currentFrame);
			condition = Parsers::parseExpression(arguments.substr(expressionBegin), lookupHelper);
		} catch (Parsers::ExpressionParserException&) {
		}

		if (condition == nullptr) {
			throw DebuggerException("Syntax or type error in condition");
		}
	}

	try {
		Location breakpoint = Parsers::parseLocation(breakpointString);

		if (debugger.process == nullptr) {
			// To be processed after starting the process
			assert(condition == nullptr);
			debugger.breakpointsToApply.insert( { debugger.nextBreakpointId++, breakpoint });

		} else {
			try {
				debugger.applySingleBreakpoint(Breakpoint(breakpoint, condition), debugger.nextBreakpointId++);
			} catch (DebuggerException& e) {
				--debugger.nextBreakpointId;
				throw e;
			}
		}

	} catch (Parsers::LocationParseException& e) {
		throw DebuggerException("Breakpoint parse error: " + e.getMessage());
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(deleteBreakpoints) {
	if (debugger.process && (!debugger.process->isTerminated() && !debugger.process->isStopped())) {
		throw DebuggerException("The process needs to be stopped");
	}

	vector<string> tokens = Parsers::splitString(arguments);

	vector<unsigned> breakpointsToDelete;
	if (debugger.process) {
		for (const string& token : tokens) {
			try {
				const unsigned which = stoul(token);
				if (debugger.currentBreakpoints.count(which) != 0) {
					breakpointsToDelete.push_back(which);
				}
			} catch (invalid_argument&) {
			}
		}

	} else {
		for (const string& token : tokens) {
			try {
				const unsigned which = stoul(token);
				if (debugger.breakpointsToApply.count(breakpointsToDelete.back()) != 0) {
					breakpointsToDelete.push_back(which);
				}
			} catch (invalid_argument&) {
			}
		}
	}

	for (unsigned breakpointId : breakpointsToDelete) {
		debugger.removeBreakpoint(breakpointId);
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(printBreakpoints) {
	TablePrinter tablePrinter( { "ID", "Location", "Conditional" });

	if (debugger.process) {
		assert(debugger.breakpointsToApply.empty());
		for (auto& it : debugger.currentBreakpoints) {
			tablePrinter.put(it.first);
			tablePrinter.put(it.second.first.location.toString());
			tablePrinter.put(it.second.first.isConditional());
		}

	} else {
		assert(debugger.currentBreakpoints.empty());
		for (auto& it : debugger.breakpointsToApply) {
			tablePrinter.put(it.first);
			tablePrinter.put(it.second.location.toString());
			tablePrinter.put(it.second.isConditional());
		}
	}

	tablePrinter.output(outputStream);
}

DYNDEBUGGER_DEBUGGER_COMMAND(addBreakpointActions) {
	if (debugger.process && (!debugger.process->isTerminated() && !debugger.process->isStopped())) {
		throw DebuggerException("The process needs to be stopped");
	}

	vector<string> tokens = Parsers::splitString(arguments);
	vector<unsigned> handles;
	for (string& s : tokens) {
		try {
			unsigned breakpointId = stoul(s);
			if ((debugger.process && debugger.currentBreakpoints.count(breakpointId) != 0)
					|| debugger.breakpointsToApply.count(breakpointId) != 0) {
				handles.push_back(breakpointId);
			}
		} catch (invalid_argument&) {
		}
	}

	if (tokens.size() != handles.size()) {
		throw DebuggerException("Some breakpoint IDs are invalid");
		return;
	}

	if (handles.size() == 0) {
		//By default, apply commands to the most recent breakpoint.
		if (debugger.currentBreakpoints.empty() && debugger.breakpointsToApply.empty()) {
			throw DebuggerException("No breakpoints defined yet");
		}

		unsigned int newest = 0;
		if (debugger.process) {
			assert(debugger.breakpointsToApply.empty());
			for (auto& breakpoint : debugger.currentBreakpoints) {
				newest = max(newest, breakpoint.first);
			}

		} else {
			assert(debugger.currentBreakpoints.empty());
			for (auto& breakpoint : debugger.breakpointsToApply) {
				newest = max(newest, breakpoint.first);
			}
		}

		handles.push_back(newest);
	}

	vector<pair<string, string>> commandsToRun;
	string input;
	while (readlineFunction(" # ", input)) {
		string command;
		string arguments;

		if (Parsers::parseCommand(input, command, arguments)) {
			if (command == "end") {
				break;
			}

			commandsToRun.emplace_back(command, arguments);
		}
	}

	for (unsigned breakpointID : handles) {
		if (debugger.process) {
			debugger.currentBreakpoints.find(breakpointID)->second.first.actionsInDebugger = commandsToRun;
		} else {
			debugger.breakpointsToApply.find(breakpointID)->second.actionsInDebugger = commandsToRun;
		}
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(setIgnoreCount) {
	vector<string> tokens = Parsers::splitString(arguments);
	if (tokens.size() < 2) {
		throw DebuggerException("Specify a breakpoint number and an ignore count");
	}

	try {
		unsigned breakpointNumber = stoul(tokens[0]);
		unsigned ignoreCount = stoul(tokens[1]);

		if (debugger.process) {
			auto result = debugger.currentBreakpoints.find(breakpointNumber);
			if (result == debugger.currentBreakpoints.end()) {
				throw DebuggerException("Specify a valid breakpoint number");
			}

			result->second.first.ignoreCount = ignoreCount;

		} else {
			auto result = debugger.breakpointsToApply.find(breakpointNumber);
			if (result == debugger.breakpointsToApply.end()) {
				throw DebuggerException("Specify a valid breakpoint number");
			}

			result->second.ignoreCount = ignoreCount;
		}

	} catch (invalid_argument&) {
		throw DebuggerException("Specify a valid breakpoint ID and an ignore count");
	}
}

} // namespace Commands
} // namespace DynDebugger
