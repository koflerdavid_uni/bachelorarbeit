/*
 * watchpoint.cpp
 *
 *  Created on: 01.12.2013
 *      Author: David Kofler
 */

#include "../Debugger.h"
#include "../parsers/Tokenizer.h"

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_snippet.h>

#include <dyninst/PCProcess.h>

#include <stdexcept>
#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {

namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(traceAccess) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	vector<string> parts = Parsers::splitString(arguments);

	if (parts.empty()) {
		throw DebuggerException("You have to provide a variable name");
	}

	try {
		BPatch_variableExpr *var = debugger.process->getImage()->findVariable(parts.front().c_str(), false);
		if (!var) {
			throw DebuggerException("Variable not found");
		}

		if (!var->getType()) {
			throw DebuggerException("Variable has no type information");
		}

		set<string> modulesToObserve;
		vector<string>::iterator it = parts.begin();
		modulesToObserve.insert(it, parts.end());

		// Prepare access types set for call to findPoint
		set<BPatch_opCode> accessTypes;
		accessTypes.insert(BPatch_opCode::BPatch_opLoad);
		accessTypes.insert(BPatch_opCode::BPatch_opStore);
//		accessTypes.insert(BPatch_opCode::BPatch_opPrefetch);

		std::vector<BPatch_snippet*> printfArgs;
		printfArgs.push_back(new BPatch_constExpr("Access at: %p.\n"));
		printfArgs.push_back(new BPatch_effectiveAddressExpr);
		std::vector<BPatch_function *> printfFuncs;
		debugger.process->getImage()->findFunction("printf", printfFuncs);

		void *afterEndOfObject = (char*) var->getBaseAddr() + var->getSize();
		BPatch_boolExpr greaterThanBase(BPatch_relOp::BPatch_ge, BPatch_effectiveAddressExpr(),
				BPatch_constExpr(var->getBaseAddr()));
		BPatch_boolExpr inVariableMemory(BPatch_relOp::BPatch_lt, BPatch_effectiveAddressExpr(),
				BPatch_constExpr(afterEndOfObject));
		BPatch_boolExpr condition(BPatch_relOp::BPatch_and, greaterThanBase, inVariableMemory);
		BPatch_funcCallExpr printfCall(*(printfFuncs[0]), printfArgs);
		BPatch_ifExpr found(condition, printfCall);

		const int nameBufferMaxLength = 40;
		char *nameBuffer = new char[nameBufferMaxLength];

		vector<BPatch_module *> *modules = debugger.process->getImage()->getModules();

		debugger.process->beginInsertionSet();
		for (vector<BPatch_module *>::iterator it = modules->begin(); it != modules->end(); ++it) {
			BPatch_module *module = *it;

			// Infix search for name
			module->getName(nameBuffer, nameBufferMaxLength);
			if (modulesToObserve.find(string(nameBuffer)) == modulesToObserve.end()) {
				continue;
			}

			vector<BPatch_function *> *functions = module->getProcedures();

			for (vector<BPatch_function *>::iterator it = functions->begin(); it != functions->end(); ++it) {
				BPatch_function *fun = *it;

				cout << "Function " << fun->getDemangledName() << endl;

				vector<BPatch_point *> *points = fun->findPoint(accessTypes);
				for (vector<BPatch_point *>::iterator it = points->begin(); it != points->end(); ++it) {
					BPatch_point *point = *it;
					//			cout << "Instance" << endl;
					//			const BPatch_memoryAccess *access = point->getMemoryAccess();
					debugger.process->insertSnippet(found, *point, BPatch_callBefore);
				}
			}
		}

		debugger.process->finalizeInsertionSet(true);

		delete nameBuffer;

	} catch (invalid_argument& e) {
		outputStream << "Invalid memory area specification" << endl;
	}
}

}

}
