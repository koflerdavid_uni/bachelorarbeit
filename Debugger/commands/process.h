/*
 * process.h
 *
 *  Created on: 01.12.2013
 *      Author: David Kofler
 */

#ifndef __COMMANDS_PROCESS_H_
#define __COMMANDS_PROCESS_H_

#include "../Debugger.h"

#include <dyninst/PCProcess.h>
using namespace Dyninst;

#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {

namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(attachToProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(continueProcess);

void detachFromProcess(Debugger& debugger);
DYNDEBUGGER_DEBUGGER_COMMAND(detachFromProcessCommand);

DYNDEBUGGER_DEBUGGER_COMMAND(loadBinaryFile);

DYNDEBUGGER_DEBUGGER_COMMAND(killProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(runAndHoldProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(startAndHoldProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(runProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(startProcess);

DYNDEBUGGER_DEBUGGER_COMMAND(stepExecution);

DYNDEBUGGER_DEBUGGER_COMMAND(stopProcessOrThread);

DYNDEBUGGER_DEBUGGER_COMMAND(switchToThread);

DYNDEBUGGER_DEBUGGER_COMMAND(selectFrame);

DYNDEBUGGER_DEBUGGER_COMMAND(waitForStop);

}

}

#endif /* __COMMANDS_PROCESS_H_ */
