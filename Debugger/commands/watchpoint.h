/*
 * watchpoint.h
 *
 *  Created on: 01.12.2013
 *      Author: David Kofler
 */

#ifndef __COMMANDS_WATCHPOINT_H_
#define __COMMANDS_WATCHPOINT_H_

#include "../Debugger.h"

#include <dyninst/BPatch_process.h>

#include <string>
using namespace std;

namespace DynDebugger {

namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(traceAccess);

}

}

#endif /* __COMMANDS_WATCHPOINT_H_ */
