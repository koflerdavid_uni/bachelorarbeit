/*
 * information.h
 *
 *  Created on: 01.12.2013
 *      Author: David Kofler
 */

#ifndef __COMMANDS_INFORMATION_H_
#define __COMMANDS_INFORMATION_H_

#include "../Debugger.h"

#include <string>
using namespace std;

namespace DynDebugger {

namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(infoProgram);

DYNDEBUGGER_DEBUGGER_COMMAND(printExpression);

DYNDEBUGGER_DEBUGGER_COMMAND(printFunctions);

DYNDEBUGGER_DEBUGGER_COMMAND(printTypes);

DYNDEBUGGER_DEBUGGER_COMMAND(printStacktrace);

DYNDEBUGGER_DEBUGGER_COMMAND(printModules);

DYNDEBUGGER_DEBUGGER_COMMAND(printThreads);

DYNDEBUGGER_DEBUGGER_COMMAND(printLocalVariablesAndParameters);

DYNDEBUGGER_DEBUGGER_COMMAND(printVariables);

DYNDEBUGGER_DEBUGGER_COMMAND(findFunction);

DYNDEBUGGER_DEBUGGER_COMMAND(findType);

DYNDEBUGGER_DEBUGGER_COMMAND(findVariable);

DYNDEBUGGER_DEBUGGER_COMMAND(showMutateeArguments);

DYNDEBUGGER_DEBUGGER_COMMAND(showPid);

}

}

#endif /* __COMMANDS_INFORMATION_H_ */
