/*
 * breakpoint.h
 *
 *  Created on: 30.11.2013
 *      Author: David Kofler
 */

#ifndef __COMMANDS_BREAKPOINT_H_
#define __COMMANDS_BREAKPOINT_H_

#include "../Location.h"
#include "../Debugger.h"

#include <dyninst/PCProcess.h>

using namespace Dyninst;

#include <vector>
using namespace std;

namespace DynDebugger {
namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(setBreakpoint);

DYNDEBUGGER_DEBUGGER_COMMAND(deleteBreakpoints);

DYNDEBUGGER_DEBUGGER_COMMAND(printBreakpoints);

DYNDEBUGGER_DEBUGGER_COMMAND(addBreakpointActions);

DYNDEBUGGER_DEBUGGER_COMMAND(setIgnoreCount);

} // namespace Commands
} // namespace DynDebugger

#endif /* __COMMANDS_BREAKPOINT_H_ */
