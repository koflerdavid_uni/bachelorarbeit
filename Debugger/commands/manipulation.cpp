/*
 * manipulation.cpp
 *
 *  Created on: 18.02.2014
 *      Author: David Kofler
 */

#include "manipulation.h"

#include "../Debugger.h"
#include "../Location.h"
#include "../VariableLookupHelper.h"
#include "../parsers/ExpressionParser.h"
#include "../parsers/LocationParser.h"
#include "../parsers/SetStatementParser.h"
#include "../parsers/Tokenizer.h"

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_point.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <memory>
#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {
namespace Commands {

DYNDEBUGGER_DEBUGGER_COMMAND(setVariable) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (!debugger.process->isStopped()) {
		throw DebuggerException("The process needs to be stopped");
	}

	VariableLookupHelper lookupHelper(debugger.process, debugger.currentFrame);
	BPatch_arithExpr *snippet = Parsers::parseSetStatement(lookupHelper, arguments);
	if (!snippet) {
		throw DebuggerException("Illegal snippet (syntax error or not a well-typed expression)");
	}

	bool error;
	debugger.process->oneTimeCode(*snippet, &error);
	if (error) {
		throw DebuggerException("Error setting the variable");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(insertSetSnippet) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (!debugger.process->isStopped()) {
		throw DebuggerException("The process needs to be stopped");
	}

	vector<string> tokens = Parsers::splitString(arguments, 1);
	if (tokens.size() != 2) {
		throw DebuggerException("Not enough arguments");
	}

	try {
		Location location = Parsers::parseLocation(tokens[0]);
		BPatch_arithExpr *snippetPtr = nullptr;

		try {
			VariableLookupHelper lookupHelper(debugger.process, debugger.currentFrame);
			snippetPtr = Parsers::parseSetStatement(lookupHelper, tokens[1]);
		} catch (Parsers::ExpressionParserException& e) {
		}

		if (!snippetPtr) {
			throw DebuggerException("Illegal snippet (syntax error or not a well-typed expression)");
		}

		// Ensures that the snippet gets deleted at the end
		unique_ptr<BPatch_arithExpr> snippet(snippetPtr);

		vector<BPatch_point *> points = location.lookupPoints(debugger.process);
		// Insert the snippet after existing snippets.
		BPatchSnippetHandle *handle = debugger.process->insertSnippet(*snippet, points,
				BPatch_snippetOrder::BPatch_lastSnippet);
		if (!handle) {
			throw DebuggerException("Illegal point");
		}

	} catch (Parsers::LocationParseException& e) {
		throw DebuggerException("Location parse exception: " + e.getMessage());
	} catch (LocationLookupException& e) {
		throw InvalidBreakpointException(e.getLocation());
	}
}

BPatch_function *findFunction(BPatch_addressSpace *addrspace, const string& name) {
	BPatch_module *module = nullptr;
	vector<BPatch_function *> functions;

	string::size_type colonPos = name.find(':');
	if (colonPos != string::npos) {
		string functionName = name.substr(colonPos + 1);
		module = addrspace->getImage()->findModule(functionName.substr(0, colonPos).c_str(), true);
		if (!module) {
			return nullptr;
		}

		module->findFunction(name.c_str(), functions, false);
	} else {
		addrspace->getImage()->findFunction(name.c_str(), functions, false);
	}

	if (functions.empty()) {
		return nullptr;
	}

	return functions.front();
}

bool isWhite(const string& s) {
	string whitespaces(" \t\n");
	for (char c : s) {
		if (whitespaces.find(c) != string::npos) {
			return false;
		}
	}

	return true;
}

DYNDEBUGGER_DEBUGGER_COMMAND(callFunction) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (!debugger.process->isStopped()) {
		throw DebuggerException("The process needs to be stopped");
	}

	vector<string> tokens = Parsers::splitString(arguments, 1);
	assert(tokens.size() == 2);
	BPatch_function *function = findFunction(debugger.process, tokens.front());

	VariableLookupHelper lookupHelper(debugger.process, debugger.currentFrame);
	vector<BPatch_snippet *> argumentsForCall;

	string::size_type next;
	string nextString = tokens[1];

	while (!isWhite(nextString)) {
		BPatch_snippet *snippet = Parsers::parseExpression(nextString, lookupHelper, next);
		if (!snippet) {
			break;
		}

		argumentsForCall.push_back(snippet);
		if (next >= nextString.size()) {
			break;
		}

		nextString = nextString.substr(next);
	}

	BPatch_funcCallExpr call(*function, argumentsForCall);
	bool error;
	debugger.process->getThread(debugger.current_thread)->oneTimeCode(call, &error);
	if (error) {
		throw DebuggerException("Function call failed");
	}
}

DYNDEBUGGER_DEBUGGER_COMMAND(insertFunctionCall) {
	if (!debugger.process) {
		throw DebuggerException("No process");
	}

	if (!debugger.process->isStopped()) {
		throw DebuggerException("The process needs to be stopped");
	}

	vector<string> tokens = Parsers::splitString(arguments, 2);
	assert(tokens.size() == 3);

	Location location = Parsers::parseLocation(tokens.front());

	BPatch_function *function = findFunction(debugger.process, tokens[1]);

	VariableLookupHelper lookupHelper(debugger.process, debugger.currentFrame);
	vector<BPatch_snippet *> argumentsForCall;

	string::size_type next;
	string nextString = tokens[1];

	while (!isWhite(nextString)) {
		argumentsForCall.push_back(Parsers::parseExpression(nextString, lookupHelper, next));
	}

	BPatch_funcCallExpr call(*function, argumentsForCall);

	vector<BPatch_point *> points = location.lookupPoints(debugger.process);
	BPatchSnippetHandle *handle = debugger.process->insertSnippet(call, points);
	if (!handle) {
		throw DebuggerException("Inserting call failed");
	}
}

} /* namespace Commands */
} /* namespace DynDebugger */
