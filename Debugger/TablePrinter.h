/*
 * tableprinter.h
 *
 *  Created on: 20.12.2013
 *      Author: David Kofler
 */

#ifndef TABLEPRINTER_H_
#define TABLEPRINTER_H_

#include <initializer_list>
#include <ostream>
#include <string>
#include <vector>

using namespace std;
namespace DynDebugger {

class NoColumnsException {
};

/**
 * Helper class to produce tabular output. It takes a list of column names and is then fed with the data.
 * the method #output(ostream&) then prints the data to the stream and builds a nice table for it.
 */
class TablePrinter {
public:
	TablePrinter(initializer_list<string> &columns) throw (NoColumnsException);
	TablePrinter(const vector<string> &columns) throw (NoColumnsException);
	~TablePrinter();

	void put(const string& data);
	void put(const char *data);
	void put(unsigned int number);
	void putAddress(void *address);
	void put(int number);
	void put(bool value);

	bool atEndOfRow() const;

	void output(ostream& stream, bool noHeaderIfEmpty = true) const;

protected:
	void put(const string* data);

	vector<string> columns;
	vector<const string *> fields;
	vector<unsigned int> widths;
	vector<const string *> toDelete;
};

}

#endif /* TABLEPRINTER_H_ */
