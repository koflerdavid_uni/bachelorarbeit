/*
 * DyninstHelpers.cpp
 *
 *  Created on: 02.04.2014
 *      Author: David Kofler
 */

#include "DyninstHelpers.h"

#include "utility.h"

// SymtabAPI
// Always include Symtab.h before other SymtabAPI headers.
#include <dyninst/Symtab.h>
#include <dyninst/Function.h>
#include <dyninst/Type.h>

// DyninstAPI
//#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <iomanip>
#include <unordered_set>
using namespace std;

namespace DynDebugger {

void printFunction(BPatch_function *fun, bool printModuleName, ostream& stream) {
	stream << fun->getDemangledName() << '(';

	vector<BPatch_localVar *> *parameters = fun->getParams();
	vector<BPatch_localVar *>::iterator parameterIterator = parameters->begin();

	if (parameterIterator != parameters->end()) {
		BPatch_localVar *var = *parameterIterator;
		stream << var->getName() << " : " << *var->getType();

		for (++parameterIterator; parameterIterator != parameters->end(); ++parameterIterator) {
			BPatch_localVar *var = *parameterIterator;
			stream << ", " << var->getName() << " : " << *var->getType();
		}
	}

	stream << ") : ";

	if (fun->getReturnType()) {
		stream << *fun->getReturnType();
	} else {
		stream << "void";
	}

	stream << endl;

	vector<string> names;
	fun->getNames(names);
	if (names.size() > 1) {
		stream << " alias:" << endl;
		for (vector<string>::iterator currentName = names.begin(); currentName != names.end(); ++currentName) {
			if (*currentName == fun->getName()) {
				continue;
			}

			stream << '\t' << *currentName << endl;
		}
	}
}

void printFunction(SymtabAPI::Function *fun, bool printModuleName, ostream& stream) {
	const vector<string>& typedNames = fun->getAllTypedNames();

	if (printModuleName) {
		stream << fun->getModule()->fileName() << ": " << fun->getPtrOffset();
	}

	vector<string>::const_iterator nameIterator = typedNames.begin();
	string firstName = *nameIterator;
	stream << firstName;

	vector<SymtabAPI::localVar *> parameters;
	fun->getParams(parameters);

	stream << '(';

	vector<SymtabAPI::localVar *>::iterator parameterIterator = parameters.begin();

// Format function signature.
	if (parameterIterator != parameters.end()) {
		SymtabAPI::localVar *var = *parameterIterator;
		stream << var->getName() << " : " << var->getType();

		for (++parameterIterator; parameterIterator != parameters.end(); ++parameterIterator) {
			var = *parameterIterator;
			stream << ", " << var->getName() << " : " << var->getType();
		}
	}

	stream << ") : ";

	if (fun->getReturnType() != nullptr) {
		stream << fun->getReturnType();
	} else {
		stream << "void";
	}

	stream << endl;

	for (++nameIterator; nameIterator != typedNames.end(); ++nameIterator) {
		stream << "\talias " << *nameIterator << endl;
	}
}

string formatType(const BPatch_type *type) {
	return formatType(SymtabAPI::convert(type));
}

string formatFunctionType(SymtabAPI::typeFunction *functionType) {
	string ret = formatType(functionType->getReturnType());
	ret.push_back('(');

	vector<SymtabAPI::Type *>& parameters = functionType->getParams();
	if (!parameters.empty()) {
		auto parameterIterator = parameters.begin();
		ret += formatType(*parameterIterator);

		for (auto end = parameters.end(); parameterIterator != end; ++parameterIterator) {
			ret += ", ";
			ret += formatType(*parameterIterator);
		}
	}

	ret.push_back(')');
	return ret;
}

string formatType(SymtabAPI::Type *type) {
	string ret = "?";

	if (type) {
		switch (type->getDataClass()) {
			case SymtabAPI::dataClass::dataArray:
				ret = formatType(type->getArrayType()->getBaseType());
				ret.append("[]");
				break;

			case SymtabAPI::dataClass::dataEnum:
				ret = "enum ";
				ret += type->getEnumType()->getName();
				break;

			case SymtabAPI::dataClass::dataFunction:
				ret = formatFunctionType(type->getFunctionType());
				break;

			case SymtabAPI::dataClass::dataPointer:
				ret = formatType(type->getPointerType()->getConstituentType());
				ret.push_back('*');
				break;

			case SymtabAPI::dataClass::dataReference:
				ret = formatType(type->getRefType()->getConstituentType());
				ret.push_back('&');
				break;

			case SymtabAPI::dataClass::dataStructure:
				ret = "struct ";
				ret += type->getStructType()->getName();
				break;

			case SymtabAPI::dataClass::dataUnion:
				ret = "union ";
				ret += type->getUnionType()->getName();
				break;

			default:
				ret = type->getName();
		}
	}

	return ret;
}

bool printValue(BPatch_variableExpr& expr, ostream& stream) {
	SymtabAPI::Type *type = expr.getType()->getSymtabType();
	size_t size = type->getSize();
	bool error = true;

	void *result = operator new(size);
	if (expr.readValue(result, size)) {
		printValue(result, type, stream);
		stream << endl;
	} else {
		error = false;
	}

	operator delete(result);
	return error;
}

#define OFFSET(base, offset) ((void *) &( ((char *) base) [ (offset) ]) )

bool printValueOfArrayType(void* result, SymtabAPI::typeArray* type, ostream& stream, unsigned depth, unsigned level);
bool printValueOfEnumType(void* result, SymtabAPI::typeEnum* type, ostream& stream, unsigned depth,
		unsigned indentation);
bool printValueOfStructuredType(void* result, SymtabAPI::Type* type, ostream& stream, unsigned depth, unsigned level);

bool printValue(void *result, BPatch_type *type, ostream& stream, unsigned depth, unsigned level) {
	return printValue(result, SymtabAPI::convert(type), stream, depth, level);
}

bool printValue(void *result, SymtabAPI::Type *type, ostream& stream, unsigned depth, unsigned level) {
	static const unordered_set<string> signedIntegralTypes = { "int", "long", "long int", "long long int", "short",
			"short int" };
	static const unordered_set<string> unsignedIntegralTypes = { "bool", "unsigned char", "unsigned", "unsigned int",
			"unsigned long", "unsigned long int", "unsigned long long", "unsigned long long int", "unsigned short",
			"unsigned short int" };
	static const unordered_set<string> charTypes = { "char", "signed char" };

	while (type->getDataClass() == SymtabAPI::dataClass::dataTypedef) {
		type = type->getTypedefType()->getConstituentType();
	}

	size_t size = type->getSize();
	string typeName = type->getName();

	if (signedIntegralTypes.count(typeName)) {
		if (size > sizeof(long long)) {
			return false;
		}

		if (size == 1) {
			stream << *((int8_t *) result);
		} else if (size == 2) {
			stream << *((int16_t *) result);
		} else if (size == 4) {
			stream << *((int32_t *) result);
		} else if (size >= 8) {
			stream << *((int64_t *) result);
		}

	} else if (unsignedIntegralTypes.count(typeName)) {
		if (size > sizeof(unsigned long long)) {
			return false;
		}

		if (size == 1) {
			stream << *((uint8_t *) result);
		} else if (size == 2) {
			stream << *((uint16_t *) result);
		} else if (size == 4) {
			stream << *((uint32_t *) result);
		} else if (size >= 8) {
			stream << *((uint64_t *) result);
		}

	} else if (charTypes.count(typeName) && size == sizeof(char)) {
		stream << *((char *) result) << endl;

	} else if (type->getDataClass() == SymtabAPI::dataClass::dataPointer) {
		stream << "(" << formatType(type->getPointerType()->getConstituentType()) << " *) "
				<< Utility::to_string(result) << endl;

	} else if ((type->getDataClass() == SymtabAPI::dataClass::dataStructure
			|| type->getDataClass() == SymtabAPI::dataClass::dataUnion) && depth > 0) {
		printValueOfStructuredType(result, type, stream, depth, level);

	} else if (type->getDataClass() == SymtabAPI::dataClass::dataArray) {
		return printValueOfArrayType(result, type->getArrayType(), stream, depth, level);

	} else if (type->getDataClass() == SymtabAPI::dataClass::dataEnum) {
		return printValueOfEnumType(result, type->getEnumType(), stream, depth, level);

	} else {
		stream << "Result of type " << formatType(type);
	}

	return true;
}

bool printValueOfArrayType(void* result, SymtabAPI::typeArray* type, ostream& stream, unsigned depth, unsigned level) {
	bool successful = true;

	stream << "[";
	auto arrayType = type->getArrayType();
	if (arrayType->getLow() != arrayType->getHigh()) {
		auto baseType = arrayType->getBaseType();
		unsigned long baseTypeSize = baseType->getSize();

		stream << '\n' << string(level + 1, '\t');
		successful &= printValue(OFFSET(result, arrayType->getLow() * baseTypeSize), baseType, stream, level + 1);

		unsigned long high = arrayType->getHigh();
		for (unsigned long i = arrayType->getLow() + 1; i < high; ++i) {
			stream << ",\n" << string(level + 1, '\t');
			successful &= printValue(OFFSET(result, i * baseTypeSize), baseType, stream, depth - 1, level + 1);
		}

		stream << ' ';
	}

	stream << "]";

	return successful;
}

bool printValueOfEnumType(void* result, SymtabAPI::typeEnum* enumType, ostream& stream, unsigned depth,
		unsigned indentation) {
	auto constants = enumType->getConstants();
	int value = *(int*) (result);
	auto pos = find_if(constants.begin(), constants.end(), [value](pair<string,int> item) {
		return item.second == value;
	});
	if (pos == constants.end()) {
		stream << '(' << enumType->getName() << ") " << value;
	} else {
		stream << enumType->getName() << "::" << pos->first << " (" << value << ")";
	}

	return true;
}

bool printValueOfStructuredType(void* result, SymtabAPI::Type* type, ostream& stream, unsigned depth, unsigned level) {
	bool successful = true;

	SymtabAPI::fieldListType* structuredType = type->getStructType();
	if (structuredType == nullptr) {
		structuredType = type->getUnionType();
	}
	stream << structuredType->getName() << " {";
	auto fields = structuredType->getFields();
	if (!fields->empty()) {
		stream << '\n' << string(level + 1, '\t') << fields->front()->getName() << " : ";
		successful &= printValue(OFFSET(result, fields->front()->getOffset()), fields->front()->getType(), stream,
				depth - 1, level + 1);

		auto end = fields->end();
		for (auto field = fields->begin() + 1; field != end; ++field) {
			if ((*field)->getOffset() != -1) {
				stream << ",\n" << string(level + 1, '\t');
				successful &= printValue(OFFSET(result, (*field)->getOffset()), (*field)->getType(), stream, depth - 1,
						level + 1);
			}
		}

		stream << ' ';
	}
	stream << '}';

	return successful;
}

ostream& operator<<(ostream& ostream, const BPatch_type& type) {
	return ostream << formatType(&type);
}

ostream & operator<<(ostream & ostream, SymtabAPI::Type & type) {
	return ostream << formatType(&type);
}

}  // namespace DynDebugger
