enable_testing()

add_executable(test_Tokenizer parsers/test_Tokenizer.cpp parsers/Tokenizer.cpp)

add_test(Tokenizer-test test_Tokenizer)