# CMake module to locate Dyninst and its components.

# This module was inspired by the module used by the CallPath project at
# https://github.com/scalability-llnl/callpath/
# which also uses Dyninst

include(FindPackageHandleStandardArgs)
include(FindPackageMessage)

#
# Utility functions
#
function(find_path_with_hints package header)
  find_path(${package}_INCLUDE_DIR ${header}
    PATH_SUFFIXES include
    HINTS $ENV{${package}_DIR} ${${package}_DIR} $ENV{${package}_INCLUDE_DIR} ${${package}_INCLUDE_DIR})
endfunction()

function(find_library_with_hints package lib)
  find_library(${package}_LIBRARY ${lib}
    PATH_SUFFIXES lib lib64
    HINTS $ENV{${package}_DIR} ${${package}_DIR} $ENV{${package}_LIB_DIR} ${${package}_LIB_DIR})
endfunction()

find_library(IBERTY_LIBRARY iberty)

find_path_with_hints(ELF libelf.h)
find_library_with_hints(ELF elf)

find_path_with_hints(DWARF libdwarf.h)
find_library_with_hints(DWARF dwarf)

find_path_with_hints(DYNINSTAPI dyninst/BPatch.h)
find_path_with_hints(SYMTABAPI dyninst/Symtab.h)
find_path_with_hints(STACKWALKER dyninst/walker.h)

find_library(COMMON_LIBRARY common
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(DYNC_API_LIBRARY dynC_API
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(DYNINSTAPI_LIBRARY dyninstAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})
  
find_library(INSTRUCTION_LIBRARY instructionAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(PARSEAPI_LIBRARY parseAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(PATCHAPI_LIBRARY patchAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})
  
find_library(PCONTROL_LIBRARY pcontrolAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(STACKWALKER_LIBRARY stackwalk
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

find_library(SYMTAB_LIBRARY symtabAPI
  PATH_SUFFIXES lib lib64 lib/dyninst lib64/dyninst
  HINTS $ENV{Dyninst_DIR} ${Dyninst_DIR})

set(DYNINST_LIBRARIES ${DYNINSTAPI_LIBRARY} ${SYMTAB_LIBRARY} ${STACKWALKER_LIBRARY} ${COMMON_LIBRARY})
set(DYNINST_INCLUDE_DIRS ${DYNINSTAPI_INCLUDE_DIR} ${SYMTABAPI_INCLUDE_DIR} ${STACKWALKER_INCLUDE_DIR} ${COMMON_LIBRARY_INCLUDE_DIR})

if (Dyninst_FIND_REQUIRED)
    find_package_handle_standard_args(Dyninst 
        REQUIRED_VARS COMMON_LIBRARY DYNINSTAPI_LIBRARY SYMTAB_LIBRARY STACKWALKER_LIBRARY)
else()
    if(DEFINED COMMON_LIBRARY AND DEFINED DYNINSTAPI_LIBRARY AND DEFINED SYMTAB_LIBRARY AND DEFINED STACKWALKER_LIBRARY)
        set(DYNINST_FOUND ON)
    endif()
endif(Dyninst_FIND_REQUIRED)