/*
 * Breakpoint.cpp
 *
 *  Created on: 03.03.2014
 *      Author: David Kofler
 */

#include "Breakpoint.h"

namespace DynDebugger {

Breakpoint::Breakpoint(const Location& _location, BPatch_snippet *_condition, unsigned _ignoreCount) :
		location(_location), condition(_condition), ignoreCount(_ignoreCount) {
}

Breakpoint::~Breakpoint() {
}

} // namespace DynDebugger
