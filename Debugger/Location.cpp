/*
 * Location.cpp
 *
 *  Created on: 04.07.2013
 *      Author: David Kofler
 */

#include "Location.h"
#include "utility.h"

#include <string>

#include <cctype>
#include <cstdlib>
#include <cstring>
using namespace std;

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_flowGraph.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_image.h>
#include <dyninst/BPatch_module.h>
#include <dyninst/BPatch_point.h>
#include <dyninst/BPatch_statement.h>
using namespace Dyninst;

namespace DynDebugger {

Location::Location(const string& _symbol) :
		file(string("")), symbol(_symbol), _hasLineNumber(false), lineNumber(0) {
}

Location::Location(BPatch_statement& statement) :
		Location(statement.fileName(), (statement.lineNumber() >= 0) ? ((unsigned) statement.lineNumber()) : 0) {
}

Location::Location(const string& _file, const string& _symbol) :
		file(_file), symbol(_symbol), _hasLineNumber(false), lineNumber(0) {
}

Location::Location(const string& _file, unsigned int _lineNumber) :
		file(_file), _hasLineNumber(true), lineNumber(_lineNumber) {
}

string Location::toString() const {
	string repr;

	if (file.length() > 0) {
		repr += "file: ";
		repr += file;
		repr += ", ";
	}

	if (symbol.length() > 0) {
		repr += "function: ";
		repr += symbol;
	}

	// apply breakpoint to process
	if (_hasLineNumber) {
		repr += "line number: ";
		repr += to_string(lineNumber);
	}

	return repr;
}

vector<BPatch_point *> Location::lookupPoints(BPatch_addressSpace *addressSpace) const {
	BPatch_image *image = addressSpace->getImage();
	vector<BPatch_point *> points;
	BPatch_module *module = nullptr;

	if (getFile().size() > 0) {
		module = image->findModule(getFile().c_str());
		if (module == nullptr) {
			throw LocationLookupException(*this);
		}
	}

	if (hasLineNumber()) {
		assert(module != nullptr);
		typedef pair<unsigned long int, unsigned long int> AddressRange;
		// Apply breakpoint at specified line
		vector<AddressRange> ranges;
		if (!addressSpace->getImage()->getAddressRanges(getFile().c_str(), getLineNumber(), ranges)) {
			throw LocationLookupException(*this);
		}

		for (AddressRange range : ranges) {
			auto before = points.size();

			if (!module->findPoints(range.first, points) || points.size() == before) {
				throw LocationLookupException(*this,
						Utility::to_string((void *) range.first) + ": Address belongs to no function");
			}
		}

	} else {
		vector<BPatch_function *> funcs;

		if (getFile().size() > 0) {
			// Look for symbol, i.e. interpret it as a function name in a module
			assert(module != nullptr);
			module->findFunction(getSymbol().c_str(), funcs, false);

		} else {
			// No file specified. Look for symbol everywhere
			image->findFunction(getSymbol().c_str(), funcs, false);
		}

		if (funcs.size() == 0) {
			throw LocationLookupException(*this);
		}

		for (BPatch_function *fun : funcs) {
			vector<BPatch_basicBlock *> entryBlocks;
			fun->getCFG()->getEntryBasicBlock(entryBlocks);
			assert(entryBlocks.size() == 1);

			vector<BPatch_point *> pointsInFirstBlock;
			entryBlocks.front()->getAllPoints(pointsInFirstBlock);
			assert(pointsInFirstBlock.size() > 1);
			points.push_back(pointsInFirstBlock[1]);
		}
	}

	return points;
}

ostream& operator<<(ostream& stream, const Location& breakpoint) {
	return stream << breakpoint.toString();
}

LocationLookupException::LocationLookupException(const Location& _location, const string& errorMessage) :
		invalid_argument(_location.toString() + ": " + errorMessage), location(_location) {
}

} // namespace DynDebugger
