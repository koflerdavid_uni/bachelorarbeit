/*
 * VariableLookupHelper.cpp
 *
 *  Created on: 28.03.2014
 *      Author: David Kofler
 */

#include "VariableLookupHelper.h"

#include <dyninst/BPatch_frame.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_point.h>
using namespace Dyninst;

#include <algorithm>
#include <stdexcept>
#include <vector>
using namespace std;

namespace DynDebugger {

VariableLookupHelper::VariableLookupHelper(BPatch_addressSpace* _addressSpace, BPatch_frame _currentFrame) :
		addressSpace(_addressSpace), currentFrame(_currentFrame), currentFunction(currentFrame.findFunction()) {
}

VariableLookupHelper::~VariableLookupHelper() {
}

BPatch_snippet *fromLocalVariable(BPatch_addressSpace *addrspace, BPatch_variableExpr *variable, void *fp,
		BPatch_localVar *localVar) {
	vector<BPatch_register> registers;
	bool result = addrspace->getRegisters(registers);
	assert(result == true);

	switch (localVar->getStorageClass()) {
		case BPatch_storageClass::BPatch_storageFrameOffset:
			// return fp[addr]
			return addrspace->createVariable(variable->getName(),
					(Dyninst::Address) (&((char *) fp)[localVar->getFrameOffset()]),
					const_cast<BPatch_type *>(variable->getType()));

		case BPatch_storageClass::BPatch_storageAddr:
			// return *addr
			// Nothing to do; the correct place is already referenced by the variable.
			return variable;

		case BPatch_storageClass::BPatch_storageReg:
			// return register
			// Problem: no type information available
			// return new BPatch_registerExpr(MachRegister(localVar->getRegister()));
			break;

		case BPatch_storageClass::BPatch_storageRegRef:
			// return *register
			// Problem: no type information available
			// return new BPatch_arithExpr(BPatch_unOp::BPatch_deref,
			//         BPatch_registerExpr(MachRegister(localVar->getRegister())));
			break;

		case BPatch_storageClass::BPatch_storageRegOffset:
			// return register[offset]
			if (localVar->getRegister() == 4) {
				// 4 is for the rsp register. Utterly not portable, but necessary.
				return addrspace->createVariable(localVar->getName(),
						(Dyninst::Address) (&((char *) fp)[localVar->getFrameOffset()]),
						const_cast<BPatch_type *>(variable->getType()));
			}

			// Problem: no type information available
			// return new BPatch_arithExpr(BPatch_binOp::BPatch_ref,
			//         BPatch_registerExpr(MachRegister(localVar->getRegister())),
			//         BPatch_constExpr(localVar->getFrameOffset()));
			break;

		case BPatch_storageClass::BPatch_storageAddrRef:
			// return **addr
			// Problem: no type information available
			// return new BPatch_arithExpr(BPatch_unOp::BPatch_deref, *variable);
			break;
	}

	return nullptr;
}

BPatch_snippet *VariableLookupHelper::operator ()(const string& name) {
	auto it = cache.find(name);
	if (it != cache.end()) {
		return it->second;
	}

	vector<BPatch_point *> points;
	BPatch_variableExpr *variable = nullptr;

	if (!currentFunction) {
		return nullptr;
	}

	static_assert(sizeof(Dyninst::Address) >= sizeof(void *), "Pointers should be convertible to Dyninst::Address");

	vector<BPatch_localVar *> *parameters = currentFunction->getParams();
	vector<BPatch_localVar *> *localVariables = currentFunction->getVars();

	auto predicate = [name](BPatch_localVar *var) {
		return var->getName() == name;
	};

	auto pos = find_if(localVariables->begin(), localVariables->end(), predicate);

	if (pos == localVariables->end()) {
		pos = find_if(parameters->begin(), parameters->end(), predicate);

		if (pos == parameters->end()) {
			variable = addressSpace->getImage()->findVariable(name.c_str(), false);
		} else {
			auto var = *pos;
			variable = addressSpace->createVariable(name,
					(Dyninst::Address) currentFrame.getFP() + var->getFrameOffset(), var->getType());
		}

	} else {
		auto var = *pos;
		variable = addressSpace->createVariable(name, (Dyninst::Address) currentFrame.getFP() + var->getFrameOffset(),
				var->getType());
	}

	if (variable) {
		while (variable->getType()->getDataClass() == BPatch_dataClass::BPatch_dataTypeDefine) {
			variable->setType(variable->getType()->getConstituentType());
		}
	}

	cache[name] = variable;
	return variable;
}

} /* namespace DynDebugger */
