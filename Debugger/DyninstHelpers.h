/*
 * DyninstHelpers.h
 *
 *  Created on: 02.04.2014
 *      Author: David Kofler
 */

#ifndef DYNINSTHELPERS_H_
#define DYNINSTHELPERS_H_

// Always include Symtab.h before other SymtabAPI headers.
#include <dyninst/Symtab.h>
#include <dyninst/Function.h>
#include <dyninst/Type.h>

#include <dyninst/BPatch_function.h>
using namespace Dyninst;

#include <ostream>

namespace DynDebugger {

void printFunction(BPatch_function *fun, bool printModuleName, std::ostream& stream);
void printFunction(SymtabAPI::Function *fun, bool printModuleName, std::ostream& stream);

string formatType(const BPatch_type* type);
string formatType(SymtabAPI::Type* type);

bool printValue(BPatch_variableExpr& expr, std::ostream& stream);
bool printValue(void *result, SymtabAPI::Type *type, ostream& stream, unsigned depth=10, unsigned level=0);
bool printValue(void *result, BPatch_type *type, ostream& stream, unsigned depth=10, unsigned level=0);

std::ostream& operator<<(std::ostream&, const BPatch_type&);
std::ostream& operator<<(std::ostream&, const SymtabAPI::Type&);

}  // namespace DynDebugger

#endif /* DYNINSTHELPERS_H_ */
