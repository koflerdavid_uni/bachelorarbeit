/*
 * CommandTable.h
 *
 *  Created on: 02.04.2014
 *      Author: David Kofler
 */

#ifndef COMMANDS_H_
#define COMMANDS_H_

#include <functional>
#include <stdexcept>
#include <unordered_map>
using namespace std;

namespace DynDebugger {

class Debugger;

#define DYNDEBUGGER_DEBUGGER_COMMAND(name) \
	void name(Debugger& debugger, \
			ostream& outputStream, \
			std::function<bool(const string&, string&)> readlineFunction, \
			string& arguments)

typedef function<void(Debugger&, ostream&, std::function<bool(const string&, string&)>, string&)> DebuggerCommand;

class CommandTable {
public:
	static DebuggerCommand lookupCommand(const string& name);

private:
	CommandTable();

	/**
	 * Build the command table; it associates command names with the functions which implement them.
	 */
	static void initClass();

	class Initializer {
	public:
		Initializer();
	};

	friend class Initializer;

	static unordered_map<string, DebuggerCommand> commandTable;
	friend DYNDEBUGGER_DEBUGGER_COMMAND(printHelp);
};

class NoSuchCommandException: public invalid_argument {
public:
	NoSuchCommandException(const string& name);
};

} /* namespace DynDebugger */

#endif /* COMMANDS_H_ */
