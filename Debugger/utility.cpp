/*
 * utility.cpp
 *
 *  Created on: 02.03.2014
 *      Author: David Kofler
 */

#include "config.h"

#include "utility.h"

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstdint>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <cstring>
#include <string>
using namespace std;

#include <netinet/tcp.h>
#include <unistd.h>

namespace DynDebugger {
namespace Utility {

void pipeCreate(int pipefds[2]) {
	if (pipe(pipefds) == -1) {
		throw runtime_error(strerror(errno));
	}
}

string escapeString(string source) {
	static const string toEscape(" \n\t\r\\");
	string escapedString;

	for (char c : source) {
		if (toEscape.find(c) != string::npos) {
			escapedString.push_back('\\');
		}

		escapedString.push_back(c);
	}

	return escapedString;
}

string escapeString(const char *source) {
	static const string toEscape(" \n\t\r\\");
	string escapedString;

	if (source != nullptr) {

		for (const char *c = source; *c != '\0'; ++c) {
			if (toEscape.find(*c) != string::npos) {
				escapedString.push_back('\\');
			}

			escapedString.push_back(*c);
		}
	}

	return escapedString;
}

string to_string(void *address) {
	const int ADDRESS_HEX_WIDTH = sizeof(void *) * 2;
	ostringstream sstream;
	sstream.seekp(0);
	sstream << showbase << internal << setfill('0') << setw(ADDRESS_HEX_WIDTH + 2) << hex << address;
	sstream.flush();
	return sstream.str();
}

bool safeReceive(int handle, void *buffer, size_t length) {
	size_t toReceive = length;
	int result;

	while (toReceive > 0) {
		result = read(handle, buffer, toReceive);

		// The handle was closed.
		if (result == 0 || (result == -1 && (errno == EPIPE || errno == EBADF))) {
			return false;

		} else if (result == -1) {
			throw runtime_error(strerror(errno));
		}

		toReceive -= result;
	}

	return true;
}

bool safeSend(int handle, const void *buffer, size_t length) {
	size_t toWrite = length;
	int result;
	while (toWrite > 0) {
		// Otherwise the last packet might not be send.
		result = write(handle, buffer, toWrite);

		// The handle was closed.
		if (result == 0 || (result == -1 && (errno == EPIPE || errno == EBADF))) {
			return false;

		} else if (result == -1) {
			throw runtime_error(strerror(errno));
		}

		toWrite -= result;
	}

	return true;
}

uint8_t endianEncode(uint8_t arg) {
	return arg;
}

uint8_t endianDecode(uint8_t arg) {
	return arg;
}

uint16_t endianEncode(uint16_t arg) {
	return htons(arg);
}

uint16_t endianDecode(uint16_t arg) {
	return ntohs(arg);
}

uint32_t endianEncode(uint32_t arg) {
	return htonl(arg);
}

uint32_t endianDecode(uint32_t arg) {
	return ntohl(arg);
}

} /* namespace Utility */
} /* namespace DynDebugger */

