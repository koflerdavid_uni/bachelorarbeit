/*
 * Location.h
 *
 *  Created on: 04.07.2013
 *      Author: David Kofler
 */

#ifndef __LOCATION_H_
#define __LOCATION_H_

#include <ostream>
#include <stdexcept>
#include <string>
#include <vector>

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_image.h>
#include <dyninst/BPatch_point.h>
#include <dyninst/BPatch_statement.h>

namespace DynDebugger {

class Location {
	/**
	 * May or may not be null.
	 */
	std::string file;

	/**
	 * May or may not be null.
	 */
	std::string symbol;

	/**
	 * If this flag is true, then the field #lineNumber is invalid.
	 */
	bool _hasLineNumber;
	long int lineNumber;

public:
	explicit Location(const std::string& symbol);
	explicit Location(BPatch_statement& statement);
	Location(const std::string& file, const std::string& symbol);
	Location(const std::string& file, unsigned int lineNumber);

	const std::string& getFile() const {
		return file;
	}

	const std::string& getSymbol() const {
		return symbol;
	}

	const unsigned long int getLineNumber() const {
		return lineNumber;
	}

	bool hasLineNumber() const {
		return _hasLineNumber;
	}

	std::string toString() const;

	std::vector<BPatch_point *> lookupPoints(BPatch_addressSpace *addressSpace) const;
};

std::ostream& operator<<(std::ostream&, const Location&);

class LocationLookupException: public std::invalid_argument {
public:
	LocationLookupException(const Location& _location, const string& errorMessage = "Location not found");

	const Location& getLocation() const {
		return location;
	}

private:
	const Location& location;
};

} /* namespace DynDebugger */

#endif // __LOCATION_H_
