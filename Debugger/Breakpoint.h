/*
 * Breakpoint.h
 *
 *  Created on: 03.03.2014
 *      Author: David Kofler
 */

#ifndef BREAKPOINT_H_
#define BREAKPOINT_H_

#include "Location.h"

#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

namespace DynDebugger {

class Breakpoint {
public:
	Breakpoint(const Location& _location, BPatch_snippet *_condition = nullptr, unsigned _ignoreCount = 0);
	~Breakpoint();

	const Location location;
	BPatch_snippet *condition;
	unsigned ignoreCount;
	vector<pair<string, string>> actionsInDebugger;

	bool isConditional() const {
		return condition != nullptr;
	}
};

} // namespace DynDebugger

#endif /* BREAKPOINT_H_ */
