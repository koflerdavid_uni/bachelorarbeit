/*
 * CommandParser.cpp
 *
 *  Created on: 19.02.2014
 *      Author: David Kofler
 */

#include "CommandParser.h"

#include "Tokenizer.h"

#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {

namespace Parsers {

bool parseCommand(const string& input, string& command, string& arguments) {
	vector<Parsers::Range> indexes;
	vector<string> tokens = Parsers::splitString(input, indexes);

	if (tokens.size() == 0) {
		return false;
	}

	command = tokens.front();

	if (tokens.size() > 1) {
		// There are two token, i.e. some input after the first token.
		arguments = input.substr(indexes[1].first);

	} else {
		// There is only one token, the command name.
		arguments.clear();
	}

	return true;
}

}

}
