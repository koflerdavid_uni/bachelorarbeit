/*
 * SetStatementParser.cpp
 *
 *  Created on: 20.02.2014
 *      Author: David Kofler
 */

#include "SetStatementParser.h"

#include "ExpressionParser.h"
#include "Tokenizer.h"

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {
namespace Parsers {

BPatch_arithExpr *parseSetStatement(std::function<BPatch_snippet *(string)> variableLookupFunction,
		const string& input) {
	vector<Range> indexes;
	vector<string> tokens = Parsers::splitString(input, indexes);

	if (tokens.size() < 2) {
		return nullptr;
	}

	string variableName = tokens.front();
	BPatch_snippet *variable = variableLookupFunction(variableName);
	if (!variable) {
		return nullptr;
	}

	BPatch_snippet *expression = nullptr;
	try {
		expression = Parsers::parseExpression(input.substr(indexes.front().second), variableLookupFunction);
	} catch (Parsers::ExpressionParserException&) {
	}

	if (!expression || !expression->getType()) {
		return nullptr;
	}

	return new BPatch_arithExpr(BPatch_binOp::BPatch_assign, *variable, *expression);
}

} /* namespace Parsers */
} /* namespace DynDebugger */
