/*
 * Tokenizer_test.cpp
 *
 *  Created on: 03.02.2014
 *      Author: David Kofler
 */

#include "Tokenizer.h"
using namespace DynDebugger::Parsers;

#define BOOST_TEST_MODULE Tokenizer test
#include <boost/test/included/unit_test.hpp>

#include <string>
#include <vector>
using namespace std;

BOOST_AUTO_TEST_CASE(empty_string) {
	vector<string> tokens = splitString("");
	BOOST_REQUIRE(tokens.empty());
}

BOOST_AUTO_TEST_CASE(single_word) {
	vector<string> results;

	results = splitString("hello");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  hello");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("hello  ");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  hello\n\n");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());
}

BOOST_AUTO_TEST_CASE(single_singly_quoted_word) {
	vector<string> results;

	results = splitString("'hello'");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  'hello'");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("'hello'  ");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  'hello'\n\n");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());
}

BOOST_AUTO_TEST_CASE(single_doubly_quoted_word) {
	vector<string> results;

	results = splitString("\"hello\"");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  \"hello\"");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("\"hello\"  ");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());

	results = splitString("  \"hello\"\n\n");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("hello", results.front());
}

BOOST_AUTO_TEST_CASE(escape_sequence) {
	vector<string> results;

	results = splitString("\\t");
	BOOST_REQUIRE_EQUAL(1, results.size());
	BOOST_REQUIRE_EQUAL("\t", results.front());
}

BOOST_AUTO_TEST_CASE(multiple_words) {
	vector<string> expected( { "3", "2  \n 3 ", "", " ", "dr a\t " });

	string source = " 3 '2  \\n 3'\\  \"\"\n ' '  dr' 'a\\t\\   \n ";
	vector<string> results = splitString(source);
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expected.begin(), expected.end(), results.begin(), results.end());
}

BOOST_AUTO_TEST_CASE(multiple_words_with_indexes) {
	vector<string> expectedTokens( { "3", "2  \n 3 ", "", " ", "dr a\t " });
	vector<Range> expectedIndexes( { { 1, 2 }, { 3, 14 }, { 15, 17 }, { 19, 22 }, { 24, 34 } });

	string source = " 3 '2  \\n 3'\\  \"\"\n ' '  dr' 'a\\t\\   \n ";
	vector<Range> indexes;
	vector<string> results = splitString(source, indexes);
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());

	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}
}

BOOST_AUTO_TEST_CASE(trailing_backslash) {
	vector<string> expectedTokens( { "a ", "cde " });
	vector<Range> expectedIndexes( { { 1, 4 }, { 6, 13 } });

	string source = " a\\ \n cde' '\\";
	vector<Range> indexes;
	vector<string> results = splitString(source, indexes);
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());
	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}
}

BOOST_AUTO_TEST_CASE(limit_token_count) {
	vector<string> expectedTokens( { "ab", " cd " });
	vector<Range> expectedIndexes( { { 1, 3 }, { 3, 7 } });

	string source = " ab cd ";
	vector<Range> indexes;
	vector<string> results = splitString(source, indexes, 1);
	BOOST_CHECK_EQUAL(expectedTokens.size(), results.size());
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());

	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}

	expectedTokens = {"ab", "cd", " "};
	expectedIndexes = { {1, 3}, {4, 6}, {6, 7}};
	results = splitString(source, indexes, 2);

	BOOST_CHECK_EQUAL(expectedTokens.size(), results.size());
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());

	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}

	results = splitString(source, indexes, 3);

	BOOST_CHECK_EQUAL(expectedTokens.size(), results.size());
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());

	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}
}

BOOST_AUTO_TEST_CASE(limit_token_count_no_tokens) {
	vector<string> expectedTokens( { " " });
	vector<Range> expectedIndexes( { { 0, 1 } });

	string source = " ";
	vector<Range> indexes;
	vector<string> results = splitString(source, indexes, 1);
	BOOST_CHECK_EQUAL(expectedTokens.size(), results.size());
	BOOST_REQUIRE_EQUAL_COLLECTIONS(expectedTokens.begin(), expectedTokens.end(), results.begin(), results.end());

	BOOST_REQUIRE_EQUAL(expectedIndexes.size(), indexes.size());
	for (size_t i = 0; i < expectedIndexes.size(); ++i) {
		Range expected = expectedIndexes[i];
		Range actual = indexes[i];

		BOOST_CHECK_MESSAGE(expected.first == actual.first,
				"At " << i << ": (begin) " << expected.first << " != " << actual.first);

		BOOST_CHECK_MESSAGE(expected.second == actual.second,
				"At " << i << ": (end) " << expected.second << " != " << actual.second);
	}
}
