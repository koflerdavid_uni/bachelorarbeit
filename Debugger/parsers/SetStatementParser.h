/*
 * SetStatementParser.h
 *
 *  Created on: 20.02.2014
 *      Author: David Kofler
 */

#ifndef __PARSERS_SETSTATEMENTPARSER_H_
#define __PARSERS_SETSTATEMENTPARSER_H_

#include "ExpressionParser.h"

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <string>
using namespace std;

namespace DynDebugger {
namespace Parsers {

BPatch_arithExpr *parseSetStatement(std::function<BPatch_snippet *(string)> variableLookupFunction,
		const string& input);

} /* namespace Parsers */
} /* namespace DynDebugger */

#endif /* __PARSERS_SETSTATEMENTPARSER_H_ */
