/*
 * LocationParser.h
 *
 *  Created on: 19.02.2014
 *      Author: david
 */

#ifndef __PARSERS_LOCATIONPARSER_H_
#define __PARSERS_LOCATIONPARSER_H_

#include "../Location.h"

#include <string>
using namespace std;

namespace DynDebugger {

namespace Parsers {

class LocationParseException: public std::exception {
public:
	LocationParseException(const string& _message) :
			message(_message) {
	}

	LocationParseException(const char *_message) :
			message(_message) {
	}

	const string& getMessage() const {
		return message;
	}

private:
	string message;
};

Location parseLocation(const string& input) throw (LocationParseException);

}

}

#endif /* __PARSERS_LOCATIONPARSER_H_ */
