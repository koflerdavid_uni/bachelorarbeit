/*
 * Tokenizer.h
 *
 *  Created on: 03.02.2014
 *      Author: David Kofler
 */

#ifndef __PARSERS_TOKENIZER_H_
#define __PARSERS_TOKENIZER_H_

#include <string>
#include <vector>
#include <utility>

namespace DynDebugger {

namespace Parsers {

/**
 * Performs shell-like string splitting on the input. Tokens are separated by whitespaces.
 * Backslashes and quotes can be used to escape characters (\ , \t, \r, \n, \\, \", \').
 * The tokenizer is error-tolerant: if unrecognized escape sequences are encountered,
 * the second character will be copied.
 *
 * The `max` parameter limits the amount of tokens returned to `max`. The remaining input string,
 * immediately after the last token, is also returned. Therefore, the result will contain at
 * most `max`+1 strings.
 * The default value (0) causes the whole string to be splitted, and only the tokens are included
 * in the result.
 *
 * @return a vector of tokens. Thanks to C++11 move semantics, returning it can be expected to
 * be cheap.
 */
std::vector<std::string> splitString(const std::string& string, unsigned max=0);

typedef std::pair<std::string::size_type, std::string::size_type> Range;

/**
 * Performs shell-like string splitting on the input. Tokens are separated by whitespaces.
 * Backslashes and quotes can be used to escape characters (\ , \t, \r, \n, \\, \", \').
 * The tokenizer is error-tolerant: if unrecognized escape sequences are encountered,
 * the second character will be copied.
 *
 * The vector passed as second parameter will be filled with the start ad end indices of each token
 * in the source string. The end index points to the first character not belonging to the token.
 * Quote and escape characters *do* belong to the token.
 *
 * The `max` parameter limits the amount of tokens returned to `max`. The remaining input string,
 * immediately after the last token, is also returned. Therefore, the result will contain at
 * most `max`+1 strings.
 * The default value (0) causes the whole string to be splitted, and only the tokens are included
 * in the result.
 *
 * @return a vector of tokens. Thanks to C++11 move semantics, returning it can be expected to
 * be cheap.
 */
std::vector<std::string> splitString(const std::string& string, std::vector<Range>& indexes, unsigned max=0);

}

}

#endif /* __PARSERS_TOKENIZER_H_ */
