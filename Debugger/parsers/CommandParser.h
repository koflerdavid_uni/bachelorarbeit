/*
 * CommandParser.h
 *
 *  Created on: 19.02.2014
 *      Author: David Kofler
 */

#ifndef __PARSERS_COMMANDPARSER_H_
#define __PARSERS_COMMANDPARSER_H_

#include <string>
using namespace std;

namespace DynDebugger {

namespace Parsers {

/**
 * Splits an argument into the command and the arguments string.
 * Returns a string if there is a command.
 * Saves the entire remaining input after the trailing whitespace characters after the command.
 */
bool parseCommand(const string& input, string& command, string& arguments);

}

}

#endif /* __PARSERS_COMMANDPARSER_H_ */
