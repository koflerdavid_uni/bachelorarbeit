/*
 * ExpressionParser.h
 *
 *  Created on: 18.01.2014
 *      Author: david
 */

#ifndef __PARSERS__EXPRESSIONPARSER_H_
#define __PARSERS__EXPRESSIONPARSER_H_

#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <exception>
#include <functional>
#include <stdexcept>
#include <string>
using namespace std;

namespace DynDebugger {
namespace Parsers {

class ExpressionParserException: public runtime_error {
public:
	ExpressionParserException(const string& message) :
			runtime_error(message) {
	}
};

class ExpressionParserTypeError: public ExpressionParserException {
public:
	ExpressionParserTypeError(const string& message) :
			ExpressionParserException(message) {
	}
};

BPatch_snippet *parseExpression(const std::string& source,
		std::function<BPatch_snippet *(string)> variableLookupFunction);

BPatch_snippet *parseExpression(const std::string& source,
		std::function<BPatch_snippet *(string)> variableLookupFunction, std::string::size_type& endPos);

} /* namespace Parsers */
} /* namespace DynDebugger */

#endif /* __PARSERS__EXPRESSIONPARSER_H_ */
