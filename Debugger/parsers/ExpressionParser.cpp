/*
 * ExpressionParser.cpp
 *
 *  Created on: 18.01.2014
 *      Author: david
 */

#include "ExpressionParser.h"

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include "myparsercombinators/myparserlib.h"

namespace P = MyParserLib;

#include <functional>
#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;
using std::placeholders::_1;
using std::placeholders::_2;

namespace DynDebugger {
namespace Parsers {

enum TokenType {
	TT_INT,
	TT_IDENTIFIER,
	TT_KEYWORD,
	TT_ARROW,
	TT_PAREN_OPEN,
	TT_PAREN_CLOSE,
	TT_BRACKET_OPEN,
	TT_BRACKET_CLOSE,
	TT_COMMA,
	TT_DOT,
	TT_QUESTION,
	TT_COLON,
	TT_PLUS,
	TT_MINUS,
	TT_TIMES,
	TT_DIVIDE,
	TT_MODULO,
	TT_LSHIFT,
	TT_RSHIFT,
	TT_LOR,
	TT_LAND,
	TT_NOT,
	TT_OR,
	TT_XOR,
	TT_AND,
	TT_EQUAL,
	TT_NE,
	TT_LT,
	TT_LE,
	TT_GT,
	TT_GE
};

typedef P::Token<char, TokenType> Token_t;
typedef vector<Token_t>::iterator tokenIter;

class ExpressionTokenizer: public P::Tokenizer<string::const_iterator, TokenType> {
public:
	ExpressionTokenizer() {
		auto spaces = many(P::Char::space<string>());

		addRule(spaces->_r(P::match<string::const_iterator>('.')), TT_DOT);
		addRule(spaces->_r(P::match<string::const_iterator>(',')), TT_COMMA);

		string arrow("->");
		addRule(spaces->_r(P::match_string<string::const_iterator>(arrow.begin(), arrow.end())), TT_ARROW);

		addRule(spaces->_r(P::match<string::const_iterator>('+')), TT_PLUS);
		addRule(spaces->_r(P::match<string::const_iterator>('-')), TT_MINUS);
		addRule(spaces->_r(P::match<string::const_iterator>('*')), TT_TIMES);
		addRule(spaces->_r(P::match<string::const_iterator>('/')), TT_DIVIDE);
		addRule(spaces->_r(P::match<string::const_iterator>('%')), TT_MODULO);

		addRule(spaces->_r(P::match<string::const_iterator>('^')), TT_XOR);
		addRule(spaces->_r(P::match<string::const_iterator>('?')), TT_QUESTION);
		addRule(spaces->_r(P::match<string::const_iterator>(':')), TT_COLON);

		addRule(spaces->_r(P::match<string::const_iterator>('(')), TT_PAREN_OPEN);
		addRule(spaces->_r(P::match<string::const_iterator>(')')), TT_PAREN_CLOSE);

		addRule(spaces->_r(P::match<string::const_iterator>('[')), TT_BRACKET_OPEN);
		addRule(spaces->_r(P::match<string::const_iterator>(']')), TT_BRACKET_CLOSE);

		string lshift("<<"), rshift(">>");
		addRule(spaces->_r(P::match_string<string::const_iterator>(lshift.begin(), lshift.end())), TT_LSHIFT);
		addRule(spaces->_r(P::match_string<string::const_iterator>(rshift.begin(), rshift.end())), TT_RSHIFT);

		string le("<="), ge(">=");
		addRule(spaces->_r(P::match_string<string::const_iterator>(le.begin(), le.end())), TT_LE);
		addRule(spaces->_r(P::match_string<string::const_iterator>(ge.begin(), ge.end())), TT_GE);

		addRule(spaces->_r(P::match<string::const_iterator>('<')), TT_LT);
		addRule(spaces->_r(P::match<string::const_iterator>('>')), TT_GT);

		string eq("=="), notEqual("!=");
		addRule(spaces->_r(P::match_string<string::const_iterator>(eq.begin(), eq.end())), TT_EQUAL);
		addRule(spaces->_r(P::match_string<string::const_iterator>(notEqual.begin(), notEqual.end())), TT_NE);
		addRule(spaces->_r(P::match<string::const_iterator>('!')), TT_NOT);

		string logicalOr("||"), logicalAnd("&&");
		addRule(spaces->_r(P::match_string<string::const_iterator>(logicalOr.begin(), logicalOr.end())), TT_LOR);
		addRule(spaces->_r(P::match_string<string::const_iterator>(logicalAnd.begin(), logicalAnd.end())), TT_LAND);

		addRule(spaces->_r(P::match<string::const_iterator>('|')), TT_OR);
		addRule(spaces->_r(P::match<string::const_iterator>('&')), TT_AND);

		addRule(spaces->_r(P::Char::intPlain<string>()), TT_INT);

		auto initialCharacters = P::Char::alpha<string>()->or_(P::match<string::const_iterator>('_'));
		auto rearCharacters = initialCharacters->or_(P::Char::digit<string>());
		auto makeIdentifier = [] (pair<char, vector<char>> t) {
			string s;
			s.push_back(t.first);
			s.append(t.second.begin(), t.second.end());
			return s;
		};

		auto identifier = P::apply(initialCharacters->_(many(rearCharacters)), makeIdentifier);

		addRule(spaces->_r(identifier), TT_IDENTIFIER);
	}
};

class ExpressionGrammar: public P::Grammar<tokenIter, BPatch_snippet *> {
private:
	std::function<BPatch_snippet *(string)> variableLookupFunction;

	enum class AccessType {
		INDEX, MEMBER, MEMBER_INDIRECT
	};

	// Each string which gets converted into a BPatch_constExpr gets stored here.
	// Otherwise it would be impossible to get back the string stored inside a BPatch_constExpr.
	// The typechecker can then verify the validity of member accesses on compound types.
	unordered_map<BPatch_snippet *, string> strings;

public:
	ExpressionGrammar(std::function<BPatch_snippet *(string)> _variableLookupFunction) :
			variableLookupFunction(_variableLookupFunction) {
		// `expr` is the top-level rule
		init("expr");

		// expr = list-expr.
		insert("expr", getRule("list-expr"));

		constructListExpressionRule();
		constructConditionalExpressionRule();
		constructLogicalOperationsRules();
		constructBinaryOperationsRules();
		constructEqualityOperationsRules();
		constructRelationalOperationsRules();
		constructArithmeticOperationsRules();
		constructFactorRule();
		constructPrimaryExprRule();

		auto integer = P::token<tokenIter>(TT_INT);

		// `integer` terminal symbol.
		insert("integer", P::apply(integer, makeIntExpression));

		auto identifier = P::token<tokenIter>(TT_IDENTIFIER);
		auto identifierAction = bind(makeVariableExpression, variableLookupFunction, _1);

		insert("variable", P::apply_fail(identifier, identifierAction));
	}

protected:
	void constructListExpressionRule() {
		// list-expr = conditional-expr (',' conditional-expr)*.
		auto comma = P::token<tokenIter>(TT_COMMA);
		auto listParser = getRule("conditional-expr")->_(many(comma->_r(getRule("conditional-expr"))));

		insert("list-expr", P::apply(listParser, makeListExpression));
	}

	void constructConditionalExpressionRule() {
		// conditional-expr = disjunctive-expr '?' disjunctive-expr ':' disjunctive-expr | disjunctive-expr.
		auto questionMark = P::token<tokenIter>(TT_QUESTION);
		auto colon = P::token<tokenIter>(TT_COLON);

		auto conditionalParser = getRule("disjunctive-expr")->_l(questionMark)->_(
				getRule("disjunctive-expr")->_l(colon)->_(getRule("disjunctive-expr")));
		auto conditionalParserWithAction = P::apply(conditionalParser, makeConditional);

		insert("conditional-expr", conditionalParserWithAction->or_(getRule("disjunctive-expr")));
	}

	void constructLogicalOperationsRules() {
		// disjunctive-expr = conjunctive-expr ('||' conjunctive-expr)*.
		auto lOr = P::token<tokenIter>(TT_LOR);

		auto disjunctiveParser = getRule("conjunctive-expr")->_(many(lOr->_r(getRule("conjunctive-expr"))));

		insert("disjunctive-expr", P::apply(disjunctiveParser, makeDisjunctiveExpr));

		// conjunctive-expr = bin-or-expr ('&&' bin-or-expr)*.
		auto lAnd = P::token<tokenIter>(TT_LAND);

		auto conjunctiveParser = getRule("bin-or-expr")->_(many(lAnd->_r(getRule("bin-or-expr"))));

		insert("conjunctive-expr", P::apply(conjunctiveParser, makeConjunctiveExpr));
	}

	void constructBinaryOperationsRules() {
		// bin-or-expr = bin-xor-expr ('|' bin-xor-expr)*.

		auto bOr = P::token<tokenIter>(TT_OR);
		auto bOrParser = getRule("bin-xor-expr")->_(many(bOr->_r(getRule("bin-xor-expr"))));

		insert("bin-or-expr", P::apply(bOrParser, makeBinaryOrExpr));

		// bin-xor-expr = bin-and-expr ('^' bin-and-expr)*.
		auto bXor = P::token<tokenIter>(TT_XOR);
		auto bXorParser = getRule("bin-and-expr")->_(many(bOr->_r(getRule("bin-and-expr"))));

		insert("bin-xor-expr", P::apply(bXorParser, makeBinaryXorExpr));

		// bin-and-expr = equality-expr ('&' equality-expr)*.
		auto bAnd = P::token<tokenIter>(TT_AND);
		auto bAndParser = getRule("equality-expr")->_(many(bOr->_r(getRule("equality-expr"))));

		insert("bin-and-expr", P::apply(bAndParser, makeBinaryAndExpr));
	}

	void constructEqualityOperationsRules() {
		// equality-expr = relational-expr ('!=' | '=') relational-expr.
		auto equalityOperator = P::token<tokenIter>( { TT_EQUAL, TT_NE });
		auto equalityParser = getRule("relational-expr")->_(equalityOperator->_(getRule("relational-expr")));
		auto parserWithAction = P::apply(equalityParser, makeEqualityExpression);

		insert("equality-expr", parserWithAction->or_(getRule("relational-expr")));
	}

	void constructRelationalOperationsRules() {
		// relational-expr = shift-expr ('<' | '<=' | '>' | '>=') shift-expr.
		auto relationalOperator = P::token<tokenIter>( { TT_LT, TT_LE, TT_GT, TT_GE });
		auto withoutAction = getRule("shift-expr")->_(relationalOperator->_(getRule("shift-expr")));
		auto relationalParser = P::apply(withoutAction, makeRelationalExpression);

		insert("relational-expr", relationalParser->or_(getRule("shift-expr")));
	}

	void constructArithmeticOperationsRules() {
		// shift-expr =  sum-expr ( ('<<' | '>>') sum-expr).
		auto shiftOperator = P::token<tokenIter>( { TT_LSHIFT, TT_RSHIFT });
		auto withoutAction = getRule("sum-expr")->_(shiftOperator->_(getRule("sum-expr")));
		auto shiftParser = P::apply(withoutAction, makeShiftExpression);

		insert("shift-expr", shiftParser->or_(getRule("sum-expr")));

		// sum-expr = product-expr ( ('+' | '-') product-expr)*.
		auto sumOperator = P::token<tokenIter>( { TT_PLUS, TT_MINUS });
		auto sumParser = getRule("product-expr")->_(many(sumOperator->_(getRule("product-expr"))));
		auto sumAction = makeSumExpression;
		insert("sum-expr", P::apply(sumParser, sumAction));

		// product-expr = factor ( ('*' | '/' | '%') factor).
		auto productOperator = P::token<tokenIter>( { TT_TIMES, TT_DIVIDE, TT_MODULO });
		auto productParser = getRule("factor")->_(many(productOperator->_(getRule("factor"))));
		insert("product-expr", P::apply(productParser, makeProductExpression));
	}

	void constructFactorRule() {
		auto isKeywordSizeof = [] (Token_t t) {
			return t.getTag() == TT_KEYWORD && t.getContent() == "sizeof";
		};

		//unary-operator = '!' | '++' | '--' | 'sizeof' | '+' | '-' | '*' | '&'.
		auto sizeofKeywordParser = P::token<tokenIter>(TT_KEYWORD)->if_(isKeywordSizeof);
		auto unaryOperator = P::token<tokenIter>( { TT_NOT, TT_PLUS, TT_MINUS, TT_TIMES, TT_AND })->or_(
				sizeofKeywordParser);

		// factor = unary-operator* primary-expr
		auto factor = many(unaryOperator)->_(getRule("primary-expr"));
		// Because of the sizeof operator (which doesn't work when the snippet doesn't typecheck,
		// the possibility to abort is needed.
		insert("factor", P::apply_fail(factor, makeFactorExpression));
	}

	void constructPrimaryExprRule() {
		// primary-expr = identifier ( '[' expr ']' | '.' identifier | '->' identifier )*
		//              | integer
		//              | '(' expr ')'

		auto openingBracket = P::token<tokenIter>(TT_BRACKET_OPEN);
		auto closingBracket = P::token<tokenIter>(TT_BRACKET_CLOSE);
		auto arrayAccess = P::apply(openingBracket->_r(getRule("expr"))->_l(closingBracket), [](BPatch_snippet *expr) {
			return pair<AccessType, BPatch_snippet *>(AccessType::INDEX, expr);
		});

		// Hack to allow access to `strings` inside the lambdas.
		// lambdas, being implemented using anonymous types, impossibly would need `friend` declarations to
		// be able to access private members of the outer class.
		auto& strings = this->strings;

		auto dot = P::token<tokenIter>(TT_DOT);
		auto identifier = P::token<tokenIter>(TT_IDENTIFIER);
		auto memberAccess = P::apply(dot->_r(identifier), [&strings](Token_t token) {
			BPatch_snippet *memberName = new BPatch_constExpr(token.getContent().c_str());
			strings.insert(make_pair(memberName, string(token.getContent().c_str())));
			return pair<AccessType, BPatch_snippet *>(AccessType::MEMBER, memberName);
		});

		auto arrow = P::token<tokenIter>(TT_ARROW);
		auto indirectMemberAccess = P::apply(arrow->_r(identifier), [&strings](Token_t token) {
			BPatch_snippet *memberName = new BPatch_constExpr(token.getContent().c_str());
			strings.insert(make_pair(memberName, string(token.getContent().c_str())));
			return pair<AccessType, BPatch_snippet *>(AccessType::MEMBER_INDIRECT, memberName);
		});

		auto access = arrayAccess->or_(memberAccess)->or_(indirectMemberAccess);
		auto reference = P::apply(getRule("variable")->_(many(access)),
				bind(mem_fn(&ExpressionGrammar::makeReference), this, _1));

		auto openingParenthese = P::token<tokenIter>(TT_PAREN_OPEN);
		auto closingParenthese = P::token<tokenIter>(TT_PAREN_CLOSE);
		auto subExpr = openingParenthese->_r(getRule("expr"))->_l(closingParenthese);

		insert("primary-expr", reference->or_(getRule("integer"))->or_(subExpr));
	}

	static BPatch_snippet *makeListExpression(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_arithExpr(BPatch_seq, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeConditional(pair<BPatch_snippet *, pair<BPatch_snippet *, BPatch_snippet *>> t) {
		BPatch_boolExpr isTrue(BPatch_ne, *t.first, BPatch_constExpr(0));
		return new BPatch_ifExpr(isTrue, *t.second.first, *t.second.second);
	}

	static BPatch_snippet *makeDisjunctiveExpr(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_boolExpr(BPatch_or, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeConjunctiveExpr(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_boolExpr(BPatch_and, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeBinaryAndExpr(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_arithExpr(BPatch_bit_and, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeBinaryXorExpr(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_arithExpr(BPatch_bit_xor, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeBinaryOrExpr(pair<BPatch_snippet *, vector<BPatch_snippet *>> p) {
		BPatch_snippet *result = p.first;

		for (auto it = p.second.begin(); it != p.second.end(); ++it) {
			result = new BPatch_arithExpr(BPatch_bit_or, *result, **it);
		}

		return result;
	}

	static BPatch_snippet *makeEqualityExpression(pair<BPatch_snippet *, pair<Token_t, BPatch_snippet *>> t) {
		if (t.second.first.getTag() == TT_EQUAL) {
			return new BPatch_boolExpr(BPatch_eq, *t.first, *t.second.second);

		} else {
			assert(t.second.first.getTag() == TT_NE);
			return new BPatch_boolExpr(BPatch_ne, *t.first, *t.second.second);
		}
	}

	static BPatch_snippet *makeRelationalExpression(pair<BPatch_snippet *, pair<Token_t, BPatch_snippet *>> t) {
		BPatch_relOp op;
		switch (t.second.first.getTag()) {
			case TokenType::TT_LT:
				op = BPatch_lt;
				break;

			case TokenType::TT_LE:
				op = BPatch_le;
				break;

			case TokenType::TT_GT:
				op = BPatch_gt;
				break;

			case TokenType::TT_GE:
				op = BPatch_ge;
				break;

			default:
				assert(0 && "Should not happen. Parser must be invalid");
		}

		return new BPatch_boolExpr(op, *t.first, *t.second.second);
	}

	static BPatch_snippet *makeShiftExpression(pair<BPatch_snippet *, pair<Token_t, BPatch_snippet *>> t) {
		// Unfortunately, this operation is not implemented.
		if (t.second.first.getTag() == TT_LSHIFT) {
			return new BPatch_arithExpr(BPatch_left_shift, *t.first, *t.second.second);
		} else {
			assert(t.second.first.getTag() == TT_RSHIFT);
			return new BPatch_arithExpr(BPatch_right_shift, *t.first, *t.second.second);
		}
	}

	static BPatch_snippet *makeSumExpression(pair<BPatch_snippet *, vector<pair<Token_t, BPatch_snippet *>>> t) {
		BPatch_snippet *result = t.first;

		for (auto it = t.second.begin(); it != t.second.end(); ++it) {
			if (it->first.getTag() == TT_PLUS) {
				result = new BPatch_arithExpr(BPatch_plus,*result, *it->second);
			} else {
				assert(it->first.getTag() == TT_MINUS);
				result = new BPatch_arithExpr(BPatch_minus, *result, *it->second);
			}
		}

		return result;
	}

	static BPatch_snippet *makeProductExpression(pair<BPatch_snippet *, vector<pair<Token_t, BPatch_snippet *>>> t) {
		BPatch_snippet *result = t.first;

		for (auto it = t.second.begin(); it != t.second.end(); ++it) {
			if (it->first.getTag() == TT_TIMES) {
				result = new BPatch_arithExpr(BPatch_times, *result, *it->second);
			} else if (it->first.getTag() == TT_DIVIDE) {
				result = new BPatch_arithExpr(BPatch_divide, *result, *it->second);
			} else {
				assert(it->first.getTag() == TT_MODULO);
				result = new BPatch_arithExpr(BPatch_mod, *result, *it->second);
			}
		}

		return result;
	}

	static BPatch_snippet *makeFactorExpression(pair<vector<Token_t>, BPatch_snippet *> t, bool& fail) {
		BPatch_snippet *result = t.second;
		BPatch_type *type = nullptr;

		// '!'  | 'sizeof' | '+' | '-' | '*' | '&'.
		for (auto it = t.first.rbegin(); it != t.first.rend(); ++it) {
			switch (it->getTag()) {
				case TokenType::TT_NOT:
					result = new BPatch_boolExpr(BPatch_eq, *result, *(new BPatch_constExpr(0)));
					break;

				case TokenType::TT_PLUS:
					break; // Nothing to do

				case TokenType::TT_MINUS:
					result = new BPatch_arithExpr(BPatch_negate, *result);
					break;

				case TokenType::TT_TIMES:
					result = new BPatch_arithExpr(BPatch_deref, *result);
					break;

				case TokenType::TT_AND:
					result = new BPatch_arithExpr(BPatch_address, *result);
					break;

				case TokenType::TT_KEYWORD:
					assert(it->getContent() == "sizeof");
					type = result->getType();
					if (!type) {
						fail = true;
					} else {
						result = new BPatch_constExpr(type->getSize());
					}

					break;

				default:
					cout << it->getTag() << endl;
					assert(0 && "Should not happen");
			}
		}

		return result;
	}

	static BPatch_snippet *makeIntExpression(Token_t t) {
		return new BPatch_constExpr(stoi(t.getContent()));
	}

	static BPatch_snippet *makeVariableExpression(std::function<BPatch_snippet *(string)> variableLookupFunction, Token_t token) {
		string variableName = token.getContent();
		BPatch_snippet *reference = variableLookupFunction(variableName.c_str());
		if (!reference) {
			throw ExpressionParserTypeError("Variable " + variableName + " doesn't exist");
		}

		return reference;
	}

	static void ensureIsIndexableType(BPatch_type *type, BPatch_snippet *access) {
		if (!type) {
			throw ExpressionParserTypeError("Could not determine base type for access");
		}

		while (type->getDataClass() == BPatch_dataClass::BPatch_dataTypeDefine) {
			type = type->getConstituentType();
		}

		if (type->getDataClass() != BPatch_dataClass::BPatch_dataArray && type->getDataClass() != BPatch_dataClass::BPatch_dataPointer) {
			throw ExpressionParserTypeError("Referenced type is not an array type");
		}
	}

	BPatch_type *ensureHasMember(BPatch_type *type, BPatch_snippet *access) {
		if (!type) {
			throw ExpressionParserTypeError("Could not determine base type for access");
		}

		while (type->getDataClass() == BPatch_dataClass::BPatch_dataTypeDefine) {
			type = type->getConstituentType();
		}

		// This should be safe because all these strings were inserted by the parser.
		string& memberName = strings[access];
		auto predicate = [memberName](BPatch_field* field) {
			// This conversion should be safe because the grammar requires it to be a strin
			return field->getName() == memberName;
		};

		vector<BPatch_field *> *components = type->getComponents();
		if (find_if(components->begin(), components->end(), predicate) == components->end()) {
			throw ExpressionParserTypeError("Type " + string(type->getName()) + " has no member " + memberName);
		}

		return type;
	}

	BPatch_snippet *makeReference(pair<BPatch_snippet *, vector<pair<AccessType, BPatch_snippet *>>> data) {
		BPatch_snippet *reference = data.first;

		for (auto& access : data.second) {
			if (access.first == AccessType::INDEX) {
				ensureIsIndexableType(reference->getType(), access.second);
				reference = new BPatch_arithExpr(BPatch_binOp::BPatch_ref, *reference, *access.second);

			} else if (access.first == AccessType::MEMBER) {
//				auto type = ensureHasMember(reference->getType(), access.second);
				reference = new BPatch_arithExpr(BPatch_binOp::BPatch_fieldref, *reference, *access.second);

			} else {
				assert(access.first == AccessType::MEMBER_INDIRECT);
				BPatch_arithExpr target(BPatch_unOp::BPatch_deref, *reference);
				ensureHasMember(target.getType(), access.second);
				reference = new BPatch_arithExpr(BPatch_binOp::BPatch_fieldref, target, *access.second);
			}
		}

		return reference;
	}

};

void printTokens(vector<P::Token<char, TokenType> > tokens, ostream& out) {
	for (auto it = tokens.begin(); it != tokens.end(); ++it) {
		out << it->getBegin() << ": (" << it->getTag() << ") '" << it->getContent() << "'" << endl;
	}
}

BPatch_snippet *parseExpression(const std::string& source,
		std::function<BPatch_snippet *(string)> variableLookupFunction, std::string::size_type& endPos) {
	ExpressionTokenizer tokenizer;

	vector<Token_t> tokens;
	tokenizer.getTokens(source.begin(), source.end(), tokens);

	ExpressionGrammar::ptr expressionGrammar = make_shared < ExpressionGrammar > (variableLookupFunction);
	BPatch_snippet *snippet;

	try {
		tokenIter afterParsing = P::parse_with_exception(tokens.begin(), tokens.end(), expressionGrammar, snippet);
		endPos = afterParsing->getBegin();
		return snippet;
	} catch (P::ParseException& e) {
		cout << e.getPosition() << ": " << e.getErrorMessage() << endl;
		return nullptr;
	}
}

BPatch_snippet *parseExpression(const std::string& source,
		std::function<BPatch_snippet *(string)> variableLookupFunction) {
	string::size_type endPos;
	return parseExpression(source, variableLookupFunction, endPos);
}

} /* namespace Parsers */
} /* namespace DynDebugger */
