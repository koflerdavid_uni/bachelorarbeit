/*
 * Tokenizer.cpp
 *
 *  Created on: 03.02.2014
 *      Author: david
 */

#include "Tokenizer.h"

#include <string>
#include <vector>
using namespace std;

#include <cassert>

namespace DynDebugger {

namespace Parsers {

static inline char handleEscape(char escapeChar) {
	if (escapeChar == 't') {
		return '\t';
	} else if (escapeChar == 'n') {
		return '\n';
	} else if (escapeChar == 'r') {
		return '\r';
	} else {
		// This case includes \, ' and "
		return escapeChar;
	}
}

static string getToken(const string& input, string::size_type begin, const string& endChars, string::size_type &end) {
	string token;
	string::size_type pos = begin;
	bool ignoreEndChars = false;
	char currentQuoteChar = '\0';

	while (pos < input.size()) {
		char current = input[pos];
		assert(!ignoreEndChars || currentQuoteChar == '"' || currentQuoteChar == '\'');

		if (current == '\'' || current == '"') {
			if (ignoreEndChars) {
				// In this case the quote mode will be terminated,
				// but only if the current character matches the opening one.
				if (current == currentQuoteChar) {
					ignoreEndChars = false;
					currentQuoteChar = current;
				} else {
					token.push_back(current);
				}
			} else {
				ignoreEndChars = true;
				currentQuoteChar = current;
			}

		} else if (current == '\\') {
			// A backslash may initiate an escape sequence.
			if (pos + 1 < input.size()) {
				token.push_back(handleEscape(input[pos + 1]));
				// Jump past next char.
				++pos;
			} else {
				assert(pos + 1 == input.size());
				// Ignore trailing backslash.
			}

		} else if (endChars.find(current) < string::npos) {
			if (ignoreEndChars) {
				// In this mode whitespace characters will be added to the token.
				token.push_back(current);
			} else {
				// If the tokenizer is not in quote mode,
				// then the current character terminates the token.
				break;
			}

		} else {
			// Normal character.
			token.push_back(current);
		}

		++pos;
	}

	end = pos;

	return token;
}

std::vector<string> splitString(const std::string& input, unsigned max) {
	std::vector<Range> indexes;
	return splitString(input, indexes, max);
}

vector<string> splitString(const string& input, vector<Range>& indexes, unsigned max) {
	static string spaces(" \t\n\r");
	vector<string> tokens;
	indexes.clear();

	unsigned count = 0;
	string::size_type begin = input.find_first_not_of(spaces, 0);
	string::size_type end;
	while ((max == 0 || count < max) && begin < input.size()) {
		tokens.push_back(getToken(input, begin, spaces, end));
		indexes.emplace_back(begin, end);
		begin = input.find_first_not_of(spaces, end);
		++count;
	}

	if (max != 0) {
		string::size_type afterLastToken = 0;

		if (!indexes.empty()) {
			afterLastToken = indexes.rbegin()->second;
		}

		tokens.emplace_back(input, afterLastToken);
		indexes.emplace_back(afterLastToken, input.size());
	}

	return tokens;
}

}

}
