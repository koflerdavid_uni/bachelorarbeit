/*
 * LocationParser.cpp
 *
 *  Created on: 19.02.2014
 *      Author: david
 */

#include "LocationParser.h"

#include "../Location.h"
#include "Tokenizer.h"

#include <string>
#include <vector>
using namespace std;

namespace DynDebugger {

namespace Parsers {

/**
 * Formats:
 * main.c:main => file `main.c`, function `main`
 * main.c:22 => file `main.c`, line number 22
 * main => function `main`
 */
Location parseLocation(const string& input) throw (LocationParseException) {
	vector<string> tokens = splitString(input);

	if (tokens.empty()) {
		throw LocationParseException("No breakpoint specified");
	}

	std::string inputStr = tokens.front();
	std::string::size_type delimiterPos = inputStr.find(':');

	if (delimiterPos == std::string::npos) {
		// No delimiter => symbol
		return Location(inputStr);

	} else {
		// Look for a delimiter
		char *output = nullptr;
		unsigned long lineNumber = strtol(&input[delimiterPos + 1], &output, 0);
		string fileName = inputStr.substr(0, delimiterPos);

		if (output == &input[delimiterPos + 1]) {
			// No line number parsed => reference to symbol in a source file
			string symbolName = inputStr.substr(delimiterPos + 1);
			return Location(fileName, symbolName);
		} else {
			// line number parsed => reference to symbol in source file
			return Location(fileName, lineNumber);
		}
	}
}

}

}
