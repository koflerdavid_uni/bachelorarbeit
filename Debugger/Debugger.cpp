/*
 * debugger.cpp
 *
 *  Created on: 15.12.2013
 *      Author: David Kofler
 */

#include "Debugger.h"

#include "DyninstHelpers.h"
#include "Location.h"
#include "TablePrinter.h"

// SymtabAPI
#include <dyninst/Function.h>
#include <dyninst/Type.h>

// DyninstAPI
#include <dyninst/BPatch.h>
#include <dyninst/BPatch_function.h>
#include <dyninst/BPatch_point.h>
#include <dyninst/BPatch_snippet.h>
#include <dyninst/BPatch_statement.h>

using namespace Dyninst;

#include <functional>
#include <iomanip>
#include <map>
#include <mutex>
#include <ostream>
#include <unordered_set>
#include <vector>
using namespace std;

namespace DynDebugger {

std::mutex Debugger::processesMutex;
map<BPatch_process *, Debugger *> Debugger::processes;
BPatchExitCallback Debugger::previousExitCallback;

BreakpointData::BreakpointData(Debugger& _debugger, unsigned _breakpointId) :
		debugger(_debugger) {
	handleInDebuggee = debugger.process->malloc(sizeof(BreakpointData::Handle),
			"Breakpoint" + to_string(_breakpointId));
	breakpointId = _breakpointId;
	handleInDebuggee->writeValue(&breakpointId);
}

BreakpointData::~BreakpointData() {
	debugger.process->free(*handleInDebuggee);
}

void insertBreakpointsAtPoints(Debugger& debugger, vector<BPatch_point *>& points, BPatch_snippet *condition,
		BPatch_snippet *action, vector<BPatchSnippetHandle *>& snippets) {
	for (vector<BPatch_point *>::iterator it = points.begin(); it != points.end(); ++it) {
		BPatch_point *point = *it;

		BPatch_breakPointExpr breakInstruction;
		BPatch_sequence breakpoint( { action, &breakInstruction });

		if (condition) {
			BPatch_boolExpr check(BPatch_relOp::BPatch_ne, *condition, BPatch_constExpr(0));
			BPatch_ifExpr conditionalBreakpoint(check, breakpoint);
			snippets.push_back(debugger.process->insertSnippet(conditionalBreakpoint, *point, BPatch_callBefore));
		} else {
			snippets.push_back(debugger.process->insertSnippet(breakpoint, *point, BPatch_callBefore));
		}
	}
}

Debugger::Debugger(ostream& _outputStream, std::function<bool(const string&, string&)> _readlineFunction) :
		Debugger(_outputStream, [this](vector<string>& outputs) {
			for (string& output : outputs) {
				this->outputStream << output<< endl;
			}
		}, _readlineFunction) {
}

Debugger::Debugger(ostream& _outputStream, std::function<void(vector<string>&)> onAfterEvent,
		std::function<bool(const string&, string&)> _readlineFunction) :
		Debugger(_outputStream, []() {}, onAfterEvent, _readlineFunction) {
}

Debugger::Debugger(ostream& _outputStream, std::function<void()> onAfterCommand,
		std::function<void(vector<string>&)> onAfterEvent,
		std::function<bool(const string&, string&)> _readlineFunction) :
		process(nullptr), current_thread(0), outputStream(_outputStream), onAfterCommandHandler(onAfterCommand), onAfterEventHandler(
				onAfterEvent), readlineFunction(_readlineFunction) {
	initClass();

	// Event loop to handle Dyninst breakpoints because it should be possible to handle multiple breakpoints
	// at the same time if they hit different processes. Beside that, class-level threads are difficult to handle.

	breakpointHandlerThread = std::thread(bind(mem_fn(&Debugger::handleBreakpoints), this));
}

void Debugger::initClass() {
	// Initialization of this variable is executed exactly one.
	static Initializer initializer;
}

Debugger::Initializer::Initializer() {
	// Otherwise, backtraces on breakpoints become quite unreadable.
	BPatch::getBPatch()->setInstrStackFrames(true);
	BPatch::getBPatch()->registerUserEventCallback(Debugger::handleMessageFromDebuggee);
	Debugger::previousExitCallback = BPatch::getBPatch()->registerExitCallback(Debugger::handleProcessExit);
}

Debugger::~Debugger() {
	quitBreakpointHandlerThread = true;
	breakpointCondition.notify_all();
	breakpointHandlerThread.join();

	if (process) {
		detachFromProcess();
	}
}

/**
 * This function receives messages and puts them into the queue of the debugger it belongs to.
 */
void Debugger::handleMessageFromDebuggee(BPatch_process *process, void *buf, unsigned bufSize) {
	assert(bufSize == sizeof(BreakpointData::Handle));

	BreakpointData::Handle *handle = (BreakpointData::Handle *) buf;

	// Get the debugger which is responsible for this process.
	Debugger& debugger = getInstanceByProcess(process);
	assert(process == debugger.process);

	std::lock_guard<std::recursive_mutex>(debugger.dyninstLock);
	debugger.breakpointsToHandle.push(*handle);
	debugger.breakpointCondition.notify_one();
}

void Debugger::storeBreakpointStackFrame() {
	vector<BPatch_frame> frames;
	process->getThread(current_thread)->getCallStack(frames);
	unsigned frameIndex = 0;
	while (frameIndex < frames.size() - 1
			&& frames[frameIndex].getFrameType() != BPatch_frameType::BPatch_frameTrampoline) {
		++frameIndex;
	}

	if (frameIndex == frames.size() - 1) {
		// This is odd...
		storeCurrentStackFrame();
	} else {
		// frameIndex is the number of the instrumentation frame. The next frame should be the innermost
		// frame the user is interested in.
		currentFrameIndex = frameIndex + 1;
		currentFrame = frames[currentFrameIndex];
		currentFunction = currentFrame.findFunction();
	}
}

void Debugger::printBreakpointLocation(ostream& stream) {
	if (currentFunction) {
		stream << "Stopped in " << currentFunction->getDemangledName() << endl;
		vector<BPatch_statement> sourceLines;
		static_assert(sizeof(void *) <= sizeof(Dyninst::Address),
				"Check whether the Dyninst types for addresses are suitable");
#ifdef PRINT_LINE_INFORMATION_FOR_FRAMES
		if (process->getSourceLines((Dyninst::Address) currentFrame.getPC(), sourceLines)) {
			if (sourceLines.size() > 0) {
				void *start, *end;
				currentFunction->getAddressRange(start, end);
				vector<BPatch_statement> source(sourceLines);
				sourceLines.clear();
				for (auto& line : source) {
					if (line.startAddr() >= start && line.endAddr() <= end) {
						sourceLines.push_back(line);
					}
				}
			}

			if (!sourceLines.empty()) {
				if (sourceLines.size() > 1) {
					stream << "Found multiple matches; will display the first one." << endl;
				}

				stream << "Location: " << Location(sourceLines.front());

				if (sourceLines.front().lineOffset() != -1) {
					stream << ":" << sourceLines.front().lineOffset();
				}
			}
		}
#endif

		stream << endl;

	}

	TablePrinter tablePrinter( { "Pos", "Return Address", "Name", "Frame type" });
	tablePrinter.put(currentFrameIndex);
	tablePrinter.putAddress(currentFrame.getPC());

	if (currentFunction) {
		tablePrinter.put(currentFunction->getDemangledName());
	} else {
		tablePrinter.put("");
	}

	if (currentFrame.getFrameType() == BPatch_frameType::BPatch_frameSignal) {
		tablePrinter.put("Signal frame");
	} else if (currentFrame.getFrameType() == BPatch_frameType::BPatch_frameTrampoline) {
		tablePrinter.put("Instrumentation frame");
	} else if (currentFrame.getFrameType() == BPatch_frameType::BPatch_frameNormal) {
		tablePrinter.put("");
	} else {
		assert(0 && "Unknown frame type");
	}

	tablePrinter.output(stream);
}

void Debugger::handleBreakpoints() {
	while (!quitBreakpointHandlerThread) {
		// Wait until there is a breakpoint to handle.
		std::unique_lock<std::recursive_mutex> breakpointUniqueLock(dyninstLock);
		breakpointCondition.wait(breakpointUniqueLock);
		while (!quitBreakpointHandlerThread && !breakpointsToHandle.empty()) {

			unsigned breakpointId = breakpointsToHandle.front();
			breakpointsToHandle.pop();
			breakpointUniqueLock.unlock();

			auto result = currentBreakpoints.find(breakpointId);
			if (result == currentBreakpoints.end()) {
				continue;
			}

			Breakpoint& breakpoint = result->second.first;

			// Because the message informing the debugger about the breakpoint was send before the
			// breakpoint actually stopped the program, the debugger has to wait here.
			// This is even necessary if the breakpoint is going to be ignored,
			// otherwise `continueExecution` might get called before the breakpoint stops the thread,
			// destroying the point of the ignore count.
			while (!process->isStopped()) {
				;
			}

			if (breakpoint.ignoreCount == 0) {
				vector<string> output;
				ostringstream stream;

				output.push_back("Breakpoint " + to_string(breakpointId) + " hit");

				for (auto& command : breakpoint.actionsInDebugger) {
					stream.str();

					if (!executeCommand(stream, command.first, command.second)) {
						output.push_back(
								"Unknown command in breakpoint #" + to_string(breakpointId) + ": "
										+ command.first);
						break;

					} else {
						output.emplace_back(stream.str());
					}
				}

				if (process->isStopped()) {
					// Store the right stackframe. This is not trivial because instrumentation frames are irrelevent
					// and the user should not have to manually select the innermost non-instrumentation frame.
					storeBreakpointStackFrame();
					stream.str("");
					printBreakpointLocation(stream);
					output.emplace_back(stream.str());
				}

				onAfterEventHandler(output);

			} else {
				--breakpoint.ignoreCount;
				process->continueExecution();
			}
		}
	}
}

Debugger& Debugger::getInstanceByProcess(BPatch_process *process) {
	std::lock_guard<std::mutex> guard(processesMutex);
	assert(processes.count(process) > 0);
	Debugger& debugger = *processes.find(process)->second;

	return debugger;
}

void Debugger::applyBreakpoints() {
	for (auto& entry : breakpointsToApply) {
		applySingleBreakpoint(entry.second, entry.first);
	}

	breakpointsToApply.clear();
}

void Debugger::applySingleBreakpoint(const Breakpoint& breakpoint, unsigned breakpointId) {
	vector<BPatch_function *> funcs;
	BPatch_image *image = process->getImage();
	BPatch_module *module = nullptr;
	const Location& location = breakpoint.location;

	if (location.getFile().size() > 0) {
		module = image->findModule(location.getFile().c_str());
		if (module == nullptr) {
			throw InvalidBreakpointException(location);
		}
	}

	currentBreakpoints.insert( { breakpointId, { breakpoint, BreakpointData(*this, breakpointId) } });
	pair<Breakpoint, BreakpointData>& activeBreakpoint = currentBreakpoints.find(breakpointId)->second;

	const char USER_MESSAGE_SEND_FUNCTION_NAME[] = "DYNINSTuserMessage";

	vector<BPatch_function *> results;
	bool result = process->getImage()->findFunction(USER_MESSAGE_SEND_FUNCTION_NAME, results);
	assert(result && "The DyninstAPI_RT library is not present in the debuggee");

	BPatch_arithExpr setBreakpointId(BPatch_binOp::BPatch_assign, *activeBreakpoint.second.handleInDebuggee,
			BPatch_constExpr(activeBreakpoint.second.breakpointId));
	BPatch_arithExpr handleAddressInDebuggee(BPatch_unOp::BPatch_address,
			*activeBreakpoint.second.handleInDebuggee);
	BPatch_constExpr handleSize(activeBreakpoint.second.handleInDebuggee->getSize());
	assert(activeBreakpoint.second.handleInDebuggee->getSize() >= sizeof(activeBreakpoint.second.breakpointId));
	BPatch_funcCallExpr DYNINSTuserMessage(*results.front(), { &handleAddressInDebuggee, &handleSize });
	BPatch_sequence sequence( { &setBreakpointId, &DYNINSTuserMessage });

	try {
		vector<BPatch_point *> points = location.lookupPoints(process);
		insertBreakpointsAtPoints(*this, points, breakpoint.condition, &sequence,
				activeBreakpoint.second.snippetHandles);

	} catch (LocationLookupException& e) {
		currentBreakpoints.erase(breakpointId);
		throw InvalidBreakpointException(e.getLocation());
	}
}

void Debugger::removeBreakpoint(unsigned breakpointId, bool removeFromContainer) {
	if (process) {
		assert(breakpointsToApply.empty());

		auto result = currentBreakpoints.find(breakpointId);
		if (result == currentBreakpoints.end()) {
			throw DebuggerException("Breakpoint does not exist");
		}

		for (auto snippetHandle : result->second.second.snippetHandles) {
			if (!process->deleteSnippet(snippetHandle)) {
				throw DebuggerException(
						"Breakpoint instance in " + snippetHandle->getFunc()->getDemangledName()
								+ " could not be removed");
			}
		}

		if (removeFromContainer) {
			currentBreakpoints.erase(breakpointId);
		}

	} else {
		assert(currentBreakpoints.empty());

		if (breakpointsToApply.count(breakpointId) == 0) {
			throw DebuggerException("Breakpoint does not exist");
		}

		if (removeFromContainer) {
			breakpointsToApply.erase(breakpointId);
		}
	}
}

bool Debugger::executeCommand(const string& command, const string& arguments) {
	return executeCommand(outputStream, command, arguments);
}

bool Debugger::executeCommand(ostream& outputStream, const string& command, const string& arguments) {
	try {
		// It is much clearer to perform the call this way.
		DebuggerCommand fun = CommandTable::lookupCommand(command);

		string argumentsCopy(arguments);

		try {
			// This method of acquiring the lock is exception-safe
			std::lock_guard<std::recursive_mutex> lock(dyninstLock);
			fun(*this, outputStream, readlineFunction, argumentsCopy);

		} catch (DebuggerException& e) {
			outputStream << e.getMessage() << endl;
		}

		onAfterCommandHandler();

		return true;

	} catch (NoSuchCommandException& e) {
		outputStream << "Command not implemented" << endl;
		return false;
	}
}

bool Debugger::attachToProcess(int pid, const char *pathToBinary) {
	process = BPatch::getBPatch()->processAttach(pathToBinary, pid);

	if (process) {
		// Register in the global process -> debugger map
		handleProcessConnection();
	}

	return process != nullptr;
}

bool Debugger::attachToProcess(int pid, string pathToBinary) {
	return attachToProcess(pid, pathToBinary.c_str());
}

bool Debugger::startProcess(bool hold) {
	assert(!process);
	assert(!arguments.empty());

	vector<const char *> argv;
	argv.reserve(arguments.size() + 1);
	for (auto& arg : arguments) {
		argv.push_back(arg.c_str());
	}

	argv.push_back(nullptr);

	process = BPatch::getBPatch()->processCreate(argv.front(), argv.data());
	if (!process) {
		return false;
	}

	applyBreakpoints();

	handleProcessConnection();

	if (hold) {
		return true;
	} else {
		return process->continueExecution();
	}
}

bool Debugger::startProcess(vector<string> args, bool hold) {
	specifyArguments(args);
	return startProcess(hold);
}

void Debugger::storeCurrentStackFrame(unsigned frameIndex) {
	currentFrameIndex = frameIndex;

	vector<BPatch_frame> callStack;
	process->getThread(current_thread)->getCallStack(callStack);

	if (!callStack.empty()) {
		currentFrame = callStack.front();
		currentFunction = currentFrame.findFunction();
	}
}

bool Debugger::detachFromProcess() {
	assert(process != nullptr);

	for (auto& activeBreakpoint : currentBreakpoints) {
		removeBreakpoint(activeBreakpoint.first, false);
	}

	if (process->isDetached() || process->detach(true)) {
		handleProcessDisconnection();
		return true;
	}

	return false;
}

bool Debugger::terminateProcess() {
	assert(process != nullptr);
	assert(!process->isTerminated());

	return process->terminateExecution();
}

void Debugger::handleProcessConnection() {
	std::lock_guard<std::recursive_mutex> guard(dyninstLock);

	// Choose an arbitrary thread (usually the main thread)
	current_thread = process->getThreadByIndex(0)->getTid();
	storeCurrentStackFrame();

	// Register in the global process -> debugger map
	processes.insert( { process, this });
}

void Debugger::handleProcessDisconnection() {
	std::lock_guard<std::recursive_mutex> guard(dyninstLock);

	assert(process != nullptr && "The process handle should have been cleared by this method");
	processes.erase(process);

	process = nullptr;
}

void Debugger::handleProcessExit(BPatch_thread *thread, BPatch_exitType terminationStatus) {
	Debugger& debugger = getInstanceByProcess(thread->getProcess());

	if (terminationStatus == BPatch_exitType::ExitedNormally) {
		debugger.outputStream << "Process terminated with code " << debugger.process->getExitCode() << endl;
	} else {
		assert(debugger.process->terminationStatus() == BPatch_exitType::ExitedViaSignal);
		debugger.outputStream << "Process terminated with signal " << debugger.process->getExitSignal() << endl;
	}

	// Delete breakpoints, else they linger around and the next call
	//to `detachFromProcess` would try to remove them
	debugger.currentBreakpoints.clear();

	if (previousExitCallback != nullptr) {
		previousExitCallback(thread, terminationStatus);
	}

	debugger.handleProcessDisconnection();
}

void Debugger::specifyArguments(vector<string>& args) {
	arguments.resize(1);
	arguments.insert(arguments.end(), args.begin(), args.end());
}

void Debugger::specifyProgramAndArguments(vector<string>& newArgv) {
	arguments.assign(newArgv.begin(), newArgv.end());
}

} // namespace DynDebugger
