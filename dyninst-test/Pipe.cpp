/*
 * Pipe.cpp
 *
 *  Created on: 09.10.2013
 *      Author: david
 */

#include "Pipe.h"

#include <unistd.h>

Pipe::Pipe() throw (PipeException) :
		readEndOpen(true), writeEndOpen(true) {
	if (pipe(this->fds) == -1) {
		throw PipeException();
	}
}

Pipe::~Pipe() {
	closeReadEnd();
	closeWriteEnd();
}

void Pipe::closeReadEnd() {
	close(getReadFd());
}

void Pipe::closeWriteEnd() {
	close(getWriteFd());
}

bool Pipe::writeByte(char byte) throw (PipeException) {
	ensureWriteFdOpen();
	return write(getWriteFd(), &byte, 1) > 0;
}

void Pipe::ensureReadFdOpen() throw (PipeException) {
	if (!isReadEndOpen()) {
		throw PipeException();
	}
}

int Pipe::readByte() throw (PipeException) {
	ensureReadFdOpen();

	char byte;
	if (read(getReadFd(), &byte, 1) > 0) {
		return byte;
	}

	return -1;
}

void Pipe::ensureWriteFdOpen() throw (PipeException) {
	if (!isWriteEndOpen()) {
		throw PipeException();
	}
}

int Pipe::writeBuffer(char* buffer, int size) throw (PipeException) {
	ensureWriteFdOpen();
	return write(getWriteFd(), buffer, size);
}

int Pipe::readBuffer(char* buffer, int size) throw (PipeException) {
	ensureReadFdOpen();
	return read(getReadFd(), buffer, size);
}
