/*
 * Pipe.h
 *
 *  Created on: 09.10.2013
 *      Author: david
 */

#ifndef PIPE_H_
#define PIPE_H_

#include <unistd.h>

class PipeException {
};

class Pipe {
public:
	Pipe() throw (PipeException);
	virtual ~Pipe();

	int getReadFd() const {
		return fds[0];
	}

	int getWriteFd() const {
		return fds[1];
	}

	bool isReadEndOpen() const {
		return readEndOpen;
	}

	bool isWriteEndOpen() const {
		return writeEndOpen;
	}

	void closeReadEnd();
	void closeWriteEnd();

	bool writeByte(char byte) throw (PipeException);
	int readByte() throw (PipeException);

	int writeBuffer(char *buffer, int size) throw (PipeException);
	int readBuffer(char *buffer, int size) throw (PipeException);

protected:
	void ensureReadFdOpen() throw (PipeException);
	void ensureWriteFdOpen() throw (PipeException);

private:
	int fds[2];
	bool readEndOpen;
	bool writeEndOpen;
};

#endif /* PIPE_H_ */
