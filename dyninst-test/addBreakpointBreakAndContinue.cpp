#include <BPatch.h>
#include <BPatch_function.h>
#include <BPatch_point.h>
#include <BPatch_statement.h>
using namespace Dyninst;

#include <iostream>
#include <vector>
using namespace std;

#include <cstdlib>
#include <csignal>
#include <unistd.h>

void aSimpleFunction();

BPatch bpatch;

int main(int argc, char **argv) {
	int pid = fork();

	if (pid > 0) {
		// Parent process

		BPatch_process *process = bpatch.processAttach(NULL, pid);
		if (process == NULL) {
			cerr << "Attaching failed" << endl;
		}

		// Child process is stopped right now because we attach.
		// Also, it may have sent the SIGSTOP signal to itself.

		BPatch_image *image = process->getImage();

		vector<BPatch_function*> results;
		if (!image->findFunction("aSimpleFunction", results)) {
			// Clean up
			process->terminateExecution();
			cerr << "Error: couldn't find 'aSimpleFunction'" << endl;
			exit(2);
		}

		BPatch_function *func = results[0];
		vector<BPatch_point *> entryPoints;
		func->getEntryPoints(entryPoints);
		BPatch_point *entryPoint = entryPoints[0];
		process->insertSnippet(BPatch_breakPointExpr(), *entryPoint,
				BPatch_callBefore);

		cout << "Added breakpoint" << endl;

		process->continueExecution();

		sleep(3); // Critical. There is a race condition in there.

		// Synchronize with child
		kill(pid, SIGUSR1);

		bpatch.waitForStatusChange();

		cout << "Child process stopped" << endl;
		sleep(3);
		process->continueExecution();
		bpatch.waitForStatusChange();
		cout << "End" << endl;

	} else if (pid == 0) {
		// Child process
		cout << "Child: " << getpid() << endl;

		// Synchronize with parent
		sigset_t sigs;
		int dummy;
		sigemptyset(&sigs);
		sigaddset(&sigs, SIGUSR1);
		if (sigwait(&sigs, &dummy) != 0) {
			cerr << "ouch" << endl;
			exit(2);
		}

		cout << "Continuing" << endl;
		aSimpleFunction();
	} else {
		cerr << "Forking failed" << endl;
	}

	return 0;
}

void aSimpleFunction() {
	cout << "Hello" << endl;
}
