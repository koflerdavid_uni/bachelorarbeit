#include <dyninst/BPatch.h>
#include <dyninst/walker.h>
#include <dyninst/PCProcess.h>
#include <dyninst/procstate.h>
using namespace Dyninst;

#include <iostream>
using namespace std;

#include <cassert>
#include <cstdlib>

BPatch bpatch;

int main(int argc, char **argv) {
	assert(argc > 1);
	int procid = atoi(argv[1]);
	BPatch_process *proc = bpatch.processAttach(NULL, procid);

	proc->detach(true);

	return 0;
}
