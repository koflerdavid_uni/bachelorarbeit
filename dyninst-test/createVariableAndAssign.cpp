/*
 * createVariableAndAssign.cpp
 *
 *  Created on: 03.02.2014
 *      Author: david
 */

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <iostream>
using namespace std;

#include <cassert>
#include <cstdlib>

BPatch bpatch;

int main(int argc, char **argv) {
	assert(argc > 2);
	int procid = atoi(argv[1]);
	BPatch_process *proc = bpatch.processAttach(NULL, procid);
	assert(proc);

	BPatch_variableExpr *var1 = proc->getImage()->findVariable(argv[2]);
	assert(var1);
	BPatch_variableExpr *var2 = proc->malloc(*var1->getType());
	assert(var2);
	BPatch_constExpr c(2);
	BPatch_arithExpr addition(BPatch_binOp::BPatch_plus, c, *var1);

	BPatch_arithExpr expr(BPatch_binOp::BPatch_assign, *var2, addition);

	bool error;
	proc->oneTimeCode(expr, &error);
	assert(!error);

	proc->detach(true);

	return 0;
}
