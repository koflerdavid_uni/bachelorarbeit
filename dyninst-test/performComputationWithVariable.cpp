/*
 * performComputationWithVariable.cpp
 *
 *  Created on: 03.02.2014
 *      Author: david
 */

#include <dyninst/BPatch.h>
#include <dyninst/BPatch_snippet.h>
using namespace Dyninst;

#include <iostream>
using namespace std;

#include <cassert>
#include <cstdlib>

BPatch bpatch;

int main(int argc, char **argv) {
	assert(argc > 2);
	int procid = atoi(argv[1]);
	BPatch_process *proc = bpatch.processAttach(NULL, procid);
	assert(proc);

	BPatch_variableExpr *var = proc->getImage()->findVariable(argv[2]);
	BPatch_arithExpr expr(BPatch_plus, *var, BPatch_constExpr(2));

	bool error;
	void *result = proc->oneTimeCode(expr, &error);
	assert(!error);
	cout << (unsigned long long) result << endl;

	proc->detach(true);

	return 0;
}
