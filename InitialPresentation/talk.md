Welcome to the initial presentation of my bachelor thesis "Debuggin tool using dynamic instrumentation". My supervisor is Dr. Radu Prodan. 

Firstly, the concent "dynamic instrumentation" will be explained, along with 
some use cases. Then, the features which shall distinguish my debugging tool are explained. 
Since implementing a viable toolkit for this task is 
complicated and time-consuming, lastly the DyninstAPI will be presented.

Instrumentation changes a program to perform
additional duties during its execution. 
Static instrumentation operates on a binary program which has not yet been
launched. Dynamic instrumentation operates on a running process instead.
Changes introduced by static instrumentation are permanent, while manipulating
processes usually doesn't change the program stored on disk. 

Instrumentation is commonly employed to introduce logging and to collect 
performance data which can be used to optimize the
application in terms of usability, execution time, memory consumption and many
more. 

A distinguishing feature of instrumentation is not only to augment the 
program, but to actually change it. For example, debuggers rely on hardware 
and OS support to realize breakpoints, 
watchpoints and tracepoints. If these facilities are not feasible, the 
debugger has to instrument the application. 
Another use case for this kind of modifications is to fix a faulty algorithm
in a process which must not be terminated, for example long-running 
simulations or controllers. JIT compiling techniques may also be perceived as
a form of dynamic instrumentation.

A quite motivating application of dynamic instrumentation is cheating 
in playing computer games. I will use a simple (< 900 SLOC) implementation of 
the ancient classic Pacman as an example. Using a debugger, it is quite simple
(as long as the symbol table is available) to recharge lives as the player 
hits one of the hunters using either a watchpoint on the variable counting 
the remaining lives or by activating a breakpoint on the line which changes
this variable. The problem is that this change has to be done manually every
time one gets eaten. It would be convenient to insert code which would 
reset the variable as it gets decremented or to delete the statement 
performing the decrementation.

gdb is able to change the code segment of a process, but this is quite
complicated if the compiler optimized the binary. To perform large 
changes the user has to compile a new code segment and to load it into memory.
All in all, a possibly time-consuming process with many sources for errors,
for example a misunderstanding about the layout of the process' memory 
layout or using wrong addresses (transcription errors) in the forged code 
segment. The goal of this project is to make changing the code of running 
programs a tad easier. 

The output of this bachelor thesis shall be a debugging tool which, aside from
common debugger features like breakpoints, watchpoints and examining the 
stack, enables the user to examine the code executed by the program and to
make changes to it. Since the tool shall have a command-line interface,
suitable ways to examine and change source code have to be devised.

To not have to implement a toolkit for instrumentation (certainly
a complicated, time-consuming task given the vast amount of platforms, OSs and
object file formats), the DyninstAPI will be used. It was factored out from a 
profiling tool developed at the Universities of Wisconsin-Madison and 
Maryland, called Paradyn. It consists of several independent components to
control and manipulate processes, to query their symbol table (if they still
have one), to convert the machine code into a control flow graph (which is 
very important if the compiler's optimizer was active and/or no debugging info is present) and to create and inject code into processes. 

Using this language restricts the choice of programming language to C++ 
because apparently there are no bindings for other languages yet. 
The development process will follow an iterative, agile model. The 
issue-tracker provided by Bitbucket (where the code will be hosted in a Git 
repository) will be used to steer and track development. 
