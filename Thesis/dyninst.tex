\chapter{The Dyninst Library}

The Dyninst library was developed at the University of Wisconsin as part of a profiling tool. The library also proved to be useful for other tasks and was developed independently since then.

The library consists of several component libraries. Some provide basic features while others build on existing ones. Figure~\ref{fig:DyninstArchitecture} shows how these libraries are related. The term ``mutator'' is used for the process which uses the Dyninst library to modify another process, the ``mutatee''.

\begin{figure}[h]
	\includegraphics[scale=0.5]{Dyninst_Architecture.png}
	\caption{The architecture of Dyninst.}
	\label{fig:DyninstArchitecture}
\end{figure}

\section{ProcControl API}

The ProcControl~API abstracts the operating system's system calls and debugging interfaces. It is used to control the execution state of processes and their threads and to implement debugger features like breakpoints. 

The ProcControl~API also provides an interprocess communication facility. It stops the execution of a thread in the target process and makes it execute some code provided by the API user before the normal control flow resumes. The result of the computation is then send back to the debugging process. However, this facility is quite low-level because the interface demands machine code (with a trap instruction at the end) as input. To use it one would have to implement a code generator.

\section{InstructionAPI}

The InstructionAPI provides data types to represent machine code as platform-independent abstract instructions and a disassembler to decode machine code into a stream of such instructions. Furthermore, they can be translated back into assembly code. This library could therefore be used as a low-level intermediate representation in a compiler. It contains no assembler though.

\section{ParseAPI}

The ParseAPI also parses machine code, but unlike the InstructionAPI it extracts higher-level information in form of a Control Flow Graph (CFG), consisting of instructions, basic blocks, branches, functions and code objects (a contiguous sequence of functions).
It also provides methods to navigate through this graph. The API is very extensible and 
can even be used by a process to parse itself.

\section{PatchAPI}

The PatchAPI extends the ParseAPI and allows the user to insert or delete snippets in the \ac{CFG} of an application. This graph is composed of modules, functions, basic blocks and the control-flow edges connecting them. Snippets are code which are inserted by the mutator into the mutatee. The PatchAPI is also able to manipulate the \acs{CFG} of an application.

\section{SymtabAPI}

The SymtabAPI is used to parse the debugging information of a process or binary. It gives access to information about modules, functions, variables and types and contains an interface to map addresses to line numbers and back. It is extensible in the sense that users can perform sophisticated analyses on the target on their own (for examples, to discover variables and functions in a stripped binary) and make the information available to the library.

\section{StackwalkerAPI}

The StackwalkerAPI can be used to obtain a list of stack frames of a thread's callstack both of the calling process and of other process. Also this API is extensible by the user to account for special stack layouts. For example, runtime environments for dynamic languages often store secondary callstacks on the heap to implement continuations and coroutines.

\section{DyninstAPI}

The DyninstAPI, also called the BPatchAPI, presents a unified interface to all component libraries and adds some features on its own:

\begin{itemize}
\item An implementation of the Composite pattern to construct instrumentation snippets. They are type-checked (optional), compiled and assembled to be inserted into the target application.

\item A mutatee can be instructed to execute a snippet immediately and to return the results. This feature is implemented using the ProcControl~API.

\item The DyninstAPI loads a runtime library into the target application. This library contains important features for snippets, for example for controlling the mutatee and to communicate with the mutator.

\end{itemize}

The DyninstAPI is the oldest interface exposed by the library. As new applications arose which did not need the whole interface and the capabilities of the library increased, it was split and the old interface was retained.

The DyninstAPI allows the API user to convert some of its business object into the underlying abstractions because they often provide a cleaner and more powerful interface. 
In general, it is not possible to go the other way, for example constructing snippets with the PatchAPI and using the DyninstAPI to insert them into the process.
The only library where this is not possible is the ProcControl API because the developers deemed it impossible to keep its business objects synchronized with the internal state of the DyninstAPI. This makes using the breakpoint features of the ProcControl API together with the DyninstAPI impossible.

\section{dynC library}

The dynC library contains a compiler which translates code fragments written in the dynC language (a dialect of C) into snippets for the DyninstAPI.

The dynC language is a C dialect which is severely restricted in comparison to a full-fleged language. There are
\begin{itemize}
\item no loop constructs,
\item no C preprocessor,
\item no functions and
\item available data types are restricted to integral types and pointers.
\end{itemize}

The language provides an extended syntax for identifiers, as seen in Figure~\ref{fig:breakpointUsingDynC}. A backtick is used to divide the name into two parts. The first part denotes the domain and the second denotes the actual symbol name.

The domains are:
\begin{itemize}
\item The inf domain contains all functions of the program being instrumented.
\item The dyninst domain allows access to utility function such as break (which executes a breakpoint).
\item The local and param domains provide access to the local variables and parameters of the function where the snippet is inserted. 
\item The global domain contains all global variables of the program being instrumented.
\item The default domain (assumed if the backtick syntax is not used) contains variables which are only visible to the current snippet. 
\end{itemize}

% "^^ " is a hack to get the backtick character. The ^^ operator adds 32 to the code of the following character.
\begin{figure}[h]
\begin{lstlisting}[language=C]
if (local!?^^ ?!j > 4) {
	unsigned breakpointId = 4;
	inf!?^^ ?!DYNINSTuserMessage(&breakpointId, sizeof(breakpointId);
	dyninst!?^^ ?!break();
}
\end{lstlisting}
\caption{A dynC snippet implementing a conditional breakpoint}
\label{fig:breakpointUsingDynC}
\end{figure}