\chapter{Related work}

The GDB\cite{GDB} debugger is, for many programmers, the first debugger they have to use. It is available for many platforms and has a large feature set. There is also an interfaces available to integrate it into other application, for example a graphical debugger. It is also able to manipulate the machine code of its debuggee, but the feature is very limited: the user has to assembly the code by himself, to search for some code segment where to place the code and to integrate it properly into the application.

The DyninstAPI\cite{Buck00anapi} was created as part of the Paradyn\cite{Miller95theparadyn} project, which uses instrumentation to obtain performance metrics from and identify bottlenecks in unmodified binaries. Since then the DyninstAPI has been split off and extended, and over the years people have used it to built many tools concerned with debugging and profiling software\cite{DyninstTools}.

Dyninst is by far not the only toolkit providing dynamic instrumentation facilities. There are currently two approaches to dynamic instrumentation: probe-based and \ac{JIT}-based. Probe-based systems like Dyninst, DTrace\cite{cantrill2004dynamic} and Vulcan\cite{Edwards01vulcanbinary} modify the existing code and use trampoline stacks to achieve their functionality. This approach is not entirely transparent to the application because it may incur performance overhead.

\acs{JIT}-based systems like Pin\cite{luk2005pin} and DynamoRIO\cite{bruening2004efficient} use a \acs{JIT}-compiler to integrate instrumentation code into existing code. If done correctly the application is not able to detect the presence of instrumentation, the instrumentation code is not required to use trampolines to run and, in the case of Pin, an optimizer minimizes performance overhead. The disadvantage is that debugging information included in the binary may be rendered useless because misleading.

King et.al. present the idea of a \ac{TTVM} to cope with the challenges of OS debugging\cite{King05debuggingoperating}. \acs{TTVM}s deal with the following problems:
\begin{itemize}
\item Non-determinism: \acs{TTVM}s record the exact order of events (crucial to debug race conditions).

\item Long running times: \acs{TTVM}s makes it unnecessary to rerun the debuggee to get to the fault. Also it allows to go back in time if the cause of the fault was missed.

\item Lost execution history: backtraces (which usually are the only way programmers can look back in time) can become unavailable if the stack gets corrupted or pruned (as in the case of tail-call removal\cite{Steele:1977:DLP:800179.810196} or other optimizations). Also, only looking at the induction variables is not always sufficient to debug loops (for example, determining if a particular iteration happened twice).

\item Safety: The debugger itself may be at risk if the operating system corrupts memory. Finally, as soon the system becomes unusable (for example failing keyboard drivers) the debugger also does.

\item Non-repeatability: Some events are difficult, undesireable or outright impossible to repeat, such as a war ship firing a missile. Also, stopping the debuggee on breakpoints may change execution history if it interacts with the external world.

\end{itemize}

\label{section:EditAndContinueCLR} Eaddy and Feiner present an edit-and-continue environment which allows modifications to the application code at runtime, possibly with the aid of an \acs{IDE}\cite{eaddy2005multi}. Overall, this technique is very old (already the first LISP systems\cite{Teitelman:2008:HI:1529966.1529971} were pretty good at it), but so far it worked only for languages and runtime systems designed for that purpose. Their solution instead builds on the existing Microsoft Common Language Runtime, and causes minimal performance penalties. That was important for their use case, debugging computation-intensive 3D graphics applications, which disallowed for other solutions, like embedded scripting languages.