\chapter{Features}

This section provides a description of the commands offered by the debugger to the user, what they can do and what their limitations are. Most commands take one or more arguments. These arguments are separated by whitespace characters. If an argument shall contain a whitespace character, it must be escaped with a backslash, or the token must be enclosed in single or double quote characters.

\section{Starting the debugger}

If the debugger is started without arguments it doesn't connects to no process, enters the main loop and expects commands from the user. In this mode the debugger is restricted to debug processes on the local machine.

If the word ``load'' is specified as the first argument then the debugger expects the filename of an executable file as the next arguments. That program can then be started by the debugger as debuggees. All remaining arguments will be passed to the debuggee when the \verb+start+ or \verb+start_hold+ command is used.

The argument ``attach'' instructs the debugger to connect to an existing process. Its \ac{PID} is expected as the next parameter.

To start the debugger in server mode the argument ``server'' has to be specified. An optional argument is the \acs{TCP} port number where the server shall listen for connections.

To connect to a remote server the argument ``remote'' is required. The hostname of the server is expected as the second argument. A third, optional argument is the port number of the remote host where the server listens for connections.

\section{Process control}

These commands allow the user to attach to processes and to control their execution state. 

\subsection{Starting processes}

Processes can be started by the debugger in two ways. With the \verb+file+ command an executable file is loaded. The program can then be run with the \verb+start+ command which uses the most recent list of arguments (or the ones from when the debugger was started). To display the current argument list the \verb+show_args+ command can be used. 

With the \verb+run+ command a new list of arguments can be specified. When the suffix \verb+_hold+ is appended to the command (both \verb+run+ and \verb+start+) then the debuggee is suspended immediately after its creation. Figure~\ref{fig:StartingAndDisplayingArguments} demonstrates these commands.

The debuggee is going to share input and output streams with the debugger. It is therefore recommended to not start a program which prints lots of output or draws on the terminal. 

\begin{figure}[h]
\begin{lstlisting}
!?\$?! !?\textbf{dyndebugger load /bin/echo This is a message}?!
!?\textgreater?! !?\textbf{show\_args}?!
+-------+-----------+
| Index | Argument  |
+-------+-----------+
|   0   | /bin/echo |
|   1   |   This    |
|   2   |    is     |
|   3   |     a     |
|   4   |  message  |
!?\textgreater?! !?\textbf{start\_hold}?!
/bin/echo started and suspended
!?\textgreater?! !?\textbf{continue}?!
Process continues
This is a message
Process terminated with code 0
\end{lstlisting}
\caption{Example for starting a process and displaying its arguments.}
\label{fig:StartingAndDisplayingArguments}
\end{figure}

\subsection{Attaching and detaching}

The \verb+attach+ command requires the \ac{PID} of the process the user wants to debug. This might fail because of missing access privileges. Also, the debugger is not able to attach to a process more than once because of limitations of Dyninst library.

When the user is no longer interested in debugging the connection to the debuggee can be closed with the \verb+detach+ command. It is also possible to forcefully terminate a debuggee with the \verb+kill+ command. 

\subsection{Controlling execution state}

The process can be stopped using the \verb+stop+ command and resumed with \verb+continue+. Unlike \verb+gdb+, the \verb+continue+ command doesn't block until the process stops next time. This was done because in the author's opinion it is useful to be able to query information from the symbol table or to print some data while the program is executing. To actually wait for the process to stop, use the \verb+wait+ command.

Only the process as a whole can be stopped and continued, not individual threads. Nevertheless, there is the notion of a ``current thread''. Initially this is the only thread active when a program starts, but it is possible to mark another thread as such with the \verb+switch+ command. 

When a function is stopped the instance of the the currently executing function is regarded as the current stackframe. The current stackframe is important because commands which use expressions (and therefore variable references) have limited access to the local variables and parameters of the currently executing function. The \verb+frame+ command is used to select the frame, where \verb+#0+ represents the currently active function call. 

\section{Program information}

Access to the symbol table is important to effectively debug an application. While debuggers can work without any debug information, this is no pleasurable experience. There are also other kinds of interesting information available besides debug information. 

\subsection{Variables and functions}

The command \verb+find_variable+ and \verb+find_function+ query the symbol table about information on specified variable and function names. These may contain regular expressions. The \verb+find_type+ command looks for types, but for these no regular expressions are available. 

The \verb+info_variables+ and \verb+info_functions+ commands print a list of all available variables or functions. Their output can be restricted by specifying a module name. To reduce typing effort, only a unique part of the name is necessary. The \verb+info_modules+ command tells which modules are available. There is always going to be a ``DEFAULT\_MODULE'' which contains all symbols with whom the linker didn't associate a particular module when the program was built.

With the \verb+backtrace+ command the list of active function calls (the stacktrace) of the stopped process can be displayed (see Figure~\ref{fig:Stacktrace} for an example). Since there may be more than one thread of execution, the stacktrace of the default thread selected using the \verb+switch+ command is displayes. The \verb+print_scope+ command analyzes the current stackframe and prints information on available local variables and parameters. As already mentioned, the \verb+frame+ command can be used to select the current frame. See Figure~\ref{fig:PrintScope} for an example, which analyzes the stacktrace of Figure~\ref{fig:Stacktrace}.

\begin{figure}[h]
\begin{lstlisting}
!?\textgreater?! !?\textbf{backtrace}?!
+-----+--------------------+-------------------+------------+
| Pos |   Return Address   |       Name        | Frame type |
+-----+--------------------+-------------------+------------+
| #0  | 0x00000033f72eb843 | __select_nocancel |            |
| #1  | 0x0000003403016ce4 |  _nc_timed_wait   |            |
| #2  | 0x0000003eede0cb4b |    _nc_wgetch     |            |
| #3  | 0x0000003eede0d6f5 |      wgetch       |            |
| #4  | 0x000000000040210d |     GetInput      |            |
| #5  | 0x0000000000401b6e |       Delay       |            |
| #6  | 0x0000000000402fc6 |     MainLoop      |            |
| #7  | 0x00000000004017a5 |       main        |            |
| #8  | 0x00000033f7221a05 | __libc_start_main |            |
| #9  | 0x0000000000401589 |      _start       |            |
\end{lstlisting}
\caption{An example stacktrace of the game Pacman for Console\cite{Pacman}.}
\label{fig:Stacktrace}
\end{figure}

\begin{figure}
	\begin{lstlisting}
!?\textgreater?! !?\textbf{frame 7}?!
+-----+--------------------+------+------------+
| Pos |   Return Address   | Name | Frame type |
+-----+--------------------+------+------------+
|  7  | 0x00000000004017a5 | main |            |
!?\textgreater?! !?\textbf{print\_scope}?!
Parameters: 
+------+--------------------+------+--------+
| Name |      Address       | Size |  Type  |
+------+--------------------+------+--------+
| argc | 0x00007fff406e20cc |  4   |  int   |
| argv | 0x00007fff406e20c0 |  8   | char** |

Local variables:
+------+--------------------+------+------+
| Name |      Address       | Size | Type |
+------+--------------------+------+------+
|  j   | 0x00007fff406e20dc |  4   | int  |
	\end{lstlisting}
	\caption{Anaylzing the stacktrace of Figure~\ref{fig:Stacktrace}}
	\label{fig:PrintScope}
\end{figure}

\subsection{Expressions}

The debugger can also parse and execute simple computations on the debuggees' data. The results can be displayed in the debugger. The expression language follows C language \cite{Kernighan:1988:CPL:576122} syntax quite closely, but there are some differences and restrictions:

\begin{itemize}

\item There are no binary shift operators because Dyninst doesn't implement them.

\item No functions can be called; Dyninst is for some reason not able to determine the result types of functions using the debug information, and is therefore not able to generate code when function calls are involved.

\item The conditional expression might not work as expected; as always, parentheses are recommended to disambiguate it. 

\item No floating-point numbers.

\item The accessible domain of variables are the global variables and the local variables and parameters of the function which belongs to the currently selected stack frame.

\item No register contents can be used because there is no obvious way to introduce the type of their contents into the code generator.

\end{itemize}

The command which allows to do so is named \verb+print+. To allow the user to call functions in the debuggee the \verb+call+ command can be used. It takes as arguments the function name and a list of expressions which are evaluated, with the results as arguments to the function. No separator characters other than whitespaces are necessary. 

The results of the \verb+print+ program are pretty-printed. This is important for structured and array types. Also value names of enumerated types can be printed. 

\section{Breakpoints}

Breakpoints come in two flavors: conditional and unconditional. When a conditional breakpoint is encountered, its associated condition decides whether the debuggee actually stops.
The command \verb+break+ is used to create both conditional and unconditional breakpoints. The location where the breakpoint is placed can be specified using three formats: 

\begin{itemize}

\item The format \verb+<module>:<line-number>+ specifies that the breakpoint shall be placed in the specified module at the specified line. It will be triggered before the line is executed.

\item The format \verb+<module>:<name>+ specifies that the breakpoint shall trigger before the function in the module is executed. This may be necessary if multiple functions with the same name, but confined to their own modules, exist (static linkage).

\item If only a name is specified it is assumed to be the name of a function, and the breakpoint will stop the program before executing it.

\end{itemize}

After the location an expression can be specified. The breakpoint will then stop the program only if this expression evaluates to \textbf{true} (in C terms, everything equivalent to $0$ is considered to be \textbf{false}, and everything else to be \textbf{true}). Unfortunately, the expression may only use global variables because with the current implementation it would be necessary to access registers to determine the location of variables on the stack\footnote{In principle, the local variables of the current frame can also be used, but this is dangerous because these references become invalid when the call associated with the current frame terminates.}.

The \verb+ignore+ command specifies how many times a breakpoint shall be ignored before it actually stops the debuggee. This is very useful when debugging loops and recursive function calls.

When the debuggee hits a breakpoint the debugger is notified. It then decrements the ignore count and notifies the user. The user can also specify a list of debugger commands which shall be executed when the breakpoint is hit. 

With the command \verb+commands+ actions can be attached to breakpoints. Whenever a breakpoint is encountered these actions are executed by the debugger. The command takes as arguments a number of breakpoint IDs. If none are given the actions are applied to the most recently added breakpoint. After starting the command it expects a list of actions, each on its input line. These are the usual debugger commands. The list is terminated by the ``command'' \verb+end+. See Section~\ref{section:Pacman} for a good use case of this feature. 

Another good use case would be to evaluate expressions whenever a breakpoint is hit instead of stopping the debuggee. This would be a replacement for ``printf''-style debugging habits.

The \verb+delete_breakpoint+ command can be used to remove breakpoints. It expects as arguments a list of breakpoints IDs. Breakpoints are automatically removed when the debugger detaches to not disturb the debuggee any further. 

The command \verb+watch+ creates a watchpoint. Watchpoints stop the debuggee as soon as a specified variable is accessed. Because they require instrumenting all memory accesses (very expensive) they are limited to a module, which is specified after the variable name.

\section{Manipulation}

The \verb+insert_set+ command takes a location, in the format accepted by the \verb+break+ command, a variable name and an expression. It inserts a snippet at the specified location which will set the value of the variable to the result of the expression every time it is encountered. The expression can use the old value of the variable. 

The \verb+set+ command also changes the value of a variable, but it does so immediately, without modifying the debuggee's code. Its syntax is the same as the one of \verb+insert_set+, except that no location has to be specified.

The \verb+insert_call+ command takes a location, a function name and a list of expressions, separated by whitespace characters, and inserts a call to the specified function in the debuggee. Before calling the function the expressions are evaluated and passed as arguments. 