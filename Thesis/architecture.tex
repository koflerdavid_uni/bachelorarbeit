\chapter{Debugger architecture}

This section explains how the debugger integrates with the Dyninst library and the user. The DyninstAPI uses events and callbacks to communicate state changes of the mutatee to the mutator. As a result the debugger has to be thread-safe. Also, Debugger objects may start or attach to multiple processes over their lifetime. The resulting architecture loosely follows the Model-View-Controller pattern, with the Main loops being the views, the Debugger object being the controller and the DyninstAPI handle for a process being the model. 

In the following chapter and in the implementation chapter words in bold face (like \textbf{Debugger}) refer to names of \verb-C++- classes.

\begin{figure}[h]
	\includegraphics[scale=0.4]{LocalArchitecture.png}
	\caption{A dataflow diagram for a debugging session when debugging locally. Grey boxes denote processes.}
	\label{fig:LocalArchitecture}
\end{figure}

\section{The Debugger component}

The most important component is the \textbf{Debugger} class. It contains the \newline \textbf{BPatch\_process} handle connecting it to the debuggee and is responsible for executing debugger commands and handling events (most importantly breakpoints). For this, each instance of the component manages its own event handler thread. 

To interface with the user and with the debugger commands three callbacks have to be provided by the user. These are for

\begin{itemize}

\item reading a line of input from the user,

\item printing the output of a command and

\item printing the output of a event handler. 

\end{itemize}

The DyninstAPI provides an interface to handle Dyninst events, but the \textbf{Debugger} component doesn't use it by itself because they are global. It would be presumptuous to call them because there might be other code using the Dyninst library which possibly expect to be its only users (other \textbf{Debugger} instances for example, as when the debugger is run in server-mode).

\section{The Event Handler Thread}

The main event loop runs in its own thread and is implemented to depend only on the Dyninst library. It gets started when the debugger is run. Its task is to handle all events emanating from the Dyninst library. This is necessary because Dyninst event handling is managed by a Singleton class. An important design criteria was that the loop shall be able to be terminated at any time. This is important because otherwise the debugger would not be able to quit at the end of the debugging session.

\section{Debugger commands}

The \textbf{Debugger} class uses the \textbf{CommandTable} class to associate command names with the functions implementing them. The header file where this class is defined also defines the interface for debugger commands, along with a macro which generates the header of the function implementing the command. The interface gives the commands access to the Debugger structure, to the debuggee and to input and output facilities. The input and output facilities are provided by the debugger and are often redirected to buffers whose contents are transmitted over the network. Additionally a \textbf{DebuggerException} can be thrown for serious errors, which will be rendered as an error message.

\section{Main Loops}

The user interface is contained in implementations of the \textbf{AbstractMainLoop} class. There are currently two implementations. Both of them read an input line from the terminal and take care of passing it to a \textbf{Debugger} structure. The first implementation ( \textbf{LocalMainLoop}) interacts with a Debugger structure in the same process, to debug processes on the local machine. In Figure~\ref{fig:LocalArchitecture} a user debugs a single process running on the same machine as the debugger. The second implementation (\textbf{RemoteMainLoop}) connects with a debugger running on a remote machine. In Figure~\ref{fig:RemoteArchitecture} the user debugs multiple processes on the remote machine and has to open a terminal for each of them.
Currently the type of main loop is selected at startup; to switch to a different type the debugger needs to be restarted.\footnote{This is different than with GDB which can establish remote connections and handle multiple debuggees at the same time.}

\section{Terminal}

Both implementations of the main loop provide a text-oriented interface to the terminal. They read commands from the terminal using the GNU Readline\cite{Readline} library which provides line editing and history features. Also, file paths and user names are autocompleted.

\section{Server}

To be able to debug remote applications the debugger supports a server mode. The \textbf{Server} takes care of reading commands and other input from clients and to pass them to Debugger objects. \textbf{Debugger} objects can demand input from the client. Each time a client is handled a new thread is spawned and the server thread continues with waiting for clients. This choice was met because

\begin{itemize}

\item the user might want to debug more than one program on the remote host, but

\item the sessions are going to be few in numbers and quite long-lived.

\end{itemize}

Also, for this application it is easier to use threads because the sessions are not meant to interact with each other. 

The server doesn't include any security features; intruders can execute arbitrary commands with the rights of the user account running the server. Since it needs to use the operating system's debug interface, it is not possible to strip that account of any significant privileges. The debugger should therefore be used only inside a trusted network or the connection should be protected with a SSH tunnel or a VPN.

\begin{figure}[h]
	\includegraphics[scale=0.3]{RemoteArchitecture.png}
	\caption{The dataflow diagram for a remote debugging session. Grey boxes denote processes, yellow ones denote hosts.}
	\label{fig:RemoteArchitecture}
\end{figure}