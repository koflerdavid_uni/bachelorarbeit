\chapter{Practical Experiences}

\section{Pacman}
\label{section:Pacman}

Cheating in computer games is a very interesting and important application of debuggers. As in every branch of software development, debuggers are necessary to reproduce and analyze test cases which lead to failures\footnote{An embedded scripting language, together with an in-game console, covers many use cases which are difficult or impossible with a generic debugger, such as manipulating the game world or displaying variables.}. Though increasingly difficult (games become large and complex, compilers perform aggressive optimizations, and the publisher might be interested in preventing cheating), simple games can still be easily tricked. 

One such game is Pacman for Console v1.2\cite{Pacman} where the player controls a figure in a maze, eating food and trying to evade the ghosts. To make the game more playable and fun, each player may get caught a fixed number of times before the game is over. 

The goal of this example is to prevent the life count from dropping, thus making it impossible to lose the game. The assumption is that the amount of lives left is stored in a variable, though more complex (read: more difficult to cheat) schemes are possible. The example was executed on a Fedora 18 GNU/Linux system running on an Intel Core\texttrademark
 i7 64-bit processor. 

Since Pacman uses the terminal window to draw the interface using the NCurses\cite{NCurses}~library, the game cannot be started in the debugger. It would occupy the terminal and make debugging confusing, at best. Therefore the debugger has to attach to an existing process. The \verb+pgrep+\cite{pgrep} program (not included in the POSIX standard, but available on Solaris and GNU/Linux) will be used to find out the process id of the game.

\begin{lstlisting}
!?\$?! !?\textbf{dyndebugger attach `pgrep pacman`}?!
Attaching to 3460
!?\textgreater?!
\end{lstlisting}

First of all, it is useful to get an overview of how the program is structured. The command \verb+info_modules+ shows us that there is \verb+pacman.c+, lots of libraries and the \verb+DEFAULT_MODULE+ module\footnote{Dyninst puts there all symbols it couldn't confidently associate with a particular module.}. In fact, the \verb+pacman.c+ module turns out to be quite empty. 

\begin{lstlisting}
!?\textgreater?! !?\textbf{info modules}?!
+---------------------------+--------+
|           Name            | Shared |
+---------------------------+--------+
|         pacman.c          |        |
|      DEFAULT_MODULE       |        |
|        crtstuff.c         |        |
|     libncurses.so.5.9     |   *    |
|      libtinfo.so.5.9      |   *    |
|       libc-2.16.so        |   *    |
|       libdl-2.16.so       |   *    |
|        ld-2.16.so         |   *    |
| libdyninstAPI_RT.so.8.1.2 |   *    |
\end{lstlisting}

The \verb+info_variables+ command shows that the \verb+pacman.c+ module contains only three variables, while the others are contained in the default module, especially the interesting ones. One variable stands out there, the one called \verb+Lives+. 

\begin{lstlisting}
> !?\textbf{info\_variables pacman.c}?!
+-------------------+------------+------+-----------+
|       Name        |  Address   | Size |   Type    |
+-------------------+------------+------+-----------+
| SlowerGhosts.4107 | 0x00604f80 |  4   | <no type> |
|    chtmp.4041     | 0x00604f84 |  4   | <no type> |
|    itime.4121     | 0x00604f7c |  4   | <no type> |
\end{lstlisting}

\begin{lstlisting}
!?\textgreater?! !?\textbf{info\_variables DEFAULT\_MODULE }?!
+----------------+------------+------+---------+
|      Name      |  Address   | Size |  Type   |
+----------------+------------+------+---------+
.
.
.
|     HowSlow    | 0x006041ac |  4   |   int   |
|   Invincible   | 0x006042a8 |  4   |   int   |
|      Level     | 0x006042c0 | 3248 | int[][] |
|    LevelFile   | 0x00604140 | 100  | char[]  |
|   LevelNumber  | 0x00604f70 |  4   |   int   |
|      !?\textbf{Lives }?!     | 0x006041a8 |  4   |   int   |
|       Loc      | 0x00604200 |  40  | int[][] |
|     Points     | 0x006041e0 |  4   |   int   |
|   SpeedOfGame  | 0x006041b0 |  4   |   int   |
| StartingPoints | 0x00604280 |  40  | int[][] |
.
.
.
\end{lstlisting}

To make sense of what \verb+Lives+ means, we use the \verb+print+ command. It contains the value 3, which is luckily the amount of lives the player currently has. But does this value also decrease when the player meets one of the ghosts? To test that one out, a breakpoint will be used. But to set one, a guess it necessary of where \verb+Lives+ is going to be modified. 

\begin{lstlisting}
!?\textgreater?! !?\textbf{print Lives}?!
3
\end{lstlisting}

After inspecting both the default module and \verb+pacman.c+, it turns out that the application's functions are located in \verb+pacman.c+. There are only two interesting functions. What about \verb+MainLoop+? Let's set a breakpoint on \verb+MainLoop+ (using \verb+break MainLoop+) and play around a bit! It turns then out that the breakpoint is not going to be hit. 

\begin{lstlisting}
!?\textgreater?! !?\textbf{info\_functions pacman.c}?!
CheckScreenSize() : void
main(argc : int, argv : char**) : void
CreateWindows(y : int, x : int, y0 : int, x0 : int) : void
ExitProgram(message : char*) : void
GetInput() : void
InitCurses() : void
IntroScreen() : void
Delay() : void
DrawWindow() : void
CheckCollision() : void
LoadLevel(levelfile : char*) : void
MainLoop() : void
MoveGhosts() : void
MovePacman() : void
PauseGame() : void
\end{lstlisting}

The other function is called \verb+CheckCollision+. On continuing the breakpoints is hit immediately since the function is called, as its name implies, to check whether there is a collision. To not to have to step through so many iterations, a conditional breakpoint, triggered when there is one live less than currently, seems to be a good idea. Upon continuing (and steering the player into a ghost), the breakpoint hits. Inspecting \verb+Lives+ indeed says it has the value 2. If the variable is set to 3, the live count goes up, as evidenced by the game screen. 

\begin{lstlisting}
!?\textgreater?! !?\textbf{delete\_breakpoint 2}?!
!?\textgreater?! !?\textbf{break CheckCollision Lives == 2}?!
#3: function: CheckCollision
!?\textgreater?! !?\textbf{c}?!
Breakpoint 3 hit
!?\textgreater?! !?{\textbf{print Lives}?!
2
!?\textgreater?! !?\textbf{set Lives 3}?!
\end{lstlisting}

One of the goals of this projects was not only to track down ``errors'', but also to fix them. In this case the solution is to use the command \verb+insert_set+ which places an assignment (of the result of an expression to a variable) at a particular location in the  debuggee.

\begin{lstlisting}
!?\textgreater?! !?\textbf{insert\_set CheckCollision Lives 3}?!
\end{lstlisting}

\section{\ac{MPI} Bubblesort}

\acs{MPI}\cite{Forum:1994:MMI:898758} is a message-passing library and runtime environment to develop parallel applications. It is intended to be used on distributed-memory systems. It includes networking and messaging primitives as well as higher-level algorithms to distribute, transform and aggregate data using multiple processes. 

The smallest organizational unit is the process. When an \acs{MPI} application is started the user specifies how many processes shall be started, which program shall be run and how resources shall be shared across all participating processes. Processes are identified with a rank number in the range $\left[ 0, numberOfProcesses \right)$. This rank is usually used to determine the precise tasks of each process.

One of the assignments of the Proseminar "Parallele Systeme" in Fall 2013/14 was to develop a version of Bubblesort\cite{cormen2001introduction} which would use the \acs{MPI} library to distribute the workload across multiple processes. Recently, when I ran the code on my machine using the OpenMPI v1.6.3 implementation I discovered the following error message: 

\begin{lstlisting}
[localhost:4260] *** An error occurred in MPI_Isend
[localhost:4260] *** on communicator MPI_COMM_WORLD
[localhost:4260] *** MPI_ERR_RANK: invalid rank
[localhost:4260] *** MPI_ERRORS_ARE_FATAL: your MPI 
job will now abort
\end{lstlisting}

The program splits the workload into $numberOfProcesses$ chunk. Each process performs the inner iterations of the Bubblesort algorithm on them. When it is finished it sends the top-most element to the process which received the upper chunk and the bottom-most element to the lower chunk. The received elements are then compared to the top-most and bottom-most elements of the chunks. If they are smaller or greater respectively they replace the top-most or bottom-most element. This process is repeated until there is no more change.

The processes which received the top-most and bottom-most chunks perform only one of the exchange operations at a time, and the failure to do so is the error in the present implementation.

\begin{figure}
\begin{lstlisting}[language=C]
int bottom;

// Send top items and receive bottom items
if (rank == 0) {
	if (MPI_Send(&chunk[chunkSize - 1], 1, MPI_UNSIGNED,
		1, 0, MPI_COMM_WORLD) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}

} else if (rank < size - 1) {
	MPI_Isend(&chunk[chunkSize - 1], 1, MPI_UNSIGNED, 
		rank + 1, 0, MPI_COMM_WORLD, &requests[0]);
	MPI_Irecv(&bottom, 1, MPI_UNSIGNED, rank - 1, 0, 
		MPI_COMM_WORLD, &requests[1]);

	if (MPI_Waitall(2, requests, statuses) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}

} else {
	assert(rank == size - 1);
	if (MPI_Recv(&bottom, 1, MPI_UNSIGNED, size - 2, 0, 
			MPI_COMM_WORLD, &readStatus) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}
}
\end{lstlisting}
\caption{The part of the `bubblesort` algorithm which sends the bottom-most element of each chunk to the process to the left.}
\end{figure}

\begin{figure}
\begin{lstlisting}[language=C]
int top;

// Send bottom items and receive top items

if (rank == size - 1) {
	if (MPI_Send(&chunk[0], 1, MPI_UNSIGNED, size - 2, 0,
			MPI_COMM_WORLD) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}

} else if (rank > 0) {
	MPI_Isend(&chunk[0], 1, MPI_UNSIGNED, rank - 1, 0, 
		MPI_COMM_WORLD, &requests[0]);
	MPI_Irecv(&top, 1, MPI_UNSIGNED, rank + 1, 0, 
		MPI_COMM_WORLD, &requests[1]);

	if (MPI_Waitall(2, requests, statuses) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}

} else {
	assert(rank == 0);
	if (MPI_Recv(&top, 1, MPI_UNSIGNED, 1, 0, 
			MPI_COMM_WORLD, &readStatus) != MPI_SUCCESS) {
		fprintf(stderr, "Communication error\n");
		exit(1);
	}
}
\end{lstlisting}
\caption{The part of the `bubblesort` algorithm which send the top-most element of each chunk to the process to the right.}
\end{figure}

To investigate the error and to test the debugger's remote interface I launched a debugger server and connected four clients to it (because the \acs{MPI} application used four processes). These clients were instructed to attach themselves to the \acs{MPI} processes.

In the source code of the program there were only two occurrences of \verb+MPI_Isend+, so I created breakpoints on both of them. As I investigated the backtraces, I discovered that the process with rank 3  (the right-most one) already stopped because of the error. This is odd because that process should not even call \verb+MPI_Isend+ in the first place. The right-most process should only ever receive an element or send one using the blocking send/receive functions.

\begin{figure}
\begin{lstlisting}
!?\textgreater?! !?\textbf{attach 4260}?!
Attaching to 4260

!?\textgreater?! !?\textbf{bt}?!
+-----+----------------+---------------------------------------
| Pos | Return Address |                  Name                  
+-----+----------------+---------------------------------------
| #0  | 0x0033f72f2b23 |            __GI_epoll_wait             
| #1  | 0x003ee64f50d1 |                                        
| #2  | 0x003ee64f6f90 |          opal_event_base_loop          
| #3  | 0x003ee65190f1 |             opal_progress              
| #4  | 0x7f6b6da7b615 |                                        
| #5  | 0x003ee6463927 |           ompi_mpi_finalize            
| #6  | 0x00000040147c |            do_MPI_Finalize             
| #7  | 0x0033f7238ea1 |          __run_exit_handlers           
| #8  | 0x0033f7238f25 |                exit                  
| #9  | 0x003ee64c90b0 |        orte_ess_base_app_abort         
| #10 | 0x003ee64c8789 |      orte_errmgr_base_error_abort      
| #11 | 0x003ee646250b |             ompi_mpi_abort             
| #12 | 0x003ee6456427 | ompi_mpi_errors_are_fatal_comm_handler 
| #13 | 0x003ee6455918 |         ompi_errhandler_invoke         
| #14 | 0x003ee647ecc8 |               !?\textbf{PMPI\_Isend}?!               
| #15 | 0x0000004017fc |             bubblesort_mpi             
| #16 | 0x000000401600 |                  main                  
| #17 | 0x0033f7221a05 |           __libc_start_main            
| #18 | 0x0000004012d9 |                 _start                 
\end{lstlisting}
\caption{The backtrace of the \acs{MPI} process where the error occured. The call frame for ``MPI\_Isend'' is highlighted.}
\end{figure}

Finally it turned out that the \verb+size+ variable did not denote the number of participating nodes, but the size of the array. This error was introduced by a refactoring performed on the source code which moved the entire algorithm into its own function. After fixing the error by setting \verb+size+ to the number of processors the algorithm worked flawlessly. The fix could unfortunately not be applied to the already running application because the failure already disrupted the algorithm and crashed the process.