\chapter{Implementation}

The topic of this chapter is the actual implementation of the debugger's features. By reading this chapter it should become easier to understand the code and to make changes. 

\section{Main loop}

The main loop acts as the user interface. Its operation is as follows: First, the \textbf{Debugger} object is created. Then the event handler thread handling output from the \textbf{Debugger} object is created. Lastly, the main \ac{REPL} starts: It reads commands using GNU Readline\cite{Readline}, parses the input, executes commands and prints their output. If the user wants to quit the debugger the event loop is notified about that and the loop terminates.

\section{Event loops}

The Dyninst library has event-based interfaces to enable customization and extension. Many of these events are important for our use cases because messages from the debuggee are handled this way. According to the documentation, the Dyninst library is thread-safe, but the user code inside event handlers not necessarily is. For this reason Dyninst wants the user to determine when the events shall be handled. This is done by calling the following methods:
\begin{itemize}
\item \verb+BPatch::pollForStatusChange+: This method handles pending events, but doesn't wait for them.
\item \verb+BPatch::waitForStatusChange+: This method waits for events and handles them.
\end{itemize}

The trouble is that both of these methods are by themselves unsuitable for the task at hand. The first method would require the event handler to busy-wait or sleep for small time slices. The second would make it difficult for the main loop to terminate the application because \verb+BPatch::waitForStatusChange+ would block infinitely. Fortunately, Dyninst provides  \verb+BPatch::getNotificationFD+, which returns the handle of a pipe. Whenever an event occurs a character is written into this pipe. 

The solution is to create a pipe to communicate with the main loop and to use \verb+select+\cite{specifications2008draft} to wait for events on both handles. If a character arrives on the pipe specified by \verb+BPatch::getNotificationFD+ then \verb+BPatch::pollForStatusChange+ is called to handle it. After handling the Dyninst events the debuggee state is checked and the user is notified if something changed. Then the breakpoints are processed. If a character arrives at the pipe from the main loop, the event loops terminates. 

There is one event loop per \textbf{Debugger} instance, therefore a lock has to be used to make sure that nobody (the main loop, to be precise) messes with the debugger structure while handling events and possibly executing commands.

\section{Event handlers}

Since the DyninstAPI's event handlers are global additional measures are necessary to route the events to the right recipients. Two types of events are of interest. The first type are exit events which are generated when a debuggee terminates. There is at most one handler active for this type of event. Therefore the \textbf{Debugger} class must save the old handler and the new handler must call it. 
The other type are ``User'' events. They are the endpoints of a message-passing interface which the debugge can use to communicate with the debugger. For this the function \verb+DYNINSTuserMessage+ (contained in the Dyninst runtime library) has to be used. The handler is told which thread sent this message and a pointer to the actual data the debugger sent. The handler has then the task to determine the Debugger instance responsible for handling the breakpoint. This is done by querying a map where each \textbf{Debugger} object registers itself as it attaches to a process. 

\section{Remote interface}

The remote interface between client and debugger server uses a simple message protocol. The client is purposefully implemented as a thin client because otherwise it would have been necessary to sent huge data structures over the network. Also, the layout of many of these structures differ between platforms, creating incompatibilities if used elsewhere. The messages to the server consist of a 64-bit number in Big Endian byte order which indicates how long the following sequence of characters is.

The message format for the server's response is more complicated. There are three types of responses, which may consist of multiple messages. Each message carries a header which is one byte long. This header specifies the message type. Figure~\ref{fig:RemoteInteraction} demonstrates the protocol.

\subsection{Output response and error messages}

These responses are used for normal debugger output and for error messages. Each message consists of the aforementioned header, a 64-bit Big Endian number indicating how many characters follow, and finally the character sequence. Error responses consist of only a single message, but regular output can be split, where each part is treated as a line by the client. The last part of an output response carries a different header so that the client can recognize it as such.

\subsection{Input}

The server can send a message to the client which demands a single line of input. The next message sent to the server is then interpreted as a line of input. 

\subsection{End of session}

There is a special message type to notify the client of the end of the debugging session. The client and the server then both close the connection.

\begin{figure}[t]
	\includegraphics[scale=0.5]{Interaction.png}
	\caption{An example interaction between debugger client and remote server.}
	\label{fig:RemoteInteraction}
\end{figure}

\section{Breakpoints}

Breakpoints are one of the most fundamental tools of a debugger. They stop a process when its control flow reaches a certain code line or address. Stopping the process may depend upon a condition. The user can associate actions to be executed when the breakpoint is hit.

Usually, breakpoints depend on hardware support to be efficient. However, with the Dyninst library it is possible to implement conditional control breakpoints equally efficient by inserting a snippet to be executed right before the breakpoint location.

When it is encountered, every breakpoint first checks if its condition applies, if there is any. If the condition fails, then the breakpoint doesn't trigger. Since the \textbf{Debugger} object must be informed about the breakpoint (it's quite difficult to find out by other means what exactly happened when a debuggee stops) a function from the Dyninst runtime library\footnote{The library is loaded into the debuggee upon attaching to or starting it.}, called \verb+DYNINSTuserMessage+, is used to send the breakpoint ID to the debugger before the debuggee stops.

One of the event loop's jobs is to handle the messages sent using the function \verb+DYNINSTuserMessage+\footnote{There could be multiple users of this facility beside the breakpoints, but currently they are the only ones. If that changes, introducing a tag at the beginning of the message to distinguish the recipients would become necessary.}. The documentation tells that the function works asynchronously, but it turned out that this statement has to be interpreted carefully. It turned out that it actually awaits the termination of the corresponding handler in the debugger. For this reason it is not possible to handle the breakpoint event directly in the handler. This issue is solved by inserting the breakpoint handle in a queue. Each \textbf{Debugger} object has its own queue, and the respective event handling threads wait until there is something in the queue.

When the breakpoint is finally handled by the right event handler, the handler first busy-waits until the debuggee really stopped. \newline
\verb+BPatch::waitForStatusChange+ cannot be used because, as stated above, another thread is reserved for this purpose. Then the ignore count is checked. If the ignore count is 0 then the breakpoint is handled, also the ignore count is decremented and the debuggee is instructed to proceed.

The debugger can be instructed to execute commands when a breakpoint was triggered. They are executed immediately as entered by the user, therefore syntax errors may occur when they execute. Currently, the few which make sense being executed in such a context are
\begin{itemize}
\item printing the result of an expression,
\item setting a variable's value (in the debuggee),
\item continuing or aborting the debuggee,
\item detaching from the debuggee or
\item removing a breakpoint or creating a new one.
\end{itemize}

\subsection{Breakpoint life cycle}

Breakpoints can be created as soon as there is a program where they may be applied. If the program has not yet been started, the breakpoints are put into a queue and applied as soon as the program is started. Else they are applied immediately. Breakpoints are removed automatically when the debuggee is detached.

\section{Expressions and parsers}

The debugger includes several parsers which act as helper functions. All of them have the property that they return the position where parsing ended. This is important for the commands because the argument lists often require multiple invocations of parsers.

\subsection{Tokenizer}

The tokenizer is a hand-written parser which splits a token string (where the tokens are delimited by whitespace characters) into its individual tokens. To mark whitespace characters as belonging to a token, these characters can be protected by prepending a backslash or by surrounding them using matching pairs of single or double quote characters. The user can specify how many tokens are expected to be in  the string. The function then returns also the position after the last parsed character. 

\subsection{Expression parser}

Expressions are parsed using a top-down parser, implemented with a parser combinator library. The advantage of parser combinators is that they allow a very compact notation. A normal recursive-descendent parser would require huge amounts of boilerplate code, while the syntax of using parser combinators resembles regular expressions. Also, the actions can be separated from the parsing logic. 

The language recognized by the parser is a subset of the C expression language, but it had to be changed for the ease of implementation, and restricted because of Dyninst's limitations. The two most important restrictions are the lack of function calls (because Dyninst is not always able to query result types from the debugging information), floating-point operations and string literals. Also, bit-shift operations cannot be used because the code generator doesn't implement them.

Accessing local variables proved to be surprisingly difficult. Dyninst contains multiple interfaces for searching local variables, but none of them are quite reliable. Finally, it was necessary to manually get the list of local variables from functions to get their locations inside a stack frame, to add the base address of the current frame to them and generate a \textbf{BPatch\_variableExpr} snippet using the address and the type.  This works only when the current frame is known when the snippet is compiled, for example in the \verb+print+ command.

To generate snippets which work everywhere it would be necessary to store the frame pointer stored in the register, and that doesn't work because the code generator doesn't allow to perform type casts in a straightforward way.

\section{Issues with Dyninst}

Dyninst is a huge and complex libray and is not always easy to handle, nor is its feature~set complete. This section deals with the major difficulties and idiosyncrasies of the library encountered when developing and testing the debugger.

\subsection{Installation}

The library was initially very difficult to install. At first I tried to compile it by myself, but these efforts often resulted in broken libraries and failing test programs. Since the project began to use CMake v2.8\cite{CMake} as build system the situation has improved though. The latest version of the debugger was developed with a custom-built version of Dyninst v8.2. Dyninst requires a \verb-C++-11-capable compiler\cite{Stroustrup:2013:CPL:2543987}.

\subsection{Usage}

On Linux there have some environment variables to be set to use the library. These are as follows:

\begin{itemize}
\item For the process using the library, ``DYNINSTAPI\_RT\_LIB'' has to be set to the full filename of the runtime library (``libdyninstAPI\_RT.so''), otherwise the library is not able to properly attach to a process.
\item If the shared library files is contained in an ``unusual'' location (depends on the operating system) then ``LD\_LIBRARY\_PATH'' (used to find additional shared libraries) has to be set.
\end{itemize}

Also, the code assumes that the include files are in a ``dyninst'' subdirectory of the include path. Fedora does this by default\cite{DyninstOnFedora19}. 

\subsection{Limitations}

The DyninstAPI wraps all component libraries to provide a streamlined interface to the library user. While it is still possible to use the component libraries in some places, this is not possible with the ProcControl API, which is concerned with controlling the execution of the target process. Furthermore, the ProcControl API would provide native breakpoints on both code and memory locations. Not being able to use them was a major disappointment. 
Breakpoints can be emulated with snippets, but watchpoints on memory are very expensive to implement with snippets because every write access, even in the standard libraries, could affect a given memory location. Because of this the debugger restricts watchpoints to a specified module.

The DyninstAPI code generator is not able to generate bit-shift instructions, but instead aborts the debugger because of an assertion. On closer inspection of the source code it became clear that these cases were simply not implemented. The code generator is further limited by the fact that there is no good support for casting values to other data types without extensive workarounds. This is often necessary because the code generator is not smart enough to resolve type aliases and \verb/C++/ references.

Also, a bug was spotted in the code generator. The snippet which caused the bug was a simple computation (addition of a number with a variable) and then an assignment of the result into a pre-allocated memory location. The result was a segmentation fault in the code generator. The problem can be worked around with an additional addition operation, to avoid the fault.

Another problem is that Dyninst is unable to determine the result types of functions. For this reason no usable function call snippets can be created.

The most important limitation is that the DyninstAPI (not the individual libraries) is only able to attach itself once to a target process. The reason is that it is not always possible to identify all library users and to determine whether it's safe to remove the library. According to the documentation, the exact semantics are still being specified and a future version shall resolve the problem.

Before switching to Dyninst~v8.2 each mutatee caught a SIGBUS signal when exiting, even if the mutator detaches immediately after attaching without modifying the mutatee. This could be very annoying when the return code of the debuggee is used by other applications because termination with a signal sets a different return code. The problem seems to be a handler in the runtime library called upon mutatee termination which tries to communicate with the debugger. The only harmful consequence to the program seems to be the obfuscation of the return code. Dyninst~v8.2 resolved many occurrences of this bug.