\chapter{Introduction}

Software development with  a compiled programming language consists of three phases, which are usually performed performed in the following order\footnote{This is a minimal model; a realistic software development process would include dedicated planning and testing phases}:
\begin{itemize}

\item designing the program, developing algorithms and writing code,

\item a build step (compilation and linking) and then 

\item the execution of the program.

\end{itemize}
If errors occur, the program instance has to be aborted and the process starts from an earlier phase.

To be limited to this order has the following disadvantages:

\begin{itemize}

\item All instances have to be terminated, replaced by fixed ones and restarted when it is sufficiently likely that the bug will occur. These instances are doomed to lose their current execution state and their data, either because of the bug crippling them or because of the user shutting them down. Also the effort to deploy and manage large numbers of instances must not be underestimated.

\item The damage caused by the bug might be reversible, or the extent of it small enough that the current execution state and data is still usable. Nevertheless in the general case the program might not be recoverable.

\end{itemize}

These disadvantages also slow down software development. Inexperienced developers would litter the code with print statements until the cause of the bug has been found and definitely fixed. This is problematic because this debugging code might itself be wrong, may introduce bugs and ultimately not belongs to the program. More experienced developers would use debuggers, but both groups suffer from the problem that to reach the bug case and to test the solution the program has to be restarted and/or recompiled multiple times. In case of \verb/C++/\cite{Stroustrup:2013:CPL:2543987} or with sufficiently large software this effort effectively slows down software development, thus making developers bitter and management angry.

Over the years the order of the above phases has become less restricted and the impact of its disadvantages lessened:
\begin{itemize}

\item Runtime systems of dynamic languages can detect modification of the source code, thus bugs can be fixed without having to restart the program. It may also possible to recover or to fix erroneous data. These systems are called edit-and-continue environments and usually rely on an \ac{IDE} or a specialized debugging tool. The disadvantage is that dynamic languages are usually unsuitable for high performance applications\footnote{Actually, there exist exit-and-continue systems for Microsoft's \ac{CLR} which offers decent speed. See Chapter~\ref{section:EditAndContinueCLR} for details.}.

\item A \ac{DBMS} usually supports sophisticated recovery schemes. When bugs arise and if the recovery works then data loss can be minimized because only the most recently calculated results (after the last checkpoint) gets lost.

\item Some applications (like GNU Emacs) consist of a minimal set of core functionality and an embedded programming language which is then used to implement all high-level functionality in a highly modular fashion. Then, if the core of an application itself is not affected by a failure it can be used to recover data and to fix the problem.

\item Big software projects are often split into modules, interacting with each other through well-defined interfaces, to stay maintainable. The Linux kernel and the Eclipse Platform are examples for this architectural style. Which modules are enabled can usually be decided at runtime, therefore erroneous modules can be removed, fixed and reloaded.

\item Test systems enable testing and debugging an application without having to reproduce complex use cases manually. If they are kept minimal development speed increases because compilation time drops to a minimum.

\end{itemize}

The problem with the above approaches is that they either try to minimize the damage caused by bugs or to reduce the amount of work needed to fix them. Although the edit-and-continue environments can do both, but they either use dynamic languages (the point being that programs written in them tend to be slow) or address only particular platforms. They don't address the problem itself.

The question behind this thesis is the following: Is it possible to fix errors in existing programs without having to recompile them or requiring a particular runtime environment? To answer this question a simple edit-and-continue debugging tool for arbitrary programs was developed, and its performance was evaluated. 

This thesis is structured as follows: Firstly, the architecture of the Dyninst library and its capabilities are explained because it is the most important library used by the debugging tool. Secondly the architecture of the debugging tool will be explained. After that a chapter describing the debugger's capabilities follows. The next chapter deals with implementation details and issues which were encountered. A further chapter reports experiences with the debugging tool. Finally, important related work is presented and a conclusion is drawn. 