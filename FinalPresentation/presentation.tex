\documentclass[compress]{beamer}
\usetheme{Antibes}
\usecolortheme{dolphin}

\usepackage{graphicx}
\usepackage{listings}
\usepackage{caption}
\usepackage{url}

\title{Debugging tool using dynamic instrumentation}
\author{David Kofler \newline
	Supervisor: Dr. Radu Prodan}
\date{\today}
\institute{University of Innsbruck \newline Institute of Computer Science}


\begin{document}

\begin{frame}
	\maketitle
\end{frame}


\begin{frame}
	\tableofcontents
\end{frame}



\section{Goals}
\begin{frame}{Problem statement}
	\begin{block}{Software development is too restricted}
		\begin{itemize}
		\item Write-Compile-Run
		\item Long compilation times
		\item Data loss
		\item Developer frustration
		\end{itemize}
	\end{block}
	
	\begin{block}<2>{Existing solutions}
		\begin{itemize}
		\item Edit-and-continue environments
		\item Plugin architectures
		\item Dynamic languages
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Solution}
	\begin{block}{Solution: Developing a ...}
		\begin{itemize}
		\item Simple debugging tool using DyninstAPI (called \lstinline!dyndebugger!)
		\item Terminal interface
		\item Ability to fix simple errors
		\item Requires only debug information
		\item In C++, with code hosted on Bitbucket.org
		\end{itemize}
	\end{block}
\end{frame}



\section{Features}

\begin{frame}{Starting the debugger}
	\begin{block}{Locally}
		\begin{itemize}
		\item Without arguments
		\item Attach to a process: \lstinline!./dyndebugger attach 2368!
		\item Start a process: \lstinline!./dyndebugger load /usr/bin/ls -l ~!
		\end{itemize}
	\end{block}
	
	\begin{block}{Remote interface}
		\begin{itemize}
		\item Server mode: \lstinline!./dyndebugger server!
		\item Client mode: \lstinline!./dyndebugger-client zid-gpl.uibk.ac.at!
		\end{itemize}
	\end{block}
\end{frame}


\begin{frame}{Breakpoints}
	\begin{block}{Location Formats}
		\begin{itemize}
		\item \textless File\textgreater:\textless Line number\textgreater
		\item \textless File\textgreater:\textless Function\textgreater
		\item \textless Function\textgreater
		\end{itemize}
	\end{block}
	
	\begin{block}{Capabilities}
		\begin{itemize}
		\item Conditions
		\item Ignore Counts
		\item Associated actions in the debugger
		\item Stacktraces
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Process information}
	\begin{block}{Debug information}
		\begin{itemize}
		\item Very important for general usability
		\item Search functions and global variables
		\item Analyze types
		\item Print process arguments
		\end{itemize}
	\end{block}
	
	\begin{block}{Evaluating expressions}
		\begin{itemize}
		\item C-like syntax
		\item No bit-shift operators
		\item No function calls
		\item Pretty-printing results
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Fixing errors}
	\begin{block}{Variables}
		\begin{itemize}
		\item Changing variable values
		\item Insert assignment statements
		\item Problems with recursion
		\end{itemize}
	\end{block}
	
	\begin{block}{Functions}
		\begin{itemize}
		\item Perform function calls
		\item Insert function calls into the process
		\end{itemize}
	\end{block}
\end{frame}



\section{Dynamic Instrumentation}
\begin{frame}{Dynamic Instrumentation}
	\begin{block}{Use cases}
		\begin{itemize}
		\item Fixing errors in
			\begin{itemize}
			\item Simulations (should not be stopped)
			\item Programs which cannot be recompiled
			\end{itemize}
		\item Profiling and logging
		\item Bug detection
		\item Cheating on computer games
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Without helper library}
	\begin{block}{To change code of a process...}
		\begin{enumerate}
		\item Write new code
		\item Compile and assemble it
		\item Use a debugger to access process image
		\item Load forged code segment
		\item Continue operation
		\end{enumerate}
	\end{block}

	\begin{block}<2>{Problems}
		\begin{itemize}
		\item Complicated and error-prone
		\item Difficult to undo
		\item Knowledge of process image structure required
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Dyninst}
	\begin{block}{Overview}
		\begin{itemize}
		\item Factored out of Paradyn
		\item Paradyn: Performance measurement tool
		\item Developed at the University~of~Wisconsin-Madison and the University~of~Maryland
		\end{itemize}
	\end{block}

	\begin{block}{Abilities}
		\begin{itemize}
		\item Controlling and manipulating processes
		\item Querying the symbol table
		\item Inferring the Control~Flow~Graph
		\item Creating and injecting code into a process
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{The structure of the Dyninst library}
	\begin{figure}
		\includegraphics[scale=0.48]{Dyninst_Architecture.png}
	\end{figure}
\end{frame}



\section{Architecture}
\begin{frame}{Architecture}
	\begin{block}{Components}
		\begin{itemize}
		\item \textbf{Debugger} class
		\item Event-Handler Threads
		\item Main Loops
		\item Commands
		\item Dyninst library
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Event-Handler threads}
	\begin{block}{Dyninst Event Handler}
		\begin{itemize}
		\item Installed globally
		\item Uses \texttt{select}
		\item Handles all Dyninst Events
		\item Shuts down when the session ends
		\end{itemize}
	\end{block}
	
	\begin{block}{Breakpoint Handler}
		\begin{itemize}
		\item One thread per \textbf{Debugger} object
		\item Handles breakpoints
		\item Executes breakpoint actions
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Main Loops}
	\begin{block}{Common}
		\begin{itemize}
		\item Uses the GNU Readline library
		\item Parses commands
		\item Interfaces with \textbf{Debugger} objects
		\item Presents output to the user
		\end{itemize}
	\end{block}

	\begin{block}{Remote Main Loop}
		\begin{itemize}
		\item Opens a connection to a debugger server
		\item Thin client
		\item Sends commands to the server
		\item Displays results
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Debugger in local mode}
	\begin{figure}
		\includegraphics[scale=0.35]{LocalArchitecture.png}
	\end{figure}
\end{frame}

\begin{frame}{Debugger in remote mode}
	\begin{figure}
		\includegraphics[scale=0.25]{RemoteArchitecture.png}
	\end{figure}
\end{frame}

\begin{frame}{Message Protocol}
	\begin{figure}
		\includegraphics[scale=0.4]{Interaction.png}
	\end{figure}
\end{frame}


\section{Tools}
\begin{frame}{}
	\begin{block}{Development tools and libraries}
		\begin{itemize}
		\item \texttt{C++11}
		\item GNU Readline
		\item Dyninst
		\item CMake
		\item Eclipse CDT
		\item gdb
		\item \texttt{pgrep} utility
		\end{itemize}
	\end{block}
		
	\begin{flushright}
		\includegraphics[scale=0.2]{bogdanco_Toolkit.png}
	\end{flushright}
\end{frame}



\section{Demonstration}
\begin{frame}{}
	\begin{center}
		\huge Demonstration
	\end{center}
\end{frame}



\section{Conclusion}
\begin{frame}{``Would be nice''- features}
	\begin{block}{Features}
		\begin{itemize}
		\item More modification
		\item Debugging multiple processes
		\item More powerful expressions
		\end{itemize}
	\end{block}

	\begin{block}{Usability}
		\begin{itemize}
		\item Better MPI support
		\item Configuration and Scripting
		\item Built-in encryption for remote interface
		\item Autocompletion for commands
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{}
	\begin{block}{Achievements}
		\begin{itemize}
		\item Usable debugger
		\item Enables users to perform some kinds code modifications
		\item Strongly dependent on accurate debug information
		\item Starting point for
			\begin{itemize}
			\item evaluating other toolkits
			\item different console applications
			\end{itemize}
		\item Lessons about Software Development
		\item Code available at
			 \url{http://bit.ly/1ecqUiG}
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\begin{center}
		\huge Questions? Objections? Suggestions?
		\begin{figure}[b]
			\includegraphics[scale=0.5]{johnny_automatic_coffee_couple.png}	
		\end{figure}		
	\end{center}
\end{frame}

\end{document}
