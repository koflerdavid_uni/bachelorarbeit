/*
 * simplechat-client.C
 *
 *  Created on: 16.03.2014
 *      Author: David Kofler
 */

#include "protocol.H"

#include <cassert>
#include <cerrno>
#include <cstring>
#include <csignal>

#include <iostream>
#include <thread>
using namespace std;

#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>

#include <readline/readline.h>

int connectToServer(string host, size_t port = 30000) {
	struct addrinfo hints;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = 0;

	struct addrinfo *results = nullptr;
	int error = getaddrinfo(host.c_str(), to_string(port).c_str(), &hints, &results);

	if (error == EAI_NODATA || error == EAI_NONAME) {
		throw invalid_argument(gai_strerror(error));
	} else if (error == EAI_SYSTEM) {
		throw runtime_error(strerror(errno));
	} else if (error != 0) {
		throw runtime_error(gai_strerror(error));
	}

	int connection;
	struct addrinfo *current = results;
	do {
		connection = socket(current->ai_family, current->ai_socktype, current->ai_protocol);
		if (connection == -1) {
			if (current->ai_next == nullptr) {
				throw runtime_error("Could not connect to " + host + ":" + to_string(port));
			}

			current = current->ai_next;
		}

	} while (connection == -1);

	if (connect(connection, current->ai_addr, current->ai_addrlen) != 0) {
		throw runtime_error(strerror(errno));
	}

	freeaddrinfo(results);

	return connection;
}

void printMessage(const string& message) {
	// Save current input line
	char *savedLine;
	int savedPoint;

	savedPoint = rl_point;
	savedLine = rl_copy_text(0, rl_end);

	string prompt(rl_prompt);
	rl_set_prompt("");
	rl_replace_line("", 0);
	rl_redisplay();

	cout << message << endl;

	// Restore input
	rl_set_prompt(prompt.c_str());
	rl_replace_line(savedLine, 0);
	rl_point = savedPoint;
	rl_redisplay();

	free(savedLine);
}

void receiveAndPrintMessages(int connection) {
	string message;

	while (SimpleChat::receiveMessage(connection, message)) {
		printMessage(message);
	}
}

// Callbacks of readline needs this.
// Since readline already is a library which relies on globals, little harm is done.
SimpleChat::Sender *sender = nullptr;
bool quit = false;

void sendLineToServer(char *line) {
	assert(sender != nullptr);
	if (line == nullptr) {
		quit = true;
		return;
	}

	if (line[0] != '\0') {
		sender->enqueueMessage(line);
		sender->tryEmptyBuffer();
	}

	rl_free(line);
}

int main(int argc, char **argv) {
	if (argc < 2) {
		cerr << "Specify a hostname" << endl;
		return 2;
	}

	// This kind of error is far easier to handle using errno.
	signal(SIGPIPE, SIG_IGN);

	const int connection = argc == 2 ? connectToServer(argv[1]) : connectToServer(argv[1], stoul(argv[2]));

	char *line = nullptr;

	do {
		line = readline("Your name: ");
		if (line == nullptr) {
			return 0;
		}

	} while (line[0] == '\0');

	string name = line;
	rl_free(line);

	if (!SimpleChat::sendMessage(connection, name)) {
		return 1;
	}

	int flags = fcntl(connection, F_GETFL) | O_NONBLOCK;
	fcntl(connection, F_SETFL, flags);
	rl_callback_handler_install("> ", sendLineToServer);

	fd_set readDescriptors, writeDescriptors;
	FD_ZERO(&readDescriptors);
	FD_ZERO(&writeDescriptors);
	const int maxFd = max(STDIN_FILENO, connection);

	SimpleChat::Receiver receiver(connection);
	SimpleChat::Sender sender(connection);
	::sender = &sender;
	string message;

	while (true) {
		FD_SET(STDIN_FILENO, &readDescriptors);
		FD_SET(connection, &readDescriptors);
		if (sender.areThereMessagesToSend()) {
			FD_SET(connection, &writeDescriptors);
		}

		int result = select(maxFd + 1, &readDescriptors, &writeDescriptors, nullptr, nullptr);
		if (result == -1) {
			cout << strerror(errno) << endl;
			return (2);
		}

		if (FD_ISSET(STDIN_FILENO, &readDescriptors)) {
			rl_callback_read_char();
			// Presumably calls sendLineToServer(), so if there was an empty input line the quit flag will be set.
			if (quit) {
				break;
			}
		}

		if (FD_ISSET(connection, &readDescriptors)) {
			// Receive message
			if (!receiver.consumeInput()) {
				cout << "Lost connection" << endl;
				break;
			}

			if (receiver.tryParseMessage(message)) {
				printMessage(message.substr(SimpleChat::HEADER_LENGTH));
			}
		}

		if (FD_ISSET(connection, &writeDescriptors)) {
			if (!sender.tryEmptyBuffer()) {
				cout << "Lost connection" << endl;
				break;
			}
		}
	}

	close(connection);
	return 0;
}

