# Source: 
# https://projects.kde.org/projects/kde/kdeedu/analitza/repository/revisions/2e3629d9602c88de8177b4600d8aca372c84c877/entry/cmake/FindReadline.cmake
# GNU Readline library finder
if(READLINE_INCLUDE_DIR AND READLINE_LIBRARY)
	set(READLINE_FOUND TRUE)
else(READLINE_INCLUDE_DIR AND READLINE_LIBRARY)
	FIND_PATH(READLINE_INCLUDE_DIR readline/readline.h
		/usr/include/readline
	)
	
	FIND_LIBRARY(READLINE_LIBRARY NAMES readline)
        include(FindPackageHandleStandardArgs)
        FIND_PACKAGE_HANDLE_STANDARD_ARGS(Readline DEFAULT_MSG READLINE_INCLUDE_DIR READLINE_LIBRARY )
	
	MARK_AS_ADVANCED(READLINE_INCLUDE_DIR READLINE_LIBRARY)
endif(READLINE_INCLUDE_DIR AND READLINE_LIBRARY)
