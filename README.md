DPSTemplate
-----------

Contains the template for a DPS bachelor thesis.

Debugger
--------

Contains the debugger code. To fetch the whole source code the shell script
``get-submodules.sh'' must be run. To built the project, CMake 2.8, a
working Dyninst installation, the GNU readline and a C++11 compiler 
(GCC: >= v4.7, Clang: >= v3.3) are required.

    $ cmake .
    $ make
    $ make test
    $ ./debugger

Add ``-DDyninst_DIR=<path-to-dyninst-libraries>'' to the `cmake` invocation
if necessary.

InitialPresentation
-------------------

Contains the `tex` files for the initial presentation, held on 2013-04-09.

Thesis
------

Contains the `tex` files for the final thesis.

dyninst-test
------------

Contains some small test programs to test out features and 
idiosynchracies of Dyninst.
